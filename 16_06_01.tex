% !TEX root = Ana2.tex

Ein Punkt $x\in\bbR^d$ heißt \emph{Randpunkt} von $D\subset \bbR^d$ , wenn für jedes $\varepsilon > 0$
gilt
\begin{equation*}
	B_\varepsilon(x)\cap D \ne \emptyset \quad\text{und}\quad B_\varepsilon(x)\cap D^c \ne \emptyset.
\end{equation*}
Die Menge der Randpunkte von $D$ heißt \emph{Rand} von $D$ und wird mit $\partial D$ bezeichnet.

Beispiel $\partial (0,1) = \{0,1\} = \partial[0,1]$ oder für eine Kugel
$\partial B_R(x) = \{x: |x| = R\}$, $\partial \bbQ = \bbR$, $\partial\bbR = \emptyset$.

\begin{satz}
	\label{9.3.5}
	Sei $D\subset \bbR^d$. Dann gilt:
	\begin{enumerate}
		\item $\partial D = \overline{D} \setminus \inner{D}$ und $\partial D$ ist abgeschlossen.
		
		\item $\overline{D} = D\cup \partial D$.
		
		\item $\inner{D} = D\setminus \partial D$.
	\end{enumerate}
\end{satz}

\begin{proof}
	Erinnerung: $x\in \overline{D} \Leftrightarrow B_\varepsilon(x)\cap D\ne \emptyset$ für alle $\varepsilon > 0$.
	\begin{enumerate}
		\item $\partial D = \overline{D} \cap \overline{D^c} = \overline{D} \cap (\inner{D})^c = \overline{D}\setminus
			\inner{D}$.
		
		\item Aus \textit{1.} folgt
			\begin{equation*}
				D\cup \partial D = D \cup (\overline{D}\setminus \inner{D}) = \overline{D}.
			\end{equation*}
		
		\item $(\inner{D})^c = \overline{D^c} \overset{\textit{2.}}{=} D^c \cup \partial(D^c) = D^c \cup \partial D$.
			Daraus folgt $\inner{D} = D \cap (\partial D)^c = D\setminus \partial D$. \qedhere
	\end{enumerate}
\end{proof}

\begin{definition}
	Ein Punkt $x\in \bbR^d$ heißt \emph{Häufungspunkt} von $D$, wenn
	\begin{equation*}
		\dot{B}_\varepsilon(x) \cap D \ne \emptyset\qquad \text{für alle $\varepsilon > 0$}
	\end{equation*}
	wobei $\dot{B}_\varepsilon(x) \coloneqq B_\varepsilon(x) \setminus\{x\}$.
\end{definition}

\noindent
Es gilt $\overline{D} = D\cup \{x\in\bbR^d\mid \text{$x$ ist Häufungspunkt von $D$}\}$.

\begin{satz}
	\label{9.3.6}
	$x\in\bbR^d$ ist genau dann Häufungspunkt von $D\subset \bbR^d$, wenn $x$ der Grenzwert einer Folge $(x_n)$ in
	$D\setminus\{x\}$ ist.
\end{satz}

\begin{proof}
	Falls $x$ der Grenzwert einer Folge $(x_n)$ in $D\setminus\{x\}$ ist, dann gilt
	$x_n\in \dot{B}_\varepsilon(x)\cap D$ für $n\in \bbN$ groß genug. Also ist $x$ Häufungspunkt von $D$.
	Ist $x\in \bbR^d$ Häufungspunkt von $D$, dann ist für alle $n\in \bbN$
	\begin{equation*}
		\dot{B}_{1/n}(x)\cap D \ne \emptyset.
	\end{equation*}
	Wähle $x_n \in \dot{B}_{1/n}(x) \cap D$ für $n\in \bbN$. Dann ist $(x_n)$ eine Folge in $D\setminus\{x\}$ und
	$x_n\to x$.
\end{proof}

Eine Menge $K\subset \bbR^d$ heißt \emph{kompakt}, wenn zu jeder Überdeckung
\begin{equation*}
	K\subset \bigcup_{i \in I} V_i
\end{equation*}
durch offene Menge $V_i\subset \bbR^d$ eine endliche Teilmenge $\{i_1,\dots,i_n\} \subset I$ existiert, so dass
\begin{equation*}
	K \subset \bigcup_{k=1}^{n}V_{i_{k}}.
\end{equation*}

Man sagt: \grqq Jede offene Überdeckung von $K$ hat eine endliche Teilüberdeckung\grqq.

Wir betrachten einige Beispiele. $\emptyset \in \bbR^d$ ist kompakt und jede endliche Menge $\{a_1,\dots,a_n\}\subset
\bbR^d$ ist kompakt. $(0,1] \subset \bbR$ ist \emph{nicht} kompakt, denn die offene Überdecckung
\begin{equation*}
	(0,1] \subset \bigcup_{n=1}^{\infty} \left(\frac{1}{n}, 2\right]
\end{equation*}
hat keine endliche Teilüberdeckung. 

Eine Menge $K\subset \bbR^d$ heißt \emph{folgenkompakt}, wenn jede Folge $(x_n)$ in $K$ eine konvergente Teilfolge
hat, deren Grenzwert in $K$ liegt. Nach Bolzano-Weierstraß ist jede abgeschlossene, beschränkte Menge folgenkompakt.

\begin{theorem}
	\label{9.3.7}
	Theorem von Heine-Borel: Folgende Aussagen über $K \subset \bbR^d$ sind äquivalent.
	\begin{enumerate}
		\item $K$ ist kompakt.
		
		\item $K$ ist folgenkompakt.
		
		\item $K$ ist abgeschlossen und beschränkt.
	\end{enumerate}
\end{theorem}

Es ist zu betonen, dass dieser Satz nicht intuitiv klar ist, da jede dieser Definitionen komplett verschieden
formuliert wurde. Damit ist er auch einer der wichtigeren Sätze der mehrdimensionalen Analysis.

\begin{proof}
	Wir machen einen Ringschluss. Wir fangen an zu zeigen, dass die erste Aussage die zweite impliziert.
	Sei dafür $(x_n)$ eine Folge in $K$, $K$ kompakt. $(x_n)$ hat genau dann eine konvergente Teilfolge in $K$
	wenn $(x_n)$ einen Häufungspunkt in $K$ hat. Widerspruchsannahme: $(x_n)$ hat keinen Häufungspunkt in $K$. Dass
	heißt, zu jedem $x \in K$ gibt es eine Umgebung $B(x) = B_{\varepsilon(x)}(x)$ welche nur endlich viele Glieder
	der Folge enthält. Dann ist
	\begin{equation*}
		K \subset \bigcup_{x\in K} B(x)
	\end{equation*}
	eine offene Überdeckung von $K$. Da $K$ kompakt ist, existiert $x_1,\dots,x_n \in K$ mit
	$K \subset \bigcup_{i=1}^{n}B(x_i)$, wobei $B(x_i)$ nur endlich viele Glieder der Folge enthält. Das ist ein
	Widerspruch zur Annahme. Also hat $(x_n)$ einen Häufungspunkt in $K$ und somit auch eine konvergente Teilfolge.
	Damit folgt die zweite Aussage aus der ersten.
	
	Wir zeigen jetzt, wie die dritte Aussage aus der zweiten folgt. Sei $(x_n)$ eine Folge in $K$ mit Grenzwert
	$x\in \bbR^d$. Da $K$ folgenkompakt ist existiert eine konvergent Teilfolge mit Grenzwert $y\in K$. Also
	$x=y\in K$. Damit ist $K$ abgeschlossen nach Satz~\ref{9.3.3}. Wäre $K$ nicht beschränkt, dann gäbe es zu jedem
	$n\in \bbN$ ein $x_n\in K$ mit $|x_n| > n$. $(x_n)$ hat keine konvergente Teilfolge, damit ist $K$ nicht
	folgenkompakt.
	
	Es bleibt zu zeigen, dass die erste Aussage aus der dritten folgt. Sei $K$ abgeschlossen und beschränkt und sei
	$K \subset \bigcup_{i\in I} V_i$ eine offene Überdeckung von $K$. Wir treffen die Widerspruchsannahme, dass es
	keine endliche Teilüberdeckung gibt. Da $K$ beschränkt ist, gibt es einen abgeschlossenen Würfel $W$ der
	Kantenlänge~$L$ mit $K\subset W$. Wir zerlegen $W$ in $2^d$ abgeschlossene Teilwürfel der Kantenlänge~$L/2$.
	Nach Annahme finden wir einen davon, nennen wir ihn $W_1$, so dass $K\cap W_1$ nicht durch endlich viele
	$V_i$'s überdeckt wird. Wir zerlegen $W_1$ wieder in $2^d$ Teilwürfel (Kantenlänge $L/4$), wobei $K\cap W_2$,
	$W_2$, nicht durch endlich viele $V_i$ überdeckt wird. So erhalten wir eine Folge von Würfeln $W_k$ mit
	\begin{enumerate}
		\item $W\supset W_1 \supset W_2 \supset \dots$; die Kantenlänge von $W_n$ ist $L/2^n$.
		
		\item $K \cap W_n$ wird nicht durch endlich viele $V_i$ überdeckt; $n\in \bbN$.
	\end{enumerate}
	Wähle $x_n \in K\cap W_n$. Dann ist $(x_n)$ eine Cauchy-Folge wegen der ersten Aussage. Sei
	$a = \lim_{n\to\infty} x_n$. Da $x_n \in K$ und $K = \overline{K}$, ist $a \in K$. Also ist $a \in V_i$ für ein
	$i\in I$. Da $V_i$ offen ist, existiert $\varepsilon > 0$ mit $B_\varepsilon (a) \subset V_i$ wobei $a \in W_n$
	für alle $n$. Für $n$ groß genug ist also $W_n \subset B_\varepsilon(a) \subset V_i$. Insbesondere ist
	$K\cap W_n \subset V_i$. Damit sind wir fertig, denn dies ist im Widerspruch zu der zweiten Aussage von oben.
\end{proof}

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\draw (0,0) rectangle (4,4);
		\draw[blue] (2,0) rectangle (4,2);
		\draw[gray] (2,1) -- (4,1)
			(3,0) -- (3,2);
		
		\draw[orange,thick] plot[smooth cycle] coordinates {(0.5,0.5) (3,0.5) (3,1) (2,2) (3,3) (0.5,3.5)}
			node[anchor=north west] {$K$};

		\node[anchor=south west]      at (0,4) {$W$};
		\node[anchor=south west,blue] at (4,0) {$W_1$};
	\end{tikzpicture}
	\caption{Skizze der Zerlegung in Würfel in zwei Dimensionen.}
\end{figure}

\section{$M(m\times n,\bbR)$ als euklidischer Vektorraum}
Der Vektorraum der reellen $m\times n$ Matrizen, $M(m\times n, \bbR)$ ist isomorph zu $\bbR^{mn}$. D.h.\ die Abbildung
\begin{align*}
	\Phi: M(m\times n, \bbR) &\to \bbR^{mn}\\
	A = (A_{ik}) &\mapsto (A_{11},A_{12},\dots,A_{mn})
\end{align*}
ist linear und bijektiv. Wir definieren in $M(m\times n,\bbR)$ ein Skalarprodukt und eine Norm durch
\begin{equation*}
	\langle A,B\rangle \coloneqq \langle \Phi(A),\Phi(B)\rangle_{\bbR^{mn}}
\end{equation*}
und
\begin{equation*}
	||A|| = \langle A,A\rangle^{1/2} = \langle \Phi(A),\Phi(A)\rangle^{1/2} = |\Phi(A)|.
\end{equation*}
Das heißt
\begin{align*}
	\langle A,B\rangle &= \sum_{i=1}^{m}\sum_{k=1}^{n} A_{ik}B_{ik}\\
	&= \sum_{i=1}^{m}\sum_{k=1}^{n} (A^T)_{ki} B_{ik} \\
	&= \sum_{k=1}^{n}\sum_{i=1}^{m} (A^T)_{ki}B_{ik}\\
	&= \operatorname{tr}(A^TB)
\end{align*}
und
\begin{align*}
	||A|| &= \operatorname{tr}(A^TA) \\
	&= \left(\sum_{i=1}^{m}\sum_{k=1}^{n} A_{ik}^2\right)^{1/2}.
\end{align*}

Dann ist $M(m\times n,\bbR)$ von $\bbR^{mn}$ als euklidischer Vektorraum nicht unterscheidbar. Damit lässt sich auch
alle Sätze und Konvergenzkriterien aus diesem Kapitel auch auf $M(m\times n,\bbR)$ anwenden.

\begin{satz}
	\label{9.4.1}
	Sei $A\in M(m\times n,\bbR)$ und $B\in M(n\times l,\bbR)$, dann gilt
	\begin{enumerate}
		\item $||AB|| \le ||A|| \cdot ||B||$.
		
		\item Falls $\lim A_k = A$ und $\lim B_k = B$, dann gilt $\lim A_k B_k = AB$.
	\end{enumerate}
\end{satz}

\begin{proof}
	\begin{enumerate}
		\item Die erste Aussage lässt sich leicht mit der Schwarzschen Ungleichung beweisen und bleibt als Übung
			überlassen.
		
		\item Es ist
			\begin{align*}
				||A_nB_n - AB|| &= ||A_n(B_n - B) + (A_n-A)B_n||\\
				&\le ||A_n||\cdot \underset{\to 0}{\underbrace{||B_n-B||}}
					+ \underset{\to 0}{\underbrace{||A_n-A||}}\cdot ||B|| \to 0. \qedhere
			\end{align*}
	\end{enumerate}
\end{proof}

Eine Matrix $A \in M(n\times n,\bbR)$ heißt \emph{invertierbar}, wenn eine Matrix $B\in M(n\times n,\bbR)$ existiert,
so dass
\begin{equation*}
	AB = BA = E_n,
\end{equation*}
wobei $E_n$ die $n\times n$ Einheitsmatrix ist.
$B$ ist dann eindeutig bestimmt und  wird mit $A^{-1}$ bezeichnet.

Sind $A, B \in M(n\times n,\bbR)$ invertierbar, dann ist $AB$ invertierbar mit
\begin{equation*}
	(AB)^{-1} = B^{-1}A^{-1}.
\end{equation*}
Für $k\in \bbN_0$ definiert man $A^0 = E_n$ und $A^{k+1} = A^k A$. D.h.\ $A^1 = A$, $A^2 = AA$ und so fort.

\begin{theorem}
	\label{9.4.2}
	Sei $A\in M(n\times n,\bbR)$. Falls die Reihe $\sum_{k=0}^{\infty} A^k$ konvergent ist, also
	z.B.\ $||A|| < 1$, dann ist $E_n - A$ invertierbar und
	\begin{equation*}
		(E_n - A)^{-1} = \sum_{k=0}^{\infty}A^k = E_n + A + A^2 + \dots.
	\end{equation*}
\end{theorem}

Dieser Satz ist wirklich bemerkenswert, betrachte dazu das Analogon aus der Taylorreihe
\begin{equation*}
	\frac{1}{1 - x} = \sum_{k=0}^{\infty} x^k.
\end{equation*}















