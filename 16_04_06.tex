% !TEX root = Ana2.tex

\begin{corollary}
	\label{6.2.3}
	Jede Regelfunktion $f$ auf einem kompakten Intervall $[a,b]$ ist beschränkt
	\begin{equation*}
		\sup|f(x)| < \infty.
	\end{equation*}
\end{corollary}

\begin{proof}
	Wir wählen nach Theorem \ref{6.2.2} einfach eine Treppenfunktion $\varphi$, mit
	\begin{equation*}
		\sup_{x}|f(x)-\varphi(x)| < 1.
	\end{equation*}
	Dann gilt
	\begin{align*}
		\left||f(x)| - |\varphi(x)|\right| &\le \left|f(x) - \varphi(x)\right| \\
		&< 1.
	\end{align*}
	Damit folgt direkt
	\begin{equation*}
		\sup_{x}|f(x)| \le \sup(|\varphi(x)| + 1) = \sup |\varphi(x)| + 1 < \infty. \qedhere
	\end{equation*}
\end{proof}

\begin{corollary}
	\label{6.2.4}
	Eine Regelfunktion $f: I\to \bbC$ hat höchstens abzählbar viele Unstetigkeitsstellen.
\end{corollary}

\begin{proof}
	Da sich jedes Intervall $I$ schreiben lässt als abzählbare Vereinigung kompakter Intervalle, genügt es,
	das Korollar zu beweisen für $I = [a,b]$. Zu $n \in \bbN$ sei
	\begin{equation*}
		U_n = \{x\in[a,b]\mid |f(x) - f(x+)| > \frac{1}{n} \text{ oder } |f(x) - f(x+)| > \frac{1}{n}\}.
	\end{equation*}
	Dann ist $\bigcup_{n \ge 1} U_n$ die Menge der Unstetigkeitsstellen von $f$ in $[a,b]$. Vergleiche
	dazu mit dem Satz 4.4.9. Es genügt also zu zeigen, dass $U_n$ abzählbar ist. Nach Theorem \ref{6.2.2}
	existiert eine Treppenfunktion $\varphi$ mit 
	\begin{equation*}
		\sup_{x} | f(x) - \varphi(x)| < \frac{1}{2n}.
	\end{equation*}
	Auf jedem Intervall $(\alpha,\beta)$, wo $\varphi$ konstant ist, gilt
	\begin{equation*}
		|f(x)- f(x\pm)| = \lim_{h \to 0+} |f(x) - f(x\pm h)| \le \lim_{h\to 0}|\varphi(x) - \varphi(x\pm h)| +
		\frac{1}{n} = \frac{1}{n}.
	\end{equation*}
	Also ist $U_n \cap (\alpha,\beta) = \emptyset$.
\end{proof}

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,ymin=-0.1,ymax=1.5,xmin=-2.3,xmax=2.3,]
			\addplot[domain=-2:1,blue] {0.25*sin(2*deg(x)) + 1};			
			\addplot[domain=1:2,blue] {0.25*x*x + 0.75};
			
			\addplot[domain=-0.2:1.5,red] {1.1} node[anchor=west] {$\varphi$};
			\addplot[domain=-0.2:1.5,dashed,red] {1.3};
			\addplot[domain=-0.2:1.5,dashed,red] {0.9};
			
			\draw[green!50!black,dotted] (axis cs:-0.2,0) node[anchor=north] {$\alpha$} -- (axis cs:-0.2,1.5);
			\draw[green!50!black,dotted] (axis cs:1.5,0) node[anchor=north] {$\beta$} -- (axis cs:1.5,1.5);
		\end{axis}
	\end{tikzpicture}
	\caption{Skizze zu Korollar \ref{6.2.4}}
\end{figure}

Zur Vereinfachung der Notation führen wir die sogenannte \emph{Supremumsnorm} $||f||$ einer Funktion
$f:[a,b] \to \bbC$ ein. Sie ist definiert durch
\begin{equation*}
	\supnorm{f} \coloneqq \sup |f(x)| \le \alpha.
\end{equation*}

Es gilt (nach Blatt 1)
\begin{align*}
	\supnorm{f} &\ge 0 \qquad\text{und}\qquad \supnorm{f} = 0 \Leftrightarrow f = 0\\
	\supnorm{\alpha f} &= |\alpha|\supnorm{f}\\
	\supnorm{f + g} &\le \supnorm{f} + \supnorm{g}.
\end{align*}
Für eine Regelfunktion ist $\supnorm{f} < \alpha$ und zu jedem $\varepsilon > 0$ existiert eine
Treppenfunktion $\varphi$ mit $\supnorm{f - \varphi} < \varepsilon$ (Theorem \ref{6.2.2}). Für jede
Treppenfunktion $\varphi$ gilt
\begin{equation*}
	\left|\int_{a}^{b}\varphi\dd x\right| < (b-a) \supnorm{\varphi}.
\end{equation*}

\section{Integration von Regelfunktionen}
Nach Theorem \ref{6.2.2} ist $f:[a,b]\to \bbC$ genau dann eine Regelfunktion, wenn eine Folge
von Treppenfunktionen $\varphi_n:[a,b]\to \bbC$ existieren mit
\begin{equation*}
	\supnorm{f - \varphi_n} = \sup_{x} | f(x) - \varphi_n(x)| \to 0 \quad (n\to\infty).
\end{equation*}
Man sagt, $\varphi_n$ konvergiert \emph{gleichmäßig} gegen $f$ und man definiert
\begin{equation}
	\label{eq:Integral v. Regelfunktionen}
	\int_{a}^{b} f \dd x = \lim_{n\to\infty} \int_{a}^{b} \varphi_n \dd x.
\end{equation}

\begin{satz}
	\label{6.3.1}
	Der Limes \eqref{eq:Integral v. Regelfunktionen} existiert und ist unabhängig von der Wahl der Folge
	$(\varphi_n)$.
\end{satz}

\begin{proof}
	Sei $I_n \coloneqq \int_{a}^{b} \varphi_n \dd x$. Wir zeigen, dass $(I_n)$ eine Cauchy Folge ist. Sei
	$\varepsilon > 0$. Wähle nun ein $N\in \bbN$, so dass
	\begin{equation*}
		\supnorm{\varphi_n - f} < \frac{\varepsilon}{2(b-a)}
	\end{equation*}
	für $n \ge N$. Dann gilt mit $n,m \ge N$, dass
	\begin{align*}
		|I_n - I_m| &= \left|\int_{a}^{b} \varphi_n(x) \dd x - \int_{a}^{b}\varphi_m(x) \dd x \right|\\
		&= \left|\int_{a}^{b} \varphi_n(x) - \varphi_m(x) \dd x \right|\\
		&\le (b-a)\supnorm{\varphi_n - \varphi_m}\\
		&\le (b-a) \left(\supnorm{\varphi_n - f} + \supnorm{f - \varphi_m}\right)\\
		&< (b-a)2\frac{\varepsilon}{2(b-a)}\\
		&= \varepsilon.
	\end{align*}
	Somit existiert $\lim I_n = \int_{a}^{b}f \dd x$.
	
	Sei jetzt $(\psi_n)$ eine andere Folge von Treppenfunktionen mit $\supnorm{\psi_n - f} \to 0 \quad (n\to\infty)$.
	Dann gilt
	\begin{equation*}
		\left|\int_{a}^{b} f - \psi_n \dd x \right| \le \left|\int_{a}^{b} f - \varphi_n \dd x \right|
			+ \left|\int_{a}^{b} \varphi_n - \psi_n \dd x \right|
	\end{equation*}
	und
	\begin{IEEEeqnarray*}{+rCl+x*}
		\left|\int_{a}^{b} \varphi_n - \psi_n \dd x \right| & \le & (b-a)\supnorm{\varphi_n - \psi_n} \\
		& \le & (b-a) \Big(\supnorm{\varphi_n - f} + \supnorm{f-\psi_n}\Big) \to 0 \quad (n\to \infty). & \qedhere
	\end{IEEEeqnarray*}
\end{proof}

Man definiert auch
\begin{equation*}
	\int_{a}^{a} f(x) \dd x \coloneqq 0
\end{equation*}
und
\begin{equation*}
	\int_{b}^{a} f(x) \dd x \coloneqq -\int_{a}^{b} f(x) \dd x .
\end{equation*}

\emph{Beispiel} Berechnung von $\int_{0}^{1} x^2\dd x$. Sei $f(x) = x^2$ auf $[0,1]$ und sei
$\varphi_n:[0,1]\to \bbR$ definiert durch $\varphi_n(0) = 0$ und $\varphi_n(x) = \frac{k^2}{n^2}$ für
$x\in (\frac{k-1}{n},\frac{k}{n})$, $k= 1,\dots,n$. Dann gilt $\varphi_n \to f$. Also
\begin{align*}
	\int_{0}^{1} x^2 \dd x &= \lim_{n\to \infty} \int_{0}^{1} \varphi_n\dd x\\
	&=\lim_{n\to\infty} \frac{1}{n} \sum_{i=1}^{n}\frac{k^2}{n^2}\\
	&=\lim_{n\to\infty} \frac{1}{n^3} \frac{1}{6} n(n+1)(2n+1)\\
	&=\lim_{n\to\infty} \frac{1}{3} + o\left(\frac{1}{n}\right)\\
	&= \frac{1}{3}.
\end{align*}
Eine Skizze dieses konvergenten Verfahrens ist für $n=10$ in Abbildung~\ref{fig:euler integration} dargestellt.
Es fällt auf, dass bei unserer Wahl der Testfunktion die approximierende Funktion für endliche $n$ das
Integral immer unterschätzt.\footnote{Die Skizze entspricht nicht der Funktion $\varphi_n$, dort wird das Integral überschätzt.} Der Fehler wird mit wachsendem $n$ immer kleiner und verschwindet bei
$n\to\infty$.


\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,ymin=-0.2,ymax=1.2]
			\addplot[domain=0:1,blue] {x*x};
			\foreach \x in {0,0.1,...,0.91}
			{
				\edef\temp{\noexpand\addplot[domain=\x:\x+0.1,orange] {\x*\x};}
				\temp
			}
			\node at (axis cs:0.2,1) {$n=10$};
		\end{axis}
	\end{tikzpicture}
	\caption{Skizze des Beispiels.}
	\label{fig:euler integration}
\end{figure}

\begin{satz}
	\label{6.3.2}
	Für Regelfunktionen $f,g,:[a,b]\to \bbC$ und $\alpha,\beta\in\bbC$ gilt
	\begin{enumerate}[label=\alph*)]
		\item \[\int_{a}^{b} \alpha f + \beta\gamma \dd x = \alpha \int_{a}^{b}f\dd x
			+ \beta \int_{a}^{b} g\dd x, \]
		
		\item \[\left|\int_{a}^{b}f\dd x \right| \le \int_{a}^{b} |f|\dd x \le (b-a)
			\sup(|f|), \]
		
		\item Falls $f \le g$, dann gilt \[\int_{a}^{b} f \dd x
			\le \int_{a}^{b} g \dd x.\]
	\end{enumerate}
\end{satz}

\begin{proof}
\leavevmode
	\begin{enumerate}[label=\alph*)]
		\item Sei $(\varphi_n)$, $(\gamma_n)$ Folgen von Treppenfunktionen mit $\supnorm{\varphi_n -f} \to 0$
			und $\supnorm{\gamma_n -f} \to 0$ mit $n \to \infty$. Dann ist $\alpha\varphi_n + \beta\gamma_n$
			eine Treppenfunktion und
			\begin{equation*}
				\supnorm{\alpha f + \beta g - (\alpha\varphi_n + \beta\gamma_n) }
				\le |\alpha|\supnorm{f - \varphi_n} + |\beta|\supnorm{g - \gamma _n} \to 0.
			\end{equation*}
			Also
			\begin{align*}
				\int_{a}^{b} (\alpha f + \beta g) &= \lim_{n\to \infty} \int_{a}^{b} \alpha\varphi_n +	
					\beta\gamma_n \dd x\\
				&= \lim_{n\to\infty}\alpha\int_{a}^{b}\varphi_n\dd x
					+ \beta \int_{a}^{b}\gamma_n\dd x\\
				&= \alpha\int_{a}^{b} f\dd x + \beta\int_{a}^{b}\gamma_n \dd x.
			\end{align*}
		
		\item Aus $\supnorm{\varphi_n -f } \to 0$ folgt
			\begin{align*}
				\supnorm{|\varphi_n| - |f|} &= \sup_{x}\Big||\varphi_n(x)| -|f(x)|\Big|\\
				&\le \sup_{x}\Big|\varphi_n(x) - f(x)\Big|\\
				&= \supnorm{\varphi_n - f} \to 0
			\end{align*}
			Also konvergiert $\supnorm{|\varphi_n| - |f|} \to 0$. Folglich
			\begin{align*}
				\left|\int_{a}^{b} f\dd x \right| &=
					\lim_{n\to\infty}\left|\int_{a}^{b} \varphi_n\dd x \right|\\
				&\le \lim_{n \to \infty} \int_{a}^{b} |\varphi_n|\dd x\\
				&= \int_{a}^{b} |f|\dd x.
			\end{align*}
			Außerdem gilt
			\begin{equation*}
				\lim_{n\to\infty} \int_{a}^{b}|\varphi_n|\dd x \le \lim_{n \to \infty} (b-a)\supnorm{\varphi_n}
				=(b-a)\supnorm{f}.
			\end{equation*}
		
		\item Sei $f\le g$, $\varphi_n \to f$ und $\gamma_n \to g$. Definiere
			$\alpha_n = \supnorm{f-\varphi_n}$, $\beta_n = \supnorm{g-\gamma_n}$, dann gilt
			\begin{equation*}
				\varphi_n - \alpha_n \le f \le g \le \gamma_n + \beta_n.
			\end{equation*}
			Weiter leiten wir
			\begin{align*}
				\varphi_n(x) = \varphi_n(x) - f(x) + f(x) \le f(x) + |\varphi_n(x) - f(x)| \le f(x) +\alpha_n
			\end{align*}
			ab, womit sich das praktische Resultat $\varphi_n - \alpha_n \le f$ ergibt.
			Es ist
			\begin{equation*}
				\supnorm{f - (\varphi_n - \alpha_n)} \le \supnorm{f - \varphi_n} + \alpha_n \to 0
			\end{equation*}
			und
			\begin{equation*}
				\supnorm{g - (\gamma_n - \beta_n)} \le \supnorm{g - \gamma_n} + \beta_n \to 0.
			\end{equation*}
			Somit gilt
			\begin{equation*}
				\int_{a}^{b} f \dd x = \lim_{n\to\infty}\int_{a}^{b} (\varphi_n - \alpha_n) \dd x
				\le \lim_{n \to \infty} \int_{a}^{b}(\gamma_n + \beta_n) \dd x = \int_{a}^{b} g \dd x. \qedhere
			\end{equation*}
	\end{enumerate}
\end{proof}

\begin{satz}
	\label{6.3.3}
	Ist $f:I\to \bbC$ eine Regelfunktion und $a,b,c,\in I$, dann gilt
	\begin{equation*}
		\int_{a}^{b} f\dd x + \int_{b}^{c} f \dd x = \int_{a}^{c} f \dd x.
	\end{equation*}
\end{satz}

\begin{proof}
	Falls $a<b<c$. Da $f\big|_{[a,b]}$ und $f\big|_{[b,c]}$ Regelfunktionen sind existieren Folgen
	$\varphi_n:[a,b]\to \bbC$ und $\psi_n:[b,c]\to \bbC$ von Treppenfunktionen mit
	\begin{equation*}
		\lim_{n\to\infty}\supnorm{f-\varphi_n} \to 0,\qquad
		\lim_{n\to\infty}\supnorm{g-\psi_n} \to 0.
	\end{equation*}
	
	Sei $\sigma_n:[a,c]\to\bbC$ definiert durch
	\begin{equation*}
		\sigma_n(x) \coloneqq
		\begin{cases}
			\varphi(n) & x\in[a,b]\\
			\psi_n(x) & x \in(b,c]
		\end{cases}.
	\end{equation*}
	Dann ist $\sigma_n$ eine Folge von Treppenfunktionen mit
	\begin{IEEEeqnarray*}{+rCl+x*}
	\sup_{x\in[a,c]}|f(x)-\sigma_n(x)| & \le & \sup_{x\in[a,b]}|f(x)-\varphi(x)|  + \sup_{x\in(b,c]}|f-\psi_n| \\
		& \to & 0 \quad  (n\to\infty).
	\end{IEEEeqnarray*}
		
	Also
	\begin{align*}
		\int_{a}^{c} f\dd x &= \lim_{n\to\infty}\int_{a}^{c} \sigma_n\dd x\\
		&= \lim\limits_{n\to\infty}\left(\int_{a}^{b}\varphi_n\dd x + \int_{b}^{c} \gamma_n \dd x\right)\\
		&= \int_{a}^{b} f\dd x + \int_{b}^{c} f \dd x.
	\end{align*}
	
	In den übrigen Fällen beweist man die Aussage ganz analog.
\end{proof}

\begin{satz}
	\label{6.3.4}
	\emph{Mittelwertsatz:} Sei $f:[a,b]\to \bbR$ stetig und $p:[a,b]\to\bbR$ eine Regelfunktion mit
	$p \ge 0$, dann existiert ein $t \in [a,b]$ mit
	\begin{equation*}
		\int_{a}^{b} fp\dd x = f(t) \int_{a}^{b} p\dd x.
	\end{equation*}
	Folgerung: Ist $f:[a,b]\to \bbR$ stetig, dann existiert $t\in[a,b]$ mit
	\begin{equation*}
		\frac{1}{b-a}\int_{a}^{b}f(x)\dd x = f(t).
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $m = \min_{x\in[a,b]} f(x)$ und $M = \max_{x\in[a,b]} f(x)$ Dann gilt $mp \le fp \le Mp$. Also, nach
	Satz \ref{6.3.2}
	\begin{equation*}
		m\int_{a}^{b} p \dd x = \int_{a}^{b} mp\dd x \le \int_{a}^{b} fp\dd x
		\le \int_{a}^{b} Mp\dd x = M\int_{a}^{b} p\dd x.
	\end{equation*}
	Wenn $\int_{a}^{b}p\dd x > 0$, dann folgt
	\begin{equation*}
		m \le \frac{\int_{a}^{b} fp\dd x}{\int_{a}^{b} p \dd x} \le M
	\end{equation*}
	und die Behauptung folgt aus dem Zwischenwertsatz für stetige Funktionen. Falls $\int_{a}^{b} p \dd x = 0$,
	dann folgt, dass $\int_{a}^{b}fp\dd x = 0$. Somit gilt die Behauptung für jedes $t\in[a,b]$.
\end{proof}