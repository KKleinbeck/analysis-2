% !TEX root = Ana2.tex

\section{Potenzreihen}
Eine Potenzreihe $\sum a_k x^k$, $a_k \in \bbC$, Konvergenzradius $R$ konvergiert für $|x| < R$ absolut
(Satz 3.7.1). Wenn also $r < R$, dann ist
\begin{equation*}
	\sum_{k=0}^{\infty} |a_k| r^k < \infty,
\end{equation*}
wobei
\begin{equation*}
	\sup_{|x|<r} |a_k x^k| \le |a_k| r^k \equiv M_k.
\end{equation*}
Also folgt aus dem Weierstraß'schen $M$-Test

\begin{theorem}
	\label{8.3.1}
	Eine Potenzreihe $\sum a_k x^k$, $a_k\in \bbC$, mit Konvergenzradius $R>0$ konvergiert auf jedem
	abgeschlossenen Intervall $[-r,r]$ mit $r < R$ gleichmäßig.
\end{theorem}

Im allgemeinen ist die Konvergenz auf $(-R,R)$ \emph{nicht} gleichmäßig.

Als Beispiel betrachten wir $a_k = 1$. Die Potenzreihe
\begin{equation*}
	\sum x^k
\end{equation*}
ist punktweise aber nicht gleichmäßig konvergent auf $(-1,1)$, denn
\begin{equation*}
	\sum_{k=0}^{n-1} x^k = \frac{1-x^n}{1-x},
\end{equation*}
wobei $x^n \to 0$ für $x\in (-1,1)$ punktweise aber nicht gleichmäßig.

\begin{theorem}
	\label{8.3.2}
	Die Summe einer Potenzreihe
	\begin{equation*}
		f(x) = \sum_{k=0}^{\infty} a_k x^k, \qquad a_k \in \bbC,
	\end{equation*}
	mit Konvergenzradius $R > 0$ ist auf $(-R,R)$ stetig und es gilt
	\begin{equation*}
		\int_{0}^{x}f(t) \dd t = \sum_{k=0}^{\infty} a_k\frac{x^{k+1}}{k+1}, \qquad |x| < R.
	\end{equation*}
	Der Konvergenzradius der Reihe rechts beträgt ebenfalls $R$.
\end{theorem}

\begin{proof}
	Nach Satz~\ref{8.2.4} ist $f$ auf jeden Intervall $[r,r]$ mit $r < R$ stetig und für $|x| \le r$ gilt
	\begin{align*}
		\int_{0}^{x} f(t) \dd t = \sum_{k=0}^{\infty} \int_{0}^{x} a_k t^k \dd t
			= \sum_{k=0}^{\infty} a_k\frac{x^{k+1}}{k+1}.
	\end{align*}
	Da $r < R$ beliebig war ist damit der erste Teil von Theorem~\ref{8.3.2} bewiesen.
	
	Es bleibt zu beweisen, dass der Konvergenzradius ebenfalls $R$ ist. Aus
	\begin{equation*}
		\lim_{k\to\infty} \sqrt[k]{k + 1} = 1
	\end{equation*}
	folgt
	\begin{equation*}
		\varrho^{-1} = \overline{\lim_{k\to 1}} \sqrt[k]{\frac{|a_k|}{k+1}}
			= \overline{\lim_{k\to 1}} \sqrt[k]{|a_k|} = R^{-1},
	\end{equation*}
	wobei $\varrho$ der Konvergenzradius von $\sum \frac{a_k}{k+1} x^k$ ist. Damit folgt $\varrho = R$.
\end{proof}

Als Beispiel betrachten wir die Stammfunktion von $e^{-x^2}$. Für $t \in\bbR$ gilt
\begin{equation*}
	e^{-t^2} = \sum_{k=0}^{\infty} (-1)^k \frac{t^{2k}}{k!},
\end{equation*}
d.h.\ der Konvergenzradius ist $R = \infty$. Für alle $x\in \bbR$ gilt also nach Theorem~\ref{8.2.2}
\begin{align*}
	\int_{0}^{x} e^{-t^2}\dd t &= \sum_{k=0}^{\infty} \frac{(-1)^k}{k!} \int_{0}^{x} t^{2k} \dd t \\
		&= \sum_{k=0}^{\infty} \frac{(-1)^k}{k!} \frac{x^{2k+1}}{2k+1}.
\end{align*}

Ein weiteres Beispiel ist
\begin{equation*}
	\frac{1}{1 + x} = \sum_{k=0}^{\infty} (-1)^k x^k,
\end{equation*}
für $|x| < 1$. Also ist
\begin{align*}
	\log(1+x) &= \int_{0}^{x} \frac{1}{1+x} \dd x\\
	&= \int_{0}^{x} \sum_{k=0}^{\infty}(-1)^{k}t^k \dd t\\
	&= \sum_{k=0}^{\infty} (-1)^k\frac{x^{k+1}}{k+1}\\
	&= x - \frac{x^2}{2} + \frac{x^3}{3} - \frac{x^4}{4} + \dots.
\end{align*}

\begin{Theorem}
	\label{8.3.3}
	Die Summe einer Potenzreihe
	\begin{equation*}
		f(x) \coloneqq \sum_{k=0}^{\infty} a_k x^k,\qquad a_k \in \bbC,
	\end{equation*}
	mit Konvergenzradius $R$ ist auf $(-R,R)$ beliebig oft differenzierbar und die Ableitungen $F^{(n)}$
	bekommt man durch gliedweises Differenzieren:
	\begin{equation*}
		f^{(n)}(x) = \sum_{k=0}^{\infty} a_k k(k-1)\dots (k-n+1) x^{k-n}.
	\end{equation*}
	Die Potenzreihe rechts hat ebenfalls Konvergenzradius $R$.
\end{Theorem}

\begin{proof}
	Die Reihe $\sum_{k=0}^{\infty} a_k kx^{k-1}$ hat denselben Konvergenzradius $\varrho$ wie
	$\sum a_k k x^k$, also
	\begin{equation*}
		\varrho^{-1} = \overline{\lim_{k\to\infty}}\sqrt[k]{|a_k|k} = \overline{\lim_{k\to\infty}}\sqrt[k]{|a_k|}
		= R^{-1}.
	\end{equation*}
	Dass heißt $\varrho = R$. Somit ist $\sum a_k k ^{k-1}$ auf jedem abgeschlossenen Intervall $[-r,r]$ mit
	$r < R$ \emph{gleichmäßig} konvergent (Theorem~\ref{8.3.1}). Nach Satz~\ref{8.2.5} ist
	$\sum a_k x^k$ auf $[-r,r]$ differenzierbar und
	\begin{equation*}
		\frac{\dd}{\dd x}\sum_{k=0}^{\infty} a_k x^k = \sum_{k=0}^{\infty} a_k k x^{k-1}.
	\end{equation*}
	Damit ist Theorem~\ref{8.3.3} für $n = 1$ bewiesen. Der Fall $n > 1$ folgt durch Induktion.
\end{proof}

Beispielsweise hat die Potenzreihe
\begin{equation*}
	\sum_{k=0}^{\infty} (-1)^{k}\frac{x^{2k+1}}{2k+1} = x - \frac{x^3}{3} + \frac{x^5}{5} + \dots
\end{equation*}
den Konvergenzradius $R = 1$. Also ist die Summe differenzierbar auf $(-1,1)$ und es gilt
\begin{equation*}
	\frac{\dd}{\dd x}\sum_{k=0}^{\infty} (-1)^{k}\frac{x^{2k+1}}{2k+1} = \sum (-x^2)^{k}
	= \frac{1}{1 + x^2} = \frac{\dd}{\dd x} \arctan x.
\end{equation*}
Also ist
\begin{equation*}
	\sum_{k=0}^{\infty} (-1)^{k}\frac{x^{2k+1}}{2k+1} = \arctan x.
\end{equation*}

\begin{cor}
	\label{8.3.4}
	Die Funktion $f: (a-R,a+R) \to \bbC$ sei gegeben durch eine konvergente Potenzreihe
	\begin{equation*}
		f(x) = \sum_{k=0}^{\infty} a_k (x-a)^k, \qquad a_k \in \bbC, a \in \bbR.
	\end{equation*}
	Dann gilt $a_k = \frac{f^{(k)}(a)}{k!}$. D. h. $\sum_{k=0}^{\infty} a_k (x-a)^{k}$ ist die
	Taylorreihe von $f$ mit Entwicklungspunkt $a$.
\end{cor}

\begin{proof}
	Der Konvergenzradius von $\sum a_k x^k$ ist nach Annahme gleich $R$ oder größer. Also ist die Funktion
	\begin{equation*}
		g(t) = f(t + a) = \sum_{k=0}^{\infty} a_k t^k
	\end{equation*}
	auf $(-R,R)$ nach Theorem~\ref{8.3.3}, beliebig oft differenzierbar und es gilt
	\begin{equation*}
		g^{(n)}(0) = n! a_n.
	\end{equation*}
	Also ist $f(x) = g(x-a)$ auf $(-R+a, R+a)$ beliebig oft differenzierbar und
	\begin{equation*}
		f^{(n)}(a) = g^{(n)}(0) = n! a_n. \qedhere
	\end{equation*}
\end{proof}

Beispiel: Wir bestimmen die Taylorreihe von $f(x) = \frac{1}{x}$ mit dem Entwicklungspunkt $a = 3$. Die
erste Möglichkeit ist, mit der Definition der Taylorreihe
\begin{equation*}
	f'(x) = -\frac{1}{x^2}, f''(x) = \frac{2}{x^3}, \dots, f^{(n)}(x) = (-1)^n\frac{n!}{x^{n+1}}.
\end{equation*}
Die Taylorreihe lautet dann
\begin{equation*}
	\sum_{n=0}^{\infty} \frac{f^{(n)}(3)}{n!} (x-3)^n = \sum (-1)^n \frac{1}{3^{n+1}} (x-3)^n.
\end{equation*}
Der zweite mögliche Weg führt über Korollar~\ref{8.3.4}
\begin{align*}
	\frac{1}{x} &= \frac{1}{3 + (x-3)}\\
	&= \frac{1}{3}\frac{1}{1 + \frac{x-3}{3}}\\
	&= \frac{1}{3} \sum_{n=0}^{\infty} (-1)^n \left(\frac{x-3}{3}\right)^n\\
	&= \sum_{n=0}^{\infty}(-1)^n \frac{1}{3^{n+1}}\left(x-3\right)^n.
\end{align*}

2. Beispiel: Berechne $f^{(30)}(0)$ für $f(x) = \sin x^2$.
Die Lösung ist über die Reihenentwicklung von $\sin x^2$:
\begin{align*}
	\sin x^2 &= \sum (-1)^{n}\frac{x^{2(2n+1)}}{(2n+1)!}\\
	&= \sum (-1)^{n}\frac{x^{4n+2}}{(2n+1)!}\\
	&= \sum \frac{f^{(k)}(0)}{k!}x^k.
\end{align*}
Mit $4n + 2 = 30$ für $n=7$ folgt
\begin{equation*}
	\frac{f^{(30)}(0)}{30!} = (-1)^{7} \frac{1}{15!}.
\end{equation*}
Also folgt $f^{(30)}(0) = -\frac{30!}{15!}$.

Als letztes Beispiel berechnen wir den Konvergeznradius und die Summe von
\begin{equation*}
	\sum_{k=0}^{\infty} k^2 x^k.
\end{equation*}
Der Konvergenzradius ist $1$ und zur Berechnung der Summe verwenden wir
\begin{equation*}
	\sum x^k = \frac{1}{1 - x} \quad\text{und} \quad \frac{\dd}{\dd x}
		\sum_{k=0}^{\infty} x^k = \sum_{k=1}^{\infty} k x^{k-1}.
\end{equation*}
Also ist
\begin{equation*}
	x\frac{\dd}{\dd x}\sum x^k = \sum k x^k
\end{equation*}
und
\begin{equation*}
	\frac{\dd}{\dd x}\left(x\frac{\dd}{\dd x} \sum x^k \right) = \sum k^2 x^{k-1}.
\end{equation*}
Folglich
\begin{equation*}
	x \frac{\dd}{\dd x}\left(x \frac{\dd}{\dd x} \frac{1}{1 - x} \right) = \sum k^2 x^k
		= \frac{x + x^2}{(1 - x)^3}.
\end{equation*}

Wir haben gesehen, dass für $x \in (-1,1)$
\begin{align*}
	\log (1+x) &= x - \frac{x^2}{2} + \frac{x^3}{3} - \dots \\
	\arctan(x) &= x - \frac{x^3}{3} + \frac{x^5}{5} - \frac{x^7}{7} + \dots.
\end{align*}
Diese alterniernden Reihen sind auch für $x=1$ noch konvergent. Also stellt sich die Frage, ob diese Gleichungen
auch noch für $x = 1$ korrekt sind.

\begin{satz}
	\label{8.3.5}
	Satz von Abel: Ist die Reihe $\sum_{k=0}^{\infty} a_k$, $a_k \in \bbC$ konvergent, so ist die Potenzreihe
	$\sum_{k=0}^{\infty} a_k x^k$ auf $[0,1]$ gleichmäßig konvergent und insbesondere gilt
	\begin{equation*}
		\sum_{k=0}^{\infty} a_k = \lim_{x \to 1-}\sum_{k=0}^{\infty} a_k x^k.
	\end{equation*}
\end{satz}





















