% !TEX root = Ana2.tex

\section{Partielle Ableitungen}
Sei $D\subset \bbR^n$ offen und $f:D\to \bbR$, d..h
\begin{equation*}
	(x_1,\dots,x_n)\mapsto f(x_1,\dots,x_n) \in \bbR.
\end{equation*}
$f$ heißt im Punkt $x\in D$ \emph{partiell differenzierbar} nach der $i$-ten Variable, falls der
Limes
\begin{equation*}
	\partial_i f(x) \coloneqq \lim_{h \to 0} \frac{1}{h} (f(x_1,\dots,x_{i-1}, x_{i} + h, x_{i+1},\dots,x_n)
		- f(x_1,\dots,x_n))
\end{equation*}
existiert. Statt $\partial_i f$ schreibt man auch
\begin{equation*}
	\frac{\partial}{\partial x_i}f = f_{x_i} = \partial_i f.
\end{equation*}

Bemerkung: $\partial_i f$ ist nichts anderes als die Ableitung der partiellen Funktion
$y_i \mapsto f(x_1,\dots,x_{i-1},y_i,x_{i+1},\dots x_n)$ an der Stelle $x_i$.

Betrachten wir als Beispiel die Funktion $f(x,y) = (1 - x^2 - y^2)^{1/2}$ für $x^2 + y^2 < 1$.
Dann gilt
\begin{equation*}
	\frac{\partial}{\partial x} f(x,y) =  \frac{1}{2}(1-x^2 - y^2)^{-1/2} \frac{\partial}{\partial x} (1 - x^2 - y^2)
		= - \frac{x}{(1 - x^2 - y^2)^{1/2}}.
\end{equation*}

\begin{figure}[!t]
	\centering
	\tdplotsetmaincoords{0}{0}
	\begin{tikzpicture}[scale=3]
		\draw[->] (0,0,0) -- (0,0,1.2) node[anchor=north east] {$x$};
		\draw[->] (0,0,0) -- (1.2,0,0) node[anchor=west] {$y$};
		\draw[->] (0,0,0) -- (0,1.2,0);
		
		\draw[dashed,gray] (1,0,0) -- (1,1,0) -- (0,1,0) -- (0,1,1) -- (0,0,1) -- (1,0,1) -- cycle;
		
		\tdplotdrawarc[canvas is yz plane at x=0,blue]{(0,0,0)}{1}{0}{90}{}{};
		\tdplotdrawarc[canvas is xz plane at y=0,blue]{(0,0,0)}{1}{0}{90}{}{};
		\tdplotdrawarc[canvas is xy plane at z=0,blue]{(0,0,0)}{1}{0}{90}{}{};
		
		\pgfmathsetmacro{\cs}{cos(60)} % cosine sixty
		\tdplotdrawarc[canvas is yz plane at x=sin(60),blue]{(0,0,0)}{\cs}{0}{90}{}{};
		\pgfmathsetmacro{\ct}{cos(30)} % cosine thirty
		\tdplotdrawarc[canvas is yz plane at x=sin(30),blue]{(0,0,0)}{\ct}{0}{90}{}{};
		\tdplotdrawarc[canvas is xz plane at y=sin(30),orange]{(0,0,0)}{\ct}{0}{90}{}{};
		\node[orange,anchor=east] at (0,\cs,\ct) {$f(x,y) = \mathrm{const}$};
		
		\draw[green!50!black,thick] (.5,.25 - .1,.8660 + .05) -- (.5,.25 + .9,.8660 - .45)
			node[anchor=south west] {$T(x,y_0) = f(x_0,y_0) + \partial_x f(x,y_0)\big|_{x=x_0}x$};
	\end{tikzpicture}
	\caption{Grafische Repräsentation von $f(x,y) = (1 - x^2 - y^2)^{1/2}$. Die Kurven mit konstantem $f(x,y)$ definieren
		Kreisringe, die partiellen Funktionen $f(x,y_0)$ und $f(x_0,y)$ mit festem $y_0$, bzw.\ $y_0$ liegen auf
		Halbkreisen. Die partiellen Ableitungen bilden Tangenten $T(x,y_0)$, bzw.\ $T(x_0,y)$, entlang der
		Ringe.}
	\label{fig:Kreisbogen}
\end{figure}

Die partiellen Ableitungen lassen sich in diesem Fall schön darstellen, denn die Kurven mit $f(x,y) = \mathrm{const}$
liegen auf Kreisringen mit verschiedenen Radius. Für $x = \mathrm{const}$ liegt die partielle Funktion $f(x,y)$
ebenfalls auf einem Halbkreis, $\partial_y f(x,y)$ spiegelt in dem Fall die Steigung der Tangenten entlang dieser
Kurve wider. Gleiches gilt für die partielle Funktion mit festem $y$ und partieller Ableitung nach $x$. Eine Skizze
dazu ist in Abbildung~\ref{fig:Kreisbogen} dargestellt.

$f: D \to \bbR$ heißt partiell differenzierbar, wenn $\partial_i f$ für alle $x\in D$ und alle $i \in \{1,\dots,n\}$
existiert.

Bezüglich partiellen Ableitungen ergeben sich einige interessante Fragen: Sind die Funktionen $\partial_i f$ noch
einmal partiell differenzierbar? Lautet die Antwort ja, gilt dann auch
$\partial_i \partial_j f = \partial_j \partial_i f$?

Betrachten wir als Beispiel $f(x,y) = \sin(xy^2)$, dann ist
\begin{align*}
	\partial_x f(x,y) &= \cos(xy^2)y^2\\
	\partial_y\partial_x f(x,y) &= -\sin(xy^2)2xy^3 + \cos(xy^2)2y\\
%
	\partial_x\partial_y f(x,y) &= -\sin(xy^2)2xy^3 + \cos(xy^2)2y.
\end{align*}
Offensichtlich können wir hier die Ableitungen vertauschen.
Aber auf der anderen Seite betrachten wir die Funktion
\begin{equation*}
	f(x,y) = \frac{x^3y - xy^3}{x^2 + y^2}, \quad f(0,0) = 0.
\end{equation*}
Dann gilt für $(x,y)\ne (0,0)$
\begin{align*}
	\partial_x f(x,y) &= \frac{x^4y + 4x^2y^3 -  y^5}{(x^2 + y^2)^2} \\
	\partial_y f(x,y) &= \frac{x^5  - 4x^3y^2 - xy^4}{(x^2 + y^2)^2} \\
\intertext{und}
	\partial_x f(0,0) &= \lim_{x\to 0} \frac{f(x,0) - f(0,0)}{x} = 0 = \partial_y f(0,0).
\end{align*}
Also ist
\begin{align*}
	\partial_y\partial_x f(0,0) &= \frac{\partial_x f(0,h) - \overset{0}{\overbrace{\partial_x f(0,0)}}}{h}
		= -1,\\
	\partial_x\partial_y f(0,0) &= \frac{\partial_y f(h,0) - \overset{0}{\overbrace{\partial_y f(0,0)}}}{h}
		= 1.
\end{align*}
Offensichtlich können wir hier die Ableitungen nicht vertauschen.

\begin{satz}
	\label{10.3.1}
	Sei $D\subset \bbR^2$ offen, $(0,0)\in D$ und $f:D\to \bbR$ wobei $\partial_1 f$, $\partial_2 f$ und
	$\partial_2\partial_1 f$ existieren. Ist $\partial_2\partial_1 f$ im Punkt $(0,0)$ stetig, dann existiert
	auch $\partial_1\partial_2 f(0,0)$ und es ist
	\begin{equation*}
		\partial_1\partial_2 f(0,0) = \partial_2\partial_1 f(0,0).
	\end{equation*}
\end{satz}

\begin{proof}
	Wir betrachten zuerst den Fall, wo $\partial_2\partial_1 f(0,0) = 0$. Dann gilt
	\begin{equation*}
		\partial_1\partial_2 f(0,0) = \lim_{x\to 0}\frac{\partial_2f(x,0) - \partial_2 f(0,0)}{x},
	\end{equation*}
	wobei der Quotient der Grenzwert $y \to 0$ ist von
	\begin{align*}
		q(x,y) &= \frac{1}{x}\left(\frac{f(x,y) - f(x,0)}{y} - \frac{f(0,y) - f(0,0)}{y}\right) \\
		&= \frac{1}{x}(\Phi(x,y) - \Phi(0,y))\\
		&= \partial_1 \Phi(\theta x, y)
\intertext{mit $\theta \in (0,1)$ nach Mittelwertsatz}\\
		&= \frac{\partial_1 f(\theta x, y) - \partial_1 f(\theta x, 0)}{y} \\
		&= \partial_2\partial_1 f(\theta x, \theta' y),
\intertext{$\theta' \in (0,1)$ wie oben.}
	\end{align*}
	Sei $\varepsilon > 0$. Da $\partial_2\partial_1 f$ in $(0,0)$ stetig ist und den Wert $0$ annimmt, existiert ein
	$\delta> 0$, so dass
	\begin{equation*}
		|x| |y| < \delta \Rightarrow |\partial_2\partial_1 f(\theta x,\theta' y) | < \varepsilon.
	\end{equation*}
	
	Also für $|x| |y| < \delta$, gilt $|q(x,y)| < \varepsilon$. Daraus folgt
	\begin{equation*}
		\left| \frac{\partial_2 f(x,0) - \partial_2 f(0,0)}{x} \right|
			= \left| \lim_{y\to 0} q(x,y)\right| \le \varepsilon
	\end{equation*}
	für $|x| < \delta$.
	
	Also ist
	\begin{equation*}
		\partial_1\partial_2 f(0,0) = \lim_{x\to 0}\frac{\partial_2 f(x,0) - f(0,0)}{x} = 0.
	\end{equation*}
	
	Falls $\partial_2\partial_1 f(0,0) = \alpha \ne 0$, dann definieren wir $g(x,y) = f(x,y) - \alpha xy$. Es
	gilt dann, $\partial_2\partial_1 g(0,0) =\partial_2\partial_1 f(0,0) - \alpha = 0$, wobei
	$\partial_2\partial_1 g(x,y) = \partial_2\partial_1 f(x,y) -\alpha$ stetig ist in $(0,0)$. Somit ist
	$\partial_2\partial_1 g(0,0) = \partial_1\partial_2 g(0,0)$ und es folgt die Behauptung.
\end{proof}

Eine Funktion $f:D\subset \bbR^n \to \bbR$ heißt $k$ Mal partiell differenzierbar, wenn $f$ $(k-1)$ Mal
partiell differenzierbar ist und alle Ableitungen $\partial_{i_1} \dots \partial_{i_{k-1}}f$ noch ein Mal partiell
differenzierbar sind. Sind die partiellen Ableitungen von Ordnung $k$ alle stetig, dann heißt $f$ $k$ Mal
\emph{stetig (partiell) differenzierbar} oder von der Klasse $C^k(D)$.

\begin{theorem}
	\label{10.3.2}
	Sei $D\subset \bbR^n$ offen und $f\in C^k(D)$. Dann ist
	\begin{equation*}
		\partial_{i_1}\dots\partial_{i_{k}} f
	\end{equation*}
	unabhängig von der Reihenfolge der Indices $i_1,,\dots,i_k \in \{1,\dots,n\}$.
\end{theorem}

\begin{proof}
	Der Beweis ist einfach Induktion nach $n$, zurückgeführt auf den Beweis von Satz~\ref{10.3.1}.
\end{proof}

Notationen zu partiellen Ableitungen:
\begin{align*}
	\frac{\partial^2}{\partial x\partial y}f &= \partial_x \partial_y f = \partial_y \partial_x\\
	\frac{\partial^2}{\partial x^2} f &= \left(\frac{\partial}{\partial x}\right)^2 f = \partial^2_x f\\
	\frac{\partial^k}{\partial x_{i_1} \dots \partial x_{i_k}} f = \partial_{i_1}\dots \partial_{i_k} f.
\end{align*}

\subsection{Richtungsableitungen}
Eine Funktion $f:D\subset \bbR^n \to \bbR$ hat im Punkt $x \in D$ und in Richtung $k \in \bbR^n$ die
Richtungsableitung
\begin{equation*}
	\partial_k f(x) \coloneqq \frac{\dd }{\dd t} f(x + tk)\Big|_{t=0},
\end{equation*}
falls die rechte Seite existiert.

Die partielle Ableitungen $\partial_i f(x)$ sind die Richtungsableitungen in die Richtungen
$e_i = (0,\dots 1,\dots 0) \in \bbR^n$ der Koordinatenachsen. Existenz aller Richtungsableitungen
$\partial_k f(x)$, $k\in \bbR^n$ garantiert \emph{nicht} die Stetigkeit von $f$ in $x$. Insbesondere folgt aus
partieller Differenzierbarkeit nicht Stetigkeit.

Beispielsweise
\begin{equation*}
	f(x,y) = \frac{xy^2}{x^2 + y^4},\qquad f(0,0) = 0.
\end{equation*}
Es gilt $\partial_1 f(0,0) = 0 = \partial_2 f(0,0)$ und für $a\ne 0$ und $b \ne 0$ gilt für $k = (a,b)$
\begin{equation*}
	f(ta,tb) = \frac{t^3}{t^2 a^2 + t^4 b^4} ab^2 = \frac{t}{a + t^2 b^4} a b^2.
\end{equation*}
Also ist
\begin{equation*}
	\frac{f(ta,tb) - f(0,0)}{t} = \frac{1}{a^2 + t^2 b^4} ab^2 \to \frac{b^2}{a} (t\to 0).
\end{equation*}
D.h.\ alle Richtungsableitungen $\partial_k f$ existieren. Trotzdem ist $f$ nicht stetig in $(0,0)$.

Bemerkung: Partielle Ableitungen treten auch auf bei partiellen Differentialgleichungen (PDG, PDE) wie zum
Beispiel der Wellengleichung
\begin{equation*}
	\frac{1}{c^2} \frac{\partial^2}{\partial t^2} u(x,t) - \frac{\partial^2}{\partial x^2} u(x,t) = 0
\end{equation*}
oder der \emph{Schrödingergleichung}
\begin{equation*}
	i \hbar \frac{\partial}{\partial t} \Psi(x,t) = -\frac{\hbar^2}{2m} \frac{\partial^2}{\partial x^2}\Psi(x,t)
		+ V(x) \Psi(x,t).
\end{equation*}
















