% !TeX root = Ana2.tex

Sei $\gamma_1,\gamma_2:[0,1]\to \bbR^n$ PC\textsuperscript{1}-Kurven mit
$\gamma_2(0) = \gamma_1(1)$, dann sind $\gamma_1 + \gamma_2$ und $-\gamma_1$,
definiert durch
\begin{align*}
    \gamma_1 + \gamma_2 &=
        \begin{cases}
            \gamma_1(t)     & t \in [0,1]\\
            \gamma_2(t - 1) & t \in [1,2]
        \end{cases}\\
%
    -\gamma_1(t) &= \gamma_1(1-t) \quad t \in [0,1],
\end{align*}
wieder PC\textsuperscript{1}-Kurven. Es gilt
\begin{align}
    \label{eq:10.12 H1}
    \int_{\gamma_1 + \gamma_2} v(x) \dd x
        &= \int_{\gamma_1} v(x) \dd x + \int_{\gamma_2} v(x) \dd x\\
    \label{eq:10.12 H2}
    \int_{-\gamma_1} v(x) \dd x &= -\int_{\gamma_1} v(x) \dd x.
\end{align}
Falls $\gamma_2(1) = \gamma_1(1)$, dann ist
\begin{equation*}
    \gamma_1 - \gamma_2 = \gamma_1 + (-\gamma_2)
\end{equation*}
definiert und aus \eqref{eq:10.12 H1} und \eqref{eq:10.12 H2} folgt
\begin{equation*}
    \int_{\gamma_1 - \gamma_2} v(x)\dd x
        = \int_{\gamma_1} v(x)\dd x - \int_{\gamma_2} v(x)\dd x.
\end{equation*}

\begin{proof}
    Beweis von \eqref{eq:10.12 H2}:
    \begin{align*}
        \int_{\gamma_1 + \gamma_2} v(x) \dd x
            &= \int_{0}^{1}\langle v(\gamma_1(t)), \dot{\gamma}_1(t)\rangle \dd t
            	+ \int_{1}^{2}\langle v(\gamma_2(t-1)), \dot{\gamma}_2(t-1)\rangle \dd t \\
            &= \int_{0}^{1}\langle v(\gamma_1(t)), \dot{\gamma}_1(t)\rangle \dd t
            	+ \int_{0}^{1}\langle v(\gamma_2(s)), \dot{\gamma}_2(s)\rangle \dd s \\
        	&= \int_{\gamma_1} v(x) \dd x + \int_{\gamma_2} v(x) \dd x.\qedhere
    \end{align*}
\end{proof}

\begin{satz}
	\label{10.12.2}
	Sei $D \subset \bbR^n$, $\Phi: D \to \bbR$ eine C\textsuperscript{1}-Funktion und
	$\gamma:[a,b] \to D$ eine PC\textsuperscript{1}-Kurve von $A$ nach $B\in D$. Dann ist
	\begin{equation*}
		\int_{\gamma}\nabla \Phi(x) \dd x = \Phi(B) - \Phi(A).
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $a = t_0 < t_1 < \dots < t_n = b$, so gewählt, dass $\gamma_1$ stetig differenzierbar auf $[t_{k-1},t_k]$ ist.
	Dann ist
	\begin{align*}
		\int_{\gamma}\nabla \Phi(x) \dd x
			&= \sum_{k=1}^{n} \int_{t_{k-1}}^{t_k}\langle\nabla\Phi(\gamma(t)),\dot{\gamma}(t)\rangle \dd t \\
			&= \sum_{k=1}^{n} \int_{t_{k-1}}^{t_k}\frac{\dd }{\dd t}\Phi(\gamma(t)) \dd t \\
			&= \sum_{k=1}^{n} \big(\Phi(\gamma(t_k)) - \Phi(\gamma(t_{k-1}))\big)\\
			&= \Phi(\gamma(b)) - \Phi(\gamma(a)) \\
			&= \Phi(B) - \Phi(A). \qedhere
	\end{align*}
\end{proof}

Als Beispiel betrachten wir
\begin{equation*}
	v(x,y) =
	\begin{pmatrix}
		\frac{-y}{x^2 + y^2} \\ \frac{x}{x^2 + y^2}
	\end{pmatrix}
	\qquad \text{in $\bbR^2\setminus\{(0,0)\}$}
\end{equation*}
und
\begin{equation*}
	\gamma(t) =
	\begin{pmatrix}
		\cos(t) \\ \sin(t)
	\end{pmatrix},
	\qquad t \in [0,2\pi].
\end{equation*}
Es ist $\gamma(0) = \gamma(2\pi)$ und damit gilt
\begin{align*}
	\int_{\gamma}v(x) \dd x &= \int_{0}^{2\pi} \langle v(\gamma(t)),\dot{\gamma}(t)\rangle \dd t \\
	&= \int_{0}^{2\pi} \sin^2(t) + \cos^2(t) \dd t = 2\pi.
\end{align*}
Aber wäre $v(x,y) = \operatorname{grad} \Phi$, dann
\begin{equation*}
	\int_{\gamma}v(x) \dd x = \Phi(\gamma(2\pi)) - \Phi(\gamma(0)) = 0 \ne 2\pi.
\end{equation*}
Also ist $v$ kein Gradientenfeld auf $D$.

Leicht lässt sich aber überprüfen, dass $\partial_1 v_2 = \partial_2 v_1$, die Integrabilitätsbedingung ist erfüllt. Somit hat
$v(x,y)$ auf einer sternförmigen Teilmenge $D' \subset D$ ein Potential. $D$ ist nicht sternförmig, aber
$D' = \bbR^2\setminus\big((\infty,0]\times\{0\}\big)$ ist sternförmig. Ein Potential von $v(x,y)$ auf $D'$ ist gegeben durch
\begin{equation*}
	\Phi(x,y) =
	\begin{cases}
		\arctan\frac{x}{y} & x>0\\
		\frac{\pi}{2} - \arctan\frac{x}{y} & y> 0\\
		-\frac{\pi}{2} -\arctan\frac{x}{y} & y<0~.
	\end{cases}
\end{equation*}
$\Phi(x,y)$ ist der Polarwinkel von $(x,y)$.

Damit folgern wir: ist $v : D \to \bbR^n$ ein stetiges Gradientenfeld und $\gamma:[a,b] \to D$ eine geschlossene
PC\textsuperscript{1}-Kurve, dann gilt
\begin{equation*}
	\int_{\gamma}v(x) \dd x = 0.
\end{equation*}
Eine offene, zusammenhängende Teilmenge $D \subset \bbR^n$ heißt Gebiet.

\begin{lemma}
	\label{10.12.3}
	In einem Gebiet $D\subset \bbR^n$ kann jedes Paar $p,q \in D$ durch einen Streckenzug in $D$ verbunden werden. D.h.\ es
	gibt endlich viele Punkte $x_1,\dots, x_n$ mit $x_1 = p$ und $x_n = q$ mit $[x_{k-1},x_k] \in D$ für $k = 2,\dots,n$.
\end{lemma}

\begin{proof}
	Sei $U =\{x\in D \mid \text{es gibt einen Streckenzug der $p$ mit $x$ verbindet.} \}$ und
	$V =\{x\in D \mid \text{es gibt keinen Streckenzug der $p$ mit $x$ verbindet.} \} = D\setminus U$. Dann gilt:
	$D = U \cup V$, $U\cap V = \emptyset$. $U$ ist offen, denn für $x\in U$ folgt $B_\varepsilon(x) \in D$ ($D$ ist offen)
	und $y \in B_\varepsilon(x) \Rightarrow [x,y] \subset B_\varepsilon(x) \subset D$. Also lässt sich $y$ mit $p$ via $x$
	durch die Strecke $[x,y]$ und dem Streckenzug von $p$ nach $x$ verbinden. D.h.\ $y\in U$ und somit
	$B_\varepsilon(x)\subset D$.
	
	$V$ ist offen, denn $x\in V$ und $B_\varepsilon(x)\subset D$. Damit folgt $B_\varepsilon(x) \subset V$, da sonst die
	Aussage im Widerspruch steht. Da $D$ zusammenhängend ist und $U \ne \emptyset$ folgt $V = \emptyset$ und $D = U$.
\end{proof}

\begin{theorem}
	Ein stetiges Vektorfeld $v$ auf einem Gebiet $D\subset \bbR^n$ ist genau dann ein Gradientenfeld, wenn
	\begin{equation*}
		\int_{\gamma} v(x) \dd x = 0
	\end{equation*}
	für jede geschlossene PC\textsuperscript{1}-Kurve $\gamma$ in $D$.
\end{theorem}

\begin{proof}
	Sei $\int_{\gamma} v(x) \dd x = 0$ für jede geschlossene PC\textsuperscript{1}-Kurve $\gamma$ in $D$. Sei
	\begin{equation*}
		\int_{x_1}^{x_2}v(x) \dd x = \int_{\gamma}v(x) \dd x,
	\end{equation*}
	wobei $\gamma$ ein Integrationsweg von $x_1$ nach $x_2$ ist. Diese Definition ist unabhängig von der Wahl von $\gamma$,
	denn für einen anderen Integrationsweg $\hat{\gamma}$ von $x_1$ nach $x_2$ ist $\gamma - \hat{\gamma}$ geschlossen, also
	\begin{equation*}
		\int_{\gamma} v(x) \dd x - \int_{\hat{\gamma}} v(x) \dd x = \int_{\gamma - \hat{\gamma}} v(x) \dd x = 0.
	\end{equation*}
	
	Wir wählen $x \in D$ beliebig und halten $x$ fest. Wir definieren $\Phi: D \to \bbR$ durch
	\begin{equation*}
		\Phi(x) = \int_{x_1}^{x}v(x) \dd x.
	\end{equation*}
	Ist $\Phi$ ist dann für alle $x\in D$ definiert, denn nach Lemma~\ref{10.12.3} gibt es ein Streckenzug von $x_1$ nach $x$
	(also eine PC\textsuperscript{1}-Kurve). Sei $x\in D$ und $\varepsilon > 0$ so klein, dass $B_\varepsilon(x) \subset D$.
	Dann gilt für $|h| < \varepsilon$ $(h\in \bbR)$, dass $x + he_k \in B_\varepsilon(x)\subset D$, $|e_k| = 1$, mit
	$e_k$ dem $k$-ten Basisvektor. Es gilt:
	\begin{align*}
		\Phi(x + he_k) &= \int_{x_1}^{x + h e_k} v(y) \dd y \\
		&= \int_{x_1}^{x} v(y) \dd y + \int_{x}^{x+he_k}v(y)\dd y,
	\end{align*}
	wobei
	\begin{align*}
		\int_{x}^{x+he_k}v(y)\dd y &= \int_{\gamma}v(y) \dd y \\
		&= \int_{0}^{1}\langle v(x+the_k),he_k\rangle \dd t\\
		&= \int_{0}^{1} v_k(x + the_k)h \dd t \\
		&= \int_{0}^{h} v_k(x + se_k) \dd s.
	\end{align*}
	
	Also
	\begin{align*}
		\frac{\partial}{\partial x_k}\Phi(x) &= \frac{\dd}{\dd k} \Phi(x + he_k)\Big|_{h=0}\\
		&= \frac{\dd}{\dd h} \int_{0}^{h} v_k(x + se_k) \dd s \Big|_{h=0} \\
		&= v_k(x + he_k)\Big|_{h=0}  = v_k(x).
	\end{align*}
	Somit $\nabla\Phi = v$.
\end{proof}

Es bleibt die Frage, wie wir prüfen können, dass
\begin{equation*}
	\int_{\gamma} v(x)\dd x = 0
\end{equation*}
für eine geschlossene Kurve $\gamma$ in $D$. Die Integrationsbedingungen $\partial_1 v_2 - \partial_2 v_1 = 0$ sind
notwendig, aber nicht hinreichend, wie das vorherige Beispiel zeigt.







