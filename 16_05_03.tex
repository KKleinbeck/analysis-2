% !TEX root = Ana2.tex

\section{Wann ist die Lösung eines Anfangswertproblem eindeutig?}
Beispiel: Das Anfangswertproblem
\begin{equation*}
	y' = \sqrt{|y|}, \qquad y(0) = 0
\end{equation*}
hat die Lösung $\varphi(x)= 0$ und für jedes $c \le 0$ zusätzlich noch die Lösung
\begin{equation*}
	\varphi(x,c) =
	\begin{cases}
		\frac{1}{4} (x-c)^2 & x \ge 0\\
		0 & x < 0
	\end{cases}.
\end{equation*}
\begin{figure}[!b]
	\centering
	\begin{tikzpicture}
		\begin{axis}[width=0.6\textwidth,
					axis lines=middle,
					xmin=-2,
					xmax=3,]
			\addplot[green!50!black,domain=-2:1,] {0};
			\addplot[green!50!black,domain=1:3,] {0.25*(x-1)^2};
		\end{axis}
	\end{tikzpicture}
	\caption{Skizze einer der möglichen Lösungen zu dem Beispiel.}
\end{figure}

Solche Mehrdeutigkeiten treten auch in der Natur auf und sind \emph{kein} reines akademisches Problem.
Beispielsweise tritt ein ähnliches Problem beim Austreten von Flüssigkeit aus einem Loch in einem Fass auf. Die
Füllhöhe $h$ wird beschrieben durch
\begin{equation*}
	\frac{\dd h}{\dd t} = - c\sqrt{h}.
\end{equation*}
Falls $h(0)=0$ haben wir solche Uneindeutigkeiten, was anschaulich Sinn macht; das Anfangswertproblem sagt uns
nicht, ob zuvor Wasser im Fass war, also $h(t) \equiv \frac{1}{4}(t-c)^2$ mit $t\le c \le 0$, oder es schon
immer leer war.

\begin{satz}
	\label{7.5.1}
	Seien $I, J \in \bbR$ Intervalle, $f, h:I\to\bbR$ stetig und $g:J\to\bbR$ \emph{lipschitzstetig}. Dann ist
	das Anfangswertproblem
	\begin{equation*}
		y' = f(x) g(y) + h(x),\qquad y(x_0 ) = y_0
	\end{equation*}
	mit $x_0 \in I$, $y_0\in J$ lokal eindeutig lösbar. D.h.\ es gibt ein $\varepsilon > 0$, so dass in
	$B_\varepsilon(x_0) \cap I$ genau eine Lösung existiert.
\end{satz}

\begin{proof}
	Wir beweisen nur die Eindeutigkeit, nicht die Existenz. Seien
	$\varphi, \tilde{\varphi}:[x_0,x_1] \to \bbR$, $x_1 > x_0$ zwei Lösungen des Anfangswertproblems und sei
	\begin{equation*}
		M \coloneqq \sup_{x\in[x_0,x_1]} |f(x)| < \infty\qquad (\text{$f$ stetig}).
	\end{equation*}
	Sei $|g(y) - g(\tilde{y})| \le L |y - \tilde{y}|$ für $y,\tilde{y} \in J$. Dann gilt
	\begin{align*}
		\varphi(x) &= \varphi(x_0) + \int_{x_0}^{x} \varphi'(t)\dd t \\
		&= y_0 + \int_{x_0}^{x} f(t) g(\varphi(t)) + h(t)\dd t
	\end{align*}
	und ebenso für $\tilde{\varphi}$. Also gilt für $x\in [x_0,x_1]$
	\begin{align*}
		\varphi(x) - \tilde{\varphi}(x)
			&= \int_{x_0}^{x} f(t)\left[g(\varphi(t)) - g(\tilde{\varphi}(x)) \right] \dd t\\
		\Rightarrow |\varphi(x) - \tilde{\varphi}(x)|
			&\le L \int_{x_0}^{x} f(t) |\varphi(x) -\tilde{\varphi}(x)|\dd t\\
		&\le ML \int_{x_0}^{x} |\varphi(x) -\tilde{\varphi}(x)|\dd t.
	\end{align*}
	Sei $\varepsilon>0$ mit $\varepsilon ML<1$ und sei
	$N = \sup_{x_0\le x \le x_0+\varepsilon}|\varphi(x)-\tilde{\varphi}(x)|$. Dann gilt für
	$x \in [x_0,x_0+\varepsilon]$
	\begin{align*}
		N &= \sup_{x_0 \le x \le x_0+\varepsilon}|\varphi - \tilde{\varphi}|\\
		&\le  \sup_{x_0\le x \le x_0+\varepsilon} \int_{x_0}^{x} MLN\dd t \le \varepsilon M L N.
	\end{align*}
	Wegen $\varepsilon ML < 1$ folgt $N = 0$. Analog zeigt man die Eindeutigkeit in einem Intervall
	$[x_0-\varepsilon,x_0]$.
\end{proof}

Unseres erstes Beispiel war $g(y) = \sqrt{|y|}$. $g$ ist \emph{nicht} lipschitzstetig auf $\bbR$, allerdings
z.B.\ in $[\frac{1}{2},\infty]$. Betrachten wir also das Anfangswertproblem
\begin{equation*}
	y' = \sqrt{|y|},\qquad y(0) = 1
\end{equation*}
eindeutig lösbar nach Satz~\ref{7.5.1}.
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,
					xmin=-3,
					xmax=2,]
			\addplot[blue,domain=-3:-2] {0};
			\addplot[blue,domain=-2:2] {0.25*(x+2)^2};
		\end{axis}
	\end{tikzpicture}
	\caption{Lösung des Beispiels. Für $x > -2$ ist die Lösung eindeutig und nur in diesem Bereich.}
\end{figure}

Als weiters Beispiel betrachten wir
\begin{equation*}
	y' = y^2,\qquad y(0) = 1
\end{equation*}
hat die eindeutige Lösung
\begin{equation*}
	\varphi(x) = \frac{1}{1-x}.
\end{equation*}
Satz~\ref{7.5.1} ist anwendbar mit $f = 1$, $h = 0$ auf $\bbR$ und $g(y) = y^2$ auf einem kompakten Intervall
lipschitzstetig.

Bemerkung: Die eindeutige Lösung aus Satz~\ref{7.5.1} kann bis zum Rand des Gebiets $I\times J$ fortgesetzt
werden.







\chapter{Funktionenfolgen}
\section{Gleichmäßige Konvergenz}
Sei $f_n(x) \to f(x)$ $(n\to\infty)$ für alle $x \in [a,b]$. Die Kernfrage ist: übertragen sich die
Kerneigenschaften (Stetigkeit, Differenzierbarkeit,etc.) von $f_n$ auf $f$? Gilt
\begin{equation*}
	\lim_{n\to\infty} \int_{a}^{b} f_n(x)\dd x = \int_{a}^{b} \lim_{n\to\infty} f_n \dd x?
\end{equation*}

\begin{definition}
	Eine Folge von Funktionen $f_n:D\to \bbC$ konvergiert \emph{punktweise} gegen $f:D\to\bbC$, wenn
	\begin{equation*}
		\lim_{n\to\infty} f_n(x) = f(x)
	\end{equation*}
	für alle $x\in D$.
	
	Die Folge konvergiert \emph{gleichmäßig} gegen $f$, wenn
	\begin{equation*}
		\supnorm{f_n - f} = \sup|f_n(x) - f(x)| \to 0,\qquad n\to\infty.
	\end{equation*}
	Mit anderen Worten gibt es zu jedem $\varepsilon > 0$ ein $N_\varepsilon \in \bbN$, so dass
	\begin{equation*}
		n \ge N_\varepsilon, x \in D\Rightarrow |f_n(x) - f(x)| < \varepsilon.
	\end{equation*}
	$N_\varepsilon$ hängt nicht von $x\in D$ ab.
\end{definition}

Bemerkung: Wenn $f_n$ gleichmäßig gegen $f$ konvergiert, dann konvergiert $f_n$ auch punktweise gegen $f$. Die
Umkehrung gilt nicht.

Beispiele
\begin{enumerate}
	\item $f_n(x) = x^n$ konvergiert auf $[0,1]$ punktweise gegen
		\begin{equation*}
			f(x) =
			\begin{cases}
				0 & x \in [0,1)\\
				1 & x = 1
			\end{cases}.
		\end{equation*}
		Die Konvergenz ist nicht gleichmäßig. $f_n$ ist differenzierbar, aber $f$ ist nicht stetig.
	
	\item $f_n(x) = x^n(1-x)$ konvergiert gleichmäßig auf $[0,1]$ gegen $f(x)=0$.
		Beweis: Wir bestimmen $\max f_n(x)$. $f'_n(x) = 0$ für $x = \frac{n}{n+1}$. Also, da $f_n \ge 0$,
		folgt $0 < f_n(x) \le f_n\left(\frac{n}{n+1}\right) \le \frac{1}{n+1}$. Damit ist
		$\sup |f_n - f| \le \frac{1}{n+1} \to 0$ für $n\to\infty$.
		
	\item $f_n(x) = \sqrt{x^2 + \frac{1}{n}}$ konvergiert auf $\bbR$ gleichmäßig gegen $f(x) = |x|$, denn
		$|x| \le f_n(x) \le |x| + \frac{1}{\sqrt{n}}$. Damit ist $\sup|f_n - f| \le \frac{1}{\sqrt{n}} \to 0$
		für $n\to\infty$. $f_n$ ist differenzierbar, $f$ ist stetig, aber nicht differenzierbar.
\end{enumerate}
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[width=0.5\textwidth,
					axis lines=middle,
					xmin=0,
					xmax=1]
			\addplot[orange,domain=0:1] {x};
			\addplot[blue,domain=0:1] {x^2};
			\addplot[violet,domain=0:1] {x^3};
			\addplot[green!50!black,domain=0:1] {x^4};
		\end{axis}
	\end{tikzpicture}
	\begin{tikzpicture}
		\begin{axis}[width=0.5\textwidth,
					axis lines=middle,
					xmin=0,
					xmax=1]
			\addplot[orange,domain=0:1] {x*(1-x)};
			\addplot[blue,domain=0:1] {x^2*(1-x)};
			\addplot[violet,domain=0:1] {x^3*(1-x)};
			\addplot[green!50!black,domain=0:1] {x^4*(1-x)};
		\end{axis}
	\end{tikzpicture}
	\begin{tikzpicture}
		\begin{axis}[width=0.5\textwidth,
					axis lines=middle,
					xmin=-2,
					xmax=2]
			\addplot[orange,domain=-2:2] {sqrt(x^2 + 1)};
			\addplot[blue,domain=-2:2] {sqrt(x^2 + 0.5)};
			\addplot[violet,domain=-2:2] {sqrt(x^2 + 0.3)};
			\addplot[green!50!black,domain=-2:2] {sqrt(x^2 + 0.25)};
			\addplot[domain=-2:2] {sqrt(x^2)};
		\end{axis}
	\end{tikzpicture}
	\caption{Graphen zu den Beispielen $x^n$, $x^n(1-x)$ und $\sqrt{x^2 + \frac{1}{n}}$.}
\end{figure}

\begin{satz}
	\label{8.1.1}
	Eine Folge $f_n:D\to\bbC$ ist genau dann gleichmäßig konvergent, wenn zu jedem $\varepsilon > 0$ ein
	$N \in \bbN$ existiert, so dass
	\begin{equation*}
		n,m\ge N \Rightarrow \supnorm{f_n - f_m} < \varepsilon.
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $f_n\to f$ $(n\to\infty)$ gleichmäßig konvergent und sei $\varepsilon >0$. Dann existiert $N\in \bbN$,
	so dass
	\begin{equation*}
		n > N \Rightarrow \supnorm{f_n - f} < \frac{\varepsilon}{2}.
	\end{equation*}
	Also
	\begin{align*}
		n,m > N \Rightarrow \supnorm{f_n - f_m} &= \supnorm{(f_n-f) + (f-f_m)}\\
		&\le \supnorm{f_n - f} + \supnorm{f_m - f}\\
		&< \frac{\varepsilon}{2} + \frac{\varepsilon}{2}\\
		& = \varepsilon.
	\end{align*}
	
	Sei nun $(f_n)$ eine Cauchy-Folge bezüglich der Supremumsnorm, d.h.\ $\supnorm{f_n - f_m} < \varepsilon/2$
	gilt für alle $n,m > N$. Dann ist für jedes $x\in D$, $(f_n(x))$ eine Cauchy-Folge in $\bbC$. Also existiert
	\begin{equation*}
		f(x) = \lim_{n\to\infty} f_n(x).
	\end{equation*}
	Dadurch wird $f:D\to \bbC$ definiert. Zu zeigen ist, dass $f_n \to f$ gleichmäßig konvergiert,
	
	Damit gilt für $n \ge N$ und alle $x\in D$
	\begin{align*}
		|f_n(x) - f(x)| &= \lim_{m\to\infty} |f_n(x)-f_m(x)|\\
		&\le \frac{\varepsilon}{2} < \varepsilon.\qedhere
	\end{align*}
\end{proof}













