% !TEX root = Ana2.tex

\todo{sinus curve mit gefüllter flache und mittelwert mit gefüllter fläche darunter zeichen: Veranschaulichung d. mittelwertsatzes -> fillbetween zum laugen bekommen}

\begin{lemma}
	\label{6.3.5}
	Ist $f:[a,b] \to \bbR$ eine Regelfunktion mit
	\begin{equation*}
		\int_{a}^{b} f \dd x = 0, f \ge 0, a< b,
	\end{equation*}
	dann gilt $f(x_0) = 0$ an jeder Stetigkeitsstelle $x_0$.
\end{lemma}

\begin{proof}
	Sei $x_0$ eine Stetigkeitsstelle und $f(x_0) >0$. Dann existiert ein Intervall $[\alpha,\beta] \subset
	[a,b]$ mit $f(x) > f(x_0)/2$ für $x \in [\alpha,\beta]$. Für $\varphi:[a,b]\to \bbR$ die Treppenfunktion
	mit
	\begin{equation*}
		\varphi \equiv
		\begin{cases}
			f(x_0)/2 & x \in [\alpha,\beta] \\
			0 & \text{sonst}
		\end{cases}.
	\end{equation*}
	Dann gilt $f \ge \varphi$, also
	\begin{equation*}
		\int_{a}^{b} f \dd x\ge \int_{a}^{b} \varphi \dd x = (\beta-\alpha) \frac{f(x_0)}{2} > 0.
	\end{equation*}
	Das steht zum Widerspruch zu $\int_{a}^{b} f \dd x = 0$.
\end{proof}

\subsection{Riemannsche Summen}
Ist $a = x_0 < x_1 < \dots < x_n = b$ eine Partition von $[a,b]$ und sind $t_k \in [x_{k-1},x_k]$ beliebig
gewählte Zwischenpunkte (Stützstellen), dann heißt die Summe
\begin{equation*}
	\sum_{k=1}^{n} f(t_k) (x_k - x_{k-1})
\end{equation*}
\emph{Riemannsumme} von $f$ bezüglich der Partition $\{x_a,\dots,x_n\}$. Die maximale Länge der Teilintervalle
$\max_k (x_k-x_{k-1})$ heißt \emph{Feinheit} der Partition.

\begin{satz}
	\label{6.3.6}
	Ist $f:[a,b]\to \bbR$ eine Regelfunktion, dann gibt es zu jedem $\varepsilon > 0$ ein $\delta >0$ mit der
	Eigenschaft: Für jede Partition von $[a,b]$ mit Feinheit $<\delta$ und für jede Wahl der Zwischenpunkte
	gilt
	\begin{equation*}
		\left| \sum_{k=1}^{n} f(t_k) \Delta x_k - \int_{a}^{b} f \dd x\right| < \varepsilon.
	\end{equation*}
\end{satz}

\begin{proof}
	Wir nehmen zuerst an, $f$ sei eine Treppenfunktion. Dann ist $f$ stückweise konstant mit einer endlichen
	Anzahl $N$ an Sprungstellen. Sei $\varepsilon > 0$, wir wählen
	$\delta = \frac{\varepsilon}{4N\supnorm{f}}$. Sei $\{x_0,\dots,x_n\}$ eine Partition des Intervalls
	$[a,b]$ mit Feinheit $< \delta$ und seien die Stützstellen $t_k$ beliebig gewählt. Dann gilt
	\begin{align}
		\label{eq: 6.2.6 Hilfe 1}
		\sum_{k=1}^{n} f(t_k) (x_k - x_{k-1}) &= \int_{a}^{b} \varphi \dd x\\
		\label{eq: 6.2.6 Hilfe 2}
		\int_{a}^{b} f\dd x &= \int_{a}^{b} g \dd x,
	\end{align}
	wobei
	\begin{equation*}
		\varphi = \sum_{k=1}^{n} f(t_k)\Delta x_k
	\end{equation*}
	und
	\begin{equation*}
		g = \sum_{k=1}^{n} f \Delta x_k
	\end{equation*}
	d.h. es gilt $g(x) = f(x)$ für alle $x \ne x_k$.
	
	Gleichung \eqref{eq: 6.2.6 Hilfe 1} folgt aus der Definition von $\int_{a}^{b} \varphi \dd x$ und
	\eqref{eq: 6.2.6 Hilfe 2} folgt von Blatt 2. Ausserdem
	\begin{align*}
		\left| \int_{a}^{b} \varphi \dd x - \int_{a}^{b} g \dd x\right| &=
			\left| \int_{a}^{b} \varphi - g \dd x \right|\\
		&= \left| \sum_{k=1}^{n} \int_{x_{k-1}}^{x_k} f(t_k) - f \dd x\right|,
	\end{align*}
	wobei das Integral
	\begin{equation*}
		\int_{x_{k-1}}^{x_k} f(t_k) - f \dd x = 0,
	\end{equation*}
	wenn $f$ keine Sprungstelle in $[x_{k-1},x_k]$ hat. Ansonsten gilt
	\begin{equation*}
		\int_{x_{k-1}}^{x_k} f(t_k) - f \dd x \le (x_k - x_{k-1}) 2 \supnorm{f} < 2\delta \supnorm{f}.
	\end{equation*}
	Also
	\begin{align*}
		\left|\int_{a}^{b} f \dd x - \int_{a}^{b} g \dd x\right|
			&\le \sum_{k=1}^{n} \left| \int_{x_{k-1}}^{x_k} f(t_k) - f \dd x\right|\\
		&< 2N\cdot 2\delta \supnorm{f}\\
		&= 4N\supnorm{f}\delta = \varepsilon.	
	\end{align*}
	
	Betrachten wir nur den Fall, dass $f$ eine Regelfunktion ist. Sei $\varepsilon >0$. Nach
	Theorem~\ref{6.2.2} existiert eine Treppenfunktion $\varphi$ mit
	$\supnorm{f - \varphi} < \frac{\varepsilon}{3(b-a)}$ und nach obigen existiert ein $\delta > 0$ mit der
	Eigenschaft
	\begin{equation*}
		\left| \sum \varphi(t_k)(x_k - x_{k-1}) - \int_{a}^{b} \varphi \dd x\right|
			< \frac{\varepsilon}{3},
	\end{equation*}
	wenn $\max(x_k - x_{k-1}) < \delta$. Da
	\begin{align*}
		\left|\sum \varphi(t_k) (x_k - x_{k-1}) - \sum f(t_k) (x_k-x_{k-1})\right|
			&\le \sum_{k=1}^{n} (x_k - x_{k-1}) | \varphi(t_k) - f(t_k)| \\
		&\le \supnorm{\varphi - f}(b-a) < \frac{\varepsilon}{3}
	\end{align*}
	und ebenso
	\begin{align*}
		\left|\int_{a}^{b} \varphi \dd x - \int_{a}^{b} f \dd x\right| &\le (b-a) \supnorm{\varphi - f}\\
			&< \frac{\varepsilon}{3}.
	\end{align*}
	Also folgt
	\begin{equation*}
		\left|\sum f(t_k) (x_k - x_{k-1}) - \int_{a}^{b} f \dd x\right| < 3\frac{\varepsilon}{3} =
			\varepsilon. \qedhere
	\end{equation*}
\end{proof}

\section{Hauptsatz der Differential- und Integralrechnung}
\begin{theorem}
	\label{6.4.1}
	Sei $f:I\to \bbC$ eine Regelfunktion und $a \in I$. Dann ist
	\begin{equation*}
		F(x) = \int_{a}^{x} f(t) \dd t
	\end{equation*}
	stetig, und in jedem Punkt $x_0 \in I$ wo $f$ stetig ist, ist $F$ differenzierbar mit $F'(x_0) = f(x_0)$.
\end{theorem}

\begin{proof}
	Auf jeden kompakten Teilintervall $[a,b] \subset I$ ist $f$ beschränkt (Korollar~\ref{6.2.3}) und $F$
	ist Lipschitz-stetig (Aufgabe 2.4), also stetig. Sei $f$ stetig in $x_0$. Dann gilt
	\begin{align*}
		\left|\frac{F(x_0 + h) - F(x_0)}{h} - f(x_0)\right| &=
			\left|\frac{1}{h}\int_{x_0}^{x_0 + h} f(x) \dd x - f(x_0)\right|\\
		&= \left| \frac{1}{h} \int_{x_0}^{x_0 + h} f(x) - f(x_0) \dd x \right|\\
		&\le \sup_{|x-x_0|\le |h|}|{f(x) - f(x_0)}| \to 0 \quad(h\to 0)
	\end{align*}
	nach Satz~\ref{6.3.2}. \qedhere
\end{proof}

Jede differenzierbare Funktion $F: I \to \bbC$, wobei $I \subset \bbR$ ein Intervall und $F' = f$ ist, heißt
\emph{Stammfunktion} von $f$. Mit $F$ ist $F + c$ ($c\in \bbC$) eine Stammfunktion von $f$ und zwei
verschiedene Stammfunktionen $F_1$, $F_2$ unterscheiden sich ausschließlich um eine Konstante, denn
$(F_1 - F_2)' = 0$; also $F_1 - F_2 = c$. Nach Theorem~\ref{6.4.1} hat jede \emph{stetige} Funktion
$f:I\to\bbC$ eine Stammfunktion.