% !TEX root = Ana2.tex

$A$ ist genau dann negativ definit, wenn $-A$ positiv definit ist, also wenn
\begin{equation*}
	(-1)^k
	\det
	\begin{pmatrix}
		A_{11} & \dots & A_{1k}\\
		\vdots & & \vdots \\
		A_{k1} & \dots & A_{kk}
	\end{pmatrix}
	> 0
\end{equation*}
d.h.\ wenn $A_{11} < 0$,
\begin{equation*}
	\det
	\begin{pmatrix}
		A_{11} & A_{12} \\ A_{21} & A_{22}
	\end{pmatrix}
	> 0
	\qquad
	\det
	\begin{pmatrix}
		A_{11} & A_{12} & A_{13} \\
		A_{21} & A_{22} & A_{23} \\
		A_{31} & A_{32} & A_{33}
	\end{pmatrix}
	< 0
	\qquad \dots
\end{equation*}

\begin{theorem}
	\label{10.8.3}
	Sei $D\subset \bbR^n$ offen, $f:D\to \bbR$ zwei Mal stetig differenzierbar und $a \in D$ ein kritischer Punkt
	von $f$.
	\begin{enumerate}
		\item Ist $H_f(a)$ positiv definit, dann hat $f$ in $a$ ein striktes lokales Minimum.
		
		\item Ist $H_f(a)$ negativ definit, dann hat $f$ in $a$ ein striktes lokales Maximum.
		
		\item Ist $H_f(a)$ indefinit, dann hat $f$ in $a$ ein kein lokales Extremum.
	\end{enumerate}
\end{theorem}

\begin{proof}
	Nach Theorem~\ref{10.8.1} gilt
	\begin{equation*}
		f(a+x) = f(a) + \frac{1}{2}\langle x,H_f(a)x\rangle + |x^2| R(a,x)
	\end{equation*}
	mit $R(a,x) \to 0$ für $|x| \to 0$.
	
	\begin{enumerate}
		\item Ist $H_f(a)$ positiv definit, dann existiert der kleinste Eigenwert $c > 0$, so dass
			$\langle x,H_f(a)x\rangle \ge c|x^2|$. Sei $\delta > 0$ so klein, dass $B_\delta(a)\subset D$ und
			$|x| < \delta \Rightarrow R(a,x) < c/2$. Dann gilt für $|x| < \delta$
			\begin{align*}
				f(a+x) &\ge f(a) + \usb{>0}{\left(\frac{c}{2} + R(a,x)\right)}|x|^2\\
					&> f(a).
			\end{align*}
			Also hat $f$ in $a$ ein lokales Minimum.
		
		\item Erfolgt analog.
		
		\item Ist $H_f(a)$ indefinit, dann existiert $x_{\pm}\in \bbR^n$, $|x_{\pm}| = 1$, so dass
			$c_+ = \langle x_+, H_f(a)x_+\rangle > 0$ und $c_- = \langle x_-,H_f(a)x_-\rangle < 0$.
			Wähle $\delta$ so klein, dass $B_\delta(a) \subset D$ und
			$|x| < \delta \Rightarrow |R(a,x)| < \min \{ c_+/2, -c_-/2 \}$. Dann gilt für $|t| < \delta$
			\begin{equation*}
				f(a+tx_+) = f(a) + t^2\left(\frac{1}{2}\usb{c_+}{\langle x_+,H_f(a)x_+\rangle}
					+ \usb{< c_+/2}{R(a,tx_+)} \right) > f(a).
			\end{equation*}
			Analog
			\begin{equation*}
				f(a+tx_-) = f(a) + t^2\left(\frac{1}{2}\usb{c_-}{\langle x_-,H_f(a)x_-\rangle}
					+ \usb{< c_-/2}{R(a,tx_-)} \right) < f(a).
			\end{equation*}
	\end{enumerate}
\end{proof}

Beispiele
\begin{enumerate}
	\item Für $f(x,y) = x^2 + y^2 + 3xy$ gilt $\nabla f(0,0) = (0,0)^T$,
		\begin{equation*}
			f''(0,0) =
			\begin{pmatrix}
				2 & 3 \\ 3 & 2
			\end{pmatrix}.
		\end{equation*}
		Der Punkt $(0,0)$ ist ein kritischer Punkt von $f$, aber $(0,0)$ ist kein lokales Extremum, da
		$2 > 0$ aber $2^2 - 3^2 < 0$.
	
	\item Die Funktion
		\begin{equation*}
			f(x,y) = (y-x^2)(y-2x^2) = 2x^4 - 3x^2y + y^2
		\end{equation*}
		hat den kritischen Punkt $(0,0)$ und dort die positiv semidefinite Hessematrix
		\begin{equation*}
			f''(0,0) =
			\begin{pmatrix}
				0 & 0 \\ 0 & 2
			\end{pmatrix}.
		\end{equation*}
		Es liegt kein Minimum vor, wie an Abbildung~\ref{10.8:Bereiche} dargestellt ist.
\end{enumerate}

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,
					xmin=-2,
					xmax=2,
					xlabel=$x$,
					ylabel=$y$,]
			\addplot[blue,domain=-2:2,samples=30] {x^2} node[anchor=west] {$y = x^2$};
			\addplot[orange,domain=-2:2,samples=30] {2*x^2} node[anchor=west] {$y = 2 x^2$};
		\end{axis}
	\end{tikzpicture}
	\caption{Bereiche in denen $f$ positiv und negativ sind.}
	\label{10.8:Bereiche}
\end{figure}
\todo{Bereiche einfärben}

\section{Taylorsche Formel}
Multiindices: Für $\alpha = (\alpha_1, \dots, \alpha_n) \in \bbN_0^n$, $x \in \bbR^n$ und $f \in C^m(D)$,
$D \subset \bbR^n$ definiert man
\begin{align*}
	|\alpha| &= \sum_{i=1}^{n} \alpha_i,\qquad \alpha! = \sum_{i=1}^{n} \alpha_i! \\
%
	x^\alpha &= \prod_{i=1}^{n}x_i^{\alpha_i}\\
%
	\partial_\alpha f = \frac{\partial^{|\alpha|}}{\partial_{x_1}^{\alpha_1}\dots \partial_{x_n}^{\alpha_n}} f
		= \partial_1^{\alpha_1}\dots \partial_n^{\alpha_n} f.
\end{align*}

\begin{lemma}
	\label{10.9.1}
	Sei $f\in C^m(D)$, $D\subset \bbR$ offen und $[a,a+x] \subset D$. Dann ist
	$t \mapsto f(a+tx)$ $m$ Mal stetig differenzierbar auf $[0,1]$ und es gilt
	\begin{equation*}
		\frac{1}{m!}\left(\frac{\dd}{\dd t}\right)^m f(a+tx) =
			\sum_{|\alpha| = m} \frac{1}{\alpha!} (\partial^\alpha f)(a+tx) x^\alpha.
	\end{equation*}
\end{lemma}

\begin{proof}
	$t \mapsto f(a+tx)$ ist differenzierbar und nach Kettenregel
	\begin{align*}
		\frac{\dd}{\dd t} f(a+tx) &= \sum_{k=1}^{n}\partial_k f(a+tx) \usb{x_k}{\frac{\dd}{\dd t} (a_k + tx_k)}\\
		\frac{\dd^2}{\dd t^2} f(a+tx) &=
			\sum_{k_1 = 1}^{n}\sum_{k_2=1}^{n} \partial_{k_1}\partial_{k_2} f(a+tx) x_{k_1}x_{k_2}
	\end{align*}
	etc. Damit kommen wir zu
	\begin{equation*}
		\left(\frac{\dd}{\dd t}\right)^m f(a+tx) = \sum_{k_1\dots k_m = 1}^{n} \partial_{k_1}\dots\partial_{k_m}
			f(a+tx) x_{k_1}\dots x_{k_m}.
	\end{equation*}
	Nach dem Satz von Schwarz ist
	\begin{equation*}
		\partial_{k_1}\dots\partial_{k_m} f = \partial_1^{\alpha_1}\dots \partial_n^{\alpha_n} f
			= \partial^\alpha f,
	\end{equation*}
	wobei $\alpha_k$ angibt wie oft der Wert $k$ in $k_1, \dots, k_m$ vorkommt. Es gibt
	\begin{equation*}
		\frac{m!}{\alpha_1!\dots \alpha_n!} = \frac{m!}{\alpha!}
	\end{equation*}
	Permutationen von $(k_1,\dots,k_m)$ welche auf verschiedene $m$-Tupel $(k_1,\dots,k_m)$ führen. Somit gibt es
	$m!/\alpha!$ Terme in der Ableitung $\dd^m/\dd t^m$, welche mit $\partial^\alpha f(a+tx)$ übereinstimmen.
	
	Also können wir $\dd^m/\dd t^m f(a+tx)$ schreiben als
	\begin{equation*}
		\sum_{|\alpha| = m} \frac{m!}{\alpha!} \partial^\alpha f(a+tx) x^\alpha. \qedhere
	\end{equation*}
\end{proof}

\begin{theorem}
	\label{10.9.2}
	Sei $f \in C^m(D)$, $D\subset \bbR^n$ offen und $[a,a+x] \subset D$. Dann gilt
	\begin{equation*}
		f(a+x) = \sum_{|\alpha|\le m - 1} \frac{1}{\alpha!}(\partial^\alpha f)(a)x^\alpha + R_m(a,x),
	\end{equation*}
	wobei
	\begin{align*}
		R(a,x) &=
			m\int_{0}^{1}(1-t)^{m-1} \sum_{|\alpha| = m} \frac{1}{\alpha!}(\partial^\alpha f)(a + tx) x^\alpha \dd t\\
		&= \sum_{|\alpha| = m} \frac{1}{\alpha!} (\partial^\alpha f)(a + \tau x) x^\alpha,\qquad \tau \in (0,1).
	\end{align*}
\end{theorem}

\begin{proof}
	Wir wenden die Taylorformel mit Integralrestglied (Theorem~\ref{6.5.2}) bzw.\ mit Lagrange-Restglied
	(Theorem 5.7.1) auf die Funktion $g(t) = f(a+tx)$ für festes $x\in \bbR^n$ an
	\begin{equation*}
		g(1) - \sum_{k=0}^{m-1} \frac{1}{k!}g^{(k)}(0)
			= \frac{1}{(m-1)!} \int_{0}^{1}(1-t)^{m-1}g^{(m)}(t) \dd t
			= \frac{1}{m!} g^{(m)}(\tau),
	\end{equation*}
	mit $\tau \in (0,1)$.
\end{proof}

\begin{cor}
	\label{10.9.3}
	Unter der Voraussetzungen von Theorem~\ref{10.9.2} gilt
	\begin{equation*}
		f(a+x) = \sum_{|\alpha| \le m} \frac{1}{\alpha!} (\partial^\alpha f)(a) x^\alpha + o(|x|^m),
			\qquad (x \to 0).
	\end{equation*}
\end{cor}

\begin{proof}
	\begin{equation*}
		\left|\sum_{|\alpha|\le m} \frac{1}{\alpha!}
			\left(\partial^\alpha f(a + \tau x) - \partial^\alpha f(a) \right)\frac{x^\alpha}{|x|^m}\right|
			\le \sum_{|\alpha| = m} \usb{\le 1}{\left|\frac{x^\alpha}{|x|^m}\right|}
				\left|\partial^\alpha f(a+\tau x) - \partial^\alpha f(a)\right| \to 0
	\end{equation*}
	für $x \to 0$, denn $f \in C^m(D)$ und somit ist $\partial^\alpha f$ stetig für $|\alpha| = m$.
\end{proof}

\section{Integrale mit Parametern}
Wir betrachten jetzt Funktionen $F:D\subset \bbR^n \to \bbR$ der Form
\begin{equation*}
	F(x) = \int_{a}^{b} f(x,t) \dd t
\end{equation*}
mit einer stetigen Funktion $f:D\times[a,b]\to \bbR$.

\begin{satz}
	\label{10.10.1}
	Seien $D\subset \bbR^n$ offen und sei $f:D\times [a,b]\to \bbR$ stetig. Dann ist die Funktion
	\begin{equation*}
		F(x) = \int_{a}^{b} f(x,t)\dd t
	\end{equation*}
	stetig auf $D$.
\end{satz}

\begin{proof}
	Sei $x_0\in D$ und $r > 0$ so klein, dass $\overline{B_r(x_0)} \subset D$.
	$\overline{K} \coloneqq \overline{B_r(x_0)}$ ist kompakt und somit ist auch $K \times [a,b]$ abgeschlossen
	und beschränkt, also kompakt. $f$ ist auf $K\times[a,b]$ gleichmäßig stetig (Theorem~\ref{10.1.4}).
	Zu $\varepsilon >0$ existiert ein $\delta > 0$ mit
	\begin{equation*}
		|(x',t') - (x,t)| <\delta \Rightarrow |f(x',t') - f(x,t)| < \frac{\varepsilon}{b-a}.
	\end{equation*}
	Also gilt für $x',x \in K$ mit $|x' - x| < \delta$
	\begin{equation*}
		|F(x') - F(x)| \le \int_{a}^{b}\left|f(x',t) - f(x,t)\right| \dd t 
			< \varepsilon.\qedhere
	\end{equation*}
\end{proof}

\begin{satz}
	\label{10.10.2}
	Sei $D \subset \bbR^n$ offen, $k\in \{1,\dots,n\}$, und $f,\partial_k f:D\times[a,b]\to \bbR$ seien stetig.
	Dann hat die Funktion $F$ die stetige partielle Ableitung
	\begin{equation*}
		\partial_k F(x) = \int_{a}^{b}\partial_k f(x,t)\dd t.
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $x\in D$ und $r > 0$ so klein, dass $\overline{B_r(x)}\subset D$. Dann ist $x + h e_k \in D$ für
	$|h| \le r$ und
	\begin{align*}
		\frac{1}{h}\left(F(x+he_k) - F(x) \right) &- \int_{a}^{b} \partial_k f(x,t) \dd t\\
		&= \frac{1}{h} \int_{a}^{b} f(x+he_k,t) -f(x,t) \dd t - \int_{a}^{b}\partial_k f(x,t) \dd t\\
		&= \int_{a}^{b} \partial_k f(x+\theta h e_k,t) - \partial_k f(x,t)\dd t,\qquad \theta \in (0,1).
	\end{align*}
	Also
	\begin{equation*}
		|x| \le \int_{a}^{b} |\partial_k f(x + \theta h e_k,t) - \partial_k f(x,t)| \dd t < \varepsilon
	\end{equation*}
	für $|h| < \delta$ denn $\partial_k f$ ist gleichmäßig stetig auf $\overline{B_r(x)}\times[a,b]$
	und somit ist
	\begin{equation*}
		|\partial_k f(x+\theta h e_k,t) - \partial_kf(x,t)| < \frac{\varepsilon}{2(b-a)}
	\end{equation*}
	für $|h| < \delta$, $\delta$ klein genug.
\end{proof}

\begin{theorem}
	\label{10.10.3}
	Leibnizsche Regel: Seien $I,J \subset \bbR$ offene Intervalle, $f, \partial_x f: I\times J \to \bbR$ stetig
	und $a,b:I\to J$ differenzierbar. Dann ist
	\begin{equation*}
		F(x) = \int_{a(x)}^{b(x)}f(x,t) \dd t
	\end{equation*}
	auf $I$ differenzierbar und
	\begin{equation*}
		F'(x) = f(x,b(x))b'(x) - f(x,a(x))a'(x) + \int_{a(x)}^{b(x)}\partial_1 f(x,t)\dd t.
	\end{equation*}
\end{theorem}





















