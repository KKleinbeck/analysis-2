% !TEX root = Ana2.tex

\begin{proof}
	Wenn $|| A|| < 1$, dann ist $\sum_{k=0}^{\infty} A^k$ konvergent. Der Beweis dafür folgt aus Blatt 9. Sei
	$\sum_{k=0}^{\infty}A^k$ konvergent. Dann gilt
	\begin{align*}
		(E_n - A) \sum_{k=0}^{\infty} A^k &= \lim\limits_{N\to\infty} (E_n - A) \sum_{k=0}^{N} A^k\\
		&= \lim_{N\to\infty}\sum_{k=0}^{N} (E_n - A) A^k\\
		&= \lim_{N\to\infty}\sum_{k=0}^{N} A^k - A^{k+1}\\
		&= \lim_{N\to\infty}\left[(E_n-A) + (A - A^2) + \dots + (A^N - A^{N+1})\right]\\
		&= \lim_{N\to\infty} E_n - A^{N+1}\\
		&= E_n,
	\end{align*}
	wegen der Konvergenz der Reihe.
	
	Also
	\begin{equation*}
		\sum_{k=0}^{\infty} A^k = (E_n - A)^{-1}. \qedhere
	\end{equation*}
\end{proof}

Als Beispiel betrachten wir
\begin{equation*}
	M =
	\begin{pmatrix}
		1 & a & b \\
		0 & 1 & c \\
		0 & 0 & 1
	\end{pmatrix}, \qquad
	A =
	\begin{pmatrix}
		0 & a & b\\
		0 & 0 & c\\
		0 & 0 & 0
	\end{pmatrix}.
\end{equation*}
$A$ ist nilpotent mit $A^3 = 0$. Also ist $\sum_{k=0}^{\infty} (-A)^k = E_3 - A + A^2$, insbesondere konvergent. Also gilt
nach Theorem \ref{9.4.2} $M^{-1} = E_3 - A + A^2$.

\begin{cor}
	\label{9.4.3}
	Ist $A \in M(n\times n,\bbR)$ invertierbar, $B\in M(n\times n,\bbR)$ und $||B-A|| < ||A^{-1}||^{-1}$, dann
	ist auch $B$ invertierbar und
	\begin{equation*}
		||B^{-1} - A^{-1}|| \le \frac{||A^{-1}||^2||B-A||}{1 - ||A^{-1}||\, ||B-A||}.
	\end{equation*}
\end{cor}

Bemerkung. Nach Korollar~\ref{9.4.3} ist $\GL(n,\bbR)$ eine offene Teilmenge von $M(n\times n, \bbR)$.

\begin{proof}
	Sei $A \in \GL(n,\bbR)$ und $||B-A|| < ||A^{-1}||^{-1}$.
	Dann ist
	\begin{equation}
		\label{9.4.3 eq:Hilf1}
		B = A - (A-B) = A(E_n - A^{-1}(A-B)),
	\end{equation}
	wobei $||A^{-1}(A-B)|| \le ||A^{-1}||\,||A-B||$. Nach Theorem~\ref{9.4.2} ist $(E_n - A^{-1}(A-B))$ invertierbar,
	und da $A$ invertierbar ist, ist nach Gleichung~\eqref{9.4.3 eq:Hilf1} auch $B$ invertierbar. Außerdem gilt
	nach Theorem \ref{9.4.2}
	\begin{equation*}
		B^{-1} = (E_n - A^{-1}(A-B))^{-1} A^{-1} = \sum_{k=0}^{\infty}\left(A^{-1}(A-B)\right)^k A^{-1}.
	\end{equation*}
	
	Daraus folgt
	\begin{equation*}
		B^{-1} - A^{-1} = \sum_{k=1}^{\infty} \left(A^{-1}(A-B)\right)^k A^{-1}
	\end{equation*}
	und damit
	\begin{align*}
		||B^{-1} - A^{-1}|| &\leq \sum_{k=1}^\infty \left\|\left(A^{-1}(A-B)\right)^k A^{-1}\right\| \\
		&\le \sum_{k=1}^{\infty} \left(||A^{-1}||\,||A-B||\right)^{k}||A^{-1}||\\
		&= \frac{||A^{-1}||^2||B-A||}{1 - ||A^{-1}||\, ||B-A||}.
	\end{align*}
	Dabei wurde benutzt, dass $S_n \to S$ in $M(n\times n,\bbR)$ impliziert, dass $||S_n|| \to ||S||$.
\end{proof}








\chapter{Differentialrechnung in $\bbR^n$}
\section{Stetige Abbildungen und Grenzwerte}
In diesem Kapitel darf $\bbR^2$ immer durch $\bbC$ ersetzt werden. Eine Abbildung $f: D\subset \bbR^n \to \bbR^m$
heißt
\begin{itemize}
	\item \emph{Stetig im Punkt $x \in D$}, wenn zu jedem $\varepsilon > 0$ ein $\delta > 0$ existiert, so dass
		\begin{equation*}
			|y-x| < \delta, y\in D \Rightarrow |f(x) - f(y)| < \varepsilon.
		\end{equation*}

	\item \emph{Stetig auf $D$}, wenn $f$ in jedem Punkt $x\in D$ stetig ist.
	
	\item \emph{Gleichmäßig stetig}, wenn zu jedem $\varepsilon > 0$ ein $\delta >0$ existiert, so dass
		\begin{equation*}
			|y-x| < \delta,\quad y,x\in D \Rightarrow |f(y) - f(x)| < \varepsilon.
		\end{equation*}
		
	\item \emph{Lipschitz-stetig},wenn ein $L\in \bbR$ existiert, so dass
		\begin{equation*}
			|f(x)-f(y)| \le L|y-x|
		\end{equation*}
		für alle $y,x \in D$.
\end{itemize}

Beispiele:
\begin{enumerate}
	\item $F:\bbR^n \to \bbR^m$ definiert durch $f(x) = Ax + b$ mit $A \in M(m\times n, \bbR)$ und $b\in \bbR^m$ ist
		Lipschitz-stetig mit $L = ||A||$.
	
	\item $Gl(n,\bbR)\to Gl(n,\bbR), A \mapsto A^{-1}$ ist stetig.
		Beweis: Zu $\varepsilon > 0$, $A \in Gl(n,\bbR)$ wählen wir
		$\delta = \min \left(\frac{1}{2||A^{-1}||, \frac{\varepsilon}{2||A^{-1}||^2}}\right)$. Dann gilt
		\begin{equation*}
			||B - A|| < \delta \Rightarrow ||B^{-1} - A^{-1}|| \le \frac{||A^{-1}||^2||B-A||}{1-||A^{-1}||||B-A||}
				<  \varepsilon.
		\end{equation*}
\end{enumerate}
Ist $f:D\subset \bbR^n \to \bbR^m$ stetig im Punkt $x = (x_1 \dots x_n) \in D$, dann sind auch die
\emph{partiellen Funktionen}
\begin{equation*}
	y_i \mapsto f(x_1 \dots x_{i-1}, y_i, x_{i+1}\dots x_n)
\end{equation*}
stetig in $x_i$. Die Umkehrung gilt nicht. Sei $f: \bbR^2 \to \bbR$ definiert durch
\begin{equation*}
	f(x,y) =
	\begin{cases}
		\frac{2xy}{x^2 + y^2} & (x,y) \ne (0,0)\\
		0 (x,y) = (0,0)
	\end{cases}.
\end{equation*}
Es gilt $f(x,0) = 0 = f(0,y)$, aber $f(x,x) = 1$.

\begin{theorem}
	\label{10.1.1}
	Folgende Aussagen über $f: D\subset \bbR^n \to \bbR^m$, $x \mapsto f(x) = (f_1(x),\dots,f_m(x))$ sind
	äquivalent
	\begin{itemize}
		\item $f$ ist stetig in $x$.
		
		\item Für jede Folge $x_k$ in $D$ mit $\lim_{k\to\infty} x_k = x$ gilt
			\begin{equation*}
				\lim_{k\to \infty} f(x_k) = f(x).
			\end{equation*}
		
		\item $f_1,\dots, f_m$ sind stetig in $x$.
	\end{itemize}
\end{theorem}

\begin{proof}
	Der Beweis, dass \textit{1.} und \textit{2.} äquivalent sind, ist analog zum Fall $n=m=1$. Dass \textit{1.}
	und \textit{3.} äquivalent sind, folgt aus
	\begin{equation*}
		|f_k(y) - f_k(x)|^2 \le |f(y) - f(x)|^2 = \sum_{k=1}^{m} |f_k(y) -f_k(x)|^2. \qedhere
	\end{equation*}
\end{proof}

Das Skalarprodukt $\bbR^n \times \bbR^n = \bbR^{2n} \to \bbR$, $(x,y) \mapsto \langle x,y\rangle$ ist stetig, denn
diese Abbildung ist folgenstetig nach Satz~\ref{9.2.1}.

Frage: Gibt es eine stetige Abbildung $\gamma: [0,1] \to [0,1]\times[0,1]$, die surjektiv ist? Erstaunlicherweise
ist die Antwort ja. Als Beispiel dafür dienen flächenfüllende Kurven (space filling curves). Sei $\Phi:\bbR\to\bbR$
stetig mit $\Phi(t + 2) = \Phi(t)$, $0 \le \Phi(t) \le 1$ und $\Phi(t) = 0$ für $t \in [0,1/3]$ und
$\Phi(t) = 1$ für $t\in[2/3,1]$. Sei $\gamma(t) = (x(t),y(t))$ mit
\begin{align*}
	x(t) &= \sum_{k=1}^{\infty} 2^{-k} \Phi(3^{3k-2}t)\\
	y(t) &= \sum_{k=1}^{\infty} 2^{-k} \Phi(3^{2k-1}t).
\end{align*}
Dann ist $0 < x(t) < 1$, $0 < y(t) <1$. Nach dem Weierstraßschen $M$-Test sind $t \to x(t), y(t)$ stetig. Also ist
$\gamma$ stetig nach Theorem~\ref{10.1.1}. $\gamma$ ist auch surjektiv, denn wenn $(a,b) \in [0,1]\times[0,1]$ mit
\begin{equation*}
	a = \sum_{k=1}^{\infty} 2^{-k}a_k, b = \sum_{k=1}^{\infty} 2^{-k}b_k
\end{equation*}
und
\begin{equation*}
	c = 2\sum_{k=1}^{\infty} 3^{-k}c_k,\qquad c_{2k-1} = a_{k-1}, c_{2k} = b_k.
\end{equation*}
Dann gilt $\gamma(c) = (a,b)$.

\begin{satz}
	\label{10.1.2}
	Sind $f,g:D\subset \bbR^n \to \bbC$ stetig im Punkt $a\in D$, dann auch $f + g$, $\lambda f$, $fg$
	$f/g$ falls $g(a) \ne 0$, $|f|$, $\overline{f}$, $\rePart f$ und $\imPart f$.
\end{satz}

















