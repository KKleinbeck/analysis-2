% !TEX root = Ana2.tex

\begin{satz}
	\label{10.7.2}
	Mittelwertabschätzung: Sei $f: D \subset \bbR^n \to \bbR^m$ differenzierbar und $[a,b] \subset D$. Dann existiert
	ein $x \in [a,b]$, so dass
	\begin{equation*}
		|f(b) - f(a)| \le ||f'(x)||\cdot |b-a|.
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $g(t) \equalDef \langle e, f(a+t(b-a)) \rangle$ für $t \in [0,1]$ mit $e \in \bbR^m$, $|e| = 1$. $g$ ist 
	differenzierbar und somit ist
	\begin{align*}
		\langle e, f(b) - f(a) \rangle &= \langle e, f(b)\rangle - \langle e, f(a) \rangle\\
		&= g(1) - g(0)\\
		&= g'(\tau)\\
		&= \langle e, f'(a + \tau (b-a))\cdot (b-a))\rangle.
	\end{align*}
	Also ist
	\begin{equation*}
		| \langle e, f(b) - f(a)\rangle | = |\langle e, f'(x) (b-a) \rangle| \le
			|f'(x)(b-a)| \le ||f'(x)||\cdot |b-a|.
	\end{equation*}
	Im Fall $f(b) \ne f(a)$ wählen wir nun $e = (f(b) - f(a)) / |f(b) - f(a)|$. Dann
	\begin{equation*}
		\langle e, f(b) - f(a) \rangle = |f(b) - f(a)|.
	\end{equation*}
	Falls $f(a) = f(b)$, ist die Behauptung trivial.
\end{proof}

In Satz~\ref{10.7.2} ist Gleichheit nicht zu erwarten. Beispiel dafür ist $f: \bbR \to \bbR^2$ mit
\begin{equation*}
	f(x) =
	\begin{pmatrix}
		\cos x \\ \sin x
	\end{pmatrix}.
\end{equation*}
Dann ist $f(2\pi) - f(0) = 0$, aber $|f'(x)| = 1$ und somit folgt $f(2\pi) - f(0) \ne 2\pi |f'(x)|$ für alle $x$.

\begin{cor}
	\label{10.7.3}
	Ist $f: D\subset \bbR^n \to \bbR^m$ \emph{stetig differenzierbar}, dann ist $f$ auf jeder konvexen, kompakten
	Teilmenge $K \subset D$ Lipschitzstetig.
\end{cor}

\begin{proof}
	Da $f$ stetig partielle Ableitungen hat ist $x \mapsto f'(x)$ stetig. Also ist auch $x \mapsto ||f'(x)||$ stetig
	und somit
	\begin{equation*}
		L \coloneqq \sup_{x \in K} ||f'(x)|| < \infty
	\end{equation*}
	für jede kompakte Menge $K\subset D$ nach Theorem~\ref{10.1.4}. Für $x,y \in K$ ist $[x,y] \subset K$ da $K$
	konvex ist. Also folgt aus Satz~\ref{10.7.2}, dass $|f(x) - f(y)| \le L |x-y|$.
\end{proof}

Erinnerung: Aus $f$ differenzierbar folgte, dass $f$ stetig ist. Jetzt haben wir gezeigt, dass wenn $f$ stetig
differenzierbar ist, dann ist $f$ lokal Lipschitzstetig. Lokal bedeutet, dass wir eine konvexe, abgeschlossene
Umgebung um einen Punkt finden können, worauf die Funktion Lipschitzstetig ist.

Frage: Wenn $f'(x) = 0$ für alle $x$, folgt $f(x) = \mathrm{const}$? Gegenbeispiel:
Betrachte zwei getrennte, abgeschlossene Gebiete $D_1$, $D_2$ mit $f(x) = 0$ für $x \in D_1$ und $f(x) = 1$ für
$x \in D_2$. Also ist $f'(x) =0$ und $f'(x) \ne \mathrm{const}$. Wir sehen die Rolle der Voraussetzung in
Satz~\ref{10.7.2}.

Eine Menge $D\subset \bbR^n$ heißt \emph{zusammenhängend}, wenn für jede disjunkte, offene Überdeckung
\begin{equation*}
	D \subset U \cup V, \qquad U \cap V = \emptyset
\end{equation*}
gilt
\begin{equation*}
	D \cap U = \emptyset \text{ oder } D \cap V = \emptyset.
\end{equation*}

Daraus folgt:
\begin{satz}
	\label{10.7.4}
	Sei $D\subset \bbR^n$ offen und zusammenhängend. Ist $f: D \to \bbR^m$ differenzierbar und $f' = 0$ in $D$, dann
	ist $f$ konstant.
\end{satz}

\begin{proof}
	Sei $ a \in D$ ein beliebiger Punkt, aber fest und $U = \{x\in D \mid f(x) = f(a)\}$ und
	$V = \{x \in D\mid f(x) \ne f(a)\}$. Dann gilt $D \subset U\cup V$ und $U\cap V = \emptyset$.
	Behauptung $U$ ist offen. Sei $x \in U$, dann existiert $\varepsilon > 0$ mit
	$B_\varepsilon(x) \subset D$. Für $y \in B_\varepsilon(x)$ gilt nach Satz~\ref{10.7.2}
	\begin{equation*}
		|f(y) - f(x) | \le ||f'(y^*)||\cdot |y-x| = 0.
	\end{equation*}
	Also $f(y) = f(x) = f(a)$. Das heißt $B_\varepsilon(x) \subset U$, also ist $U$ offen.
	
	Behauptung: $V$ ist offen. Sei $x \in V$ und $\varepsilon \coloneqq |f(X) -f(a)| > 0$. Da $f$ stetig und $D$
	offen ist, existiert $\delta > 0$ mit $B_\delta(x) \subset D$ und
	$y \in B_\delta(x) \Rightarrow |f(y) - f(x)| < \varepsilon$. Also
	\begin{align*}
		y \in B_\delta(x) \Rightarrow |f(y) - f(a)| &= |f(y) - f(x) + f(x) - f(a)| \\
		&\ge \usb{= \varepsilon}{|f(x) - f(a)|} - \usb{< \varepsilon}{|f(y) - f(x)|}\\
		&>0.
	\end{align*}
	Folglich ist $B_\delta(x) \subset V$ und $V$ ist offen.
	
	Da $D$ zusammenhängend ist, folgt dass $D\cap U = \emptyset$ oder $D\cap V = \emptyset$. Da $a \in D \cap U$ ist
	$D \cap U \ne \emptyset$ und somit $V = \emptyset$ und $D = U$.
\end{proof}

\section{Hessesche Matrix und lokale Extrema}
Sei $f: D\subset \bbR^n \to \bbR$ zwei Mal stetig differenzierbar, $D$ offen und $a \in D$. Dann ist
die Strecke $[a,a+x]\subset D$ für $|x|$ klein genug. Sei $g(t) = f(a + tx)$ für $t \in [0,1]$. Mit der Kettenregel
folgt
\begin{equation*}
	g'(t) = \sum_{k=1}^{n} \partial_k f(a + tx)\cdot x_k =\langle \nabla f(a + tx), x\rangle.
\end{equation*}
Analog ergibt sich
\begin{equation*}
	g''(t) = \sum_{k=1}^{n} \frac{\dd}{\dd t} \partial_k f(a+tx) x_k
		= \sum_{k=1}^{n} \sum_{i=1}^{n} \partial_i \partial_k f(a + tx) x_k x_i
		= \langle x, H_f(a + tx) x\rangle.
\end{equation*}
wobei die Matrix $H_f(x)_{ik} = \partial_i \partial_k f(x)$ die Hessesche Matrix von $f$ an der Stelle $x$ ist.
Für stetige Ableitungen $\partial_i \partial_k f$ ist $\partial_{ik} f = \partial_{ki} f$ und $H_f$
ist eine symmetrische Matrix (Satz von Schwarz).

\begin{theorem}
	\label{10.8.1}
	Sei $f \in C^2(D)$, $D\subset \bbR^n$ offen und $a \in D$. Dann gilt
	\begin{equation*}
		f(a+x) = f(a) + \langle \nabla f(a),x\rangle  + \frac{1}{2} \langle x, H_f(a)x\rangle + o(|x^2|),
			\qquad (x\to 0).
	\end{equation*}
\end{theorem}


Bemerkung: Theorem~\ref{10.8.1} verallgemeinert die Taylorentwicklung
\begin{equation*}
	f(a+x) = f(a) + f'(a) x + \frac{1}{2} f''(a)x^2 + o(|x^2|)
\end{equation*}
von $n = 1$ auf $n \ge 1$.

\begin{proof}
	Aus den Identitäten für $g'$ und $g''$ sehen wir, dass $g \in C^2([0,1])$. Also, nach
	Taylor (Theorem 5.7.1) ist $g(1) = g(0) + g'(0)\cdot 1 + \frac{1}{2} g''(\tau)\cdot 1$ mit $\tau \in (0,1)$.
	Also
	\begin{equation*}
		f(a + x) = f(a) + \langle \nabla f(a), x\rangle + \frac{1}{2} \langle x, H_f(a + \tau x)x\rangle
	\end{equation*}
	wobei $\langle x, H_f(a + \tau x) x\rangle = \langle x,H_f(a) x\rangle + o(|x^2|)$ für $x \to 0$, denn
	\begin{align*}
		\frac{1}{|x^2|} \left| \langle x,H_f(a + \tau x)x \rangle - \langle x,H_f(a) x\rangle\right|
			&= \left| \sum_{i,k = 1}^{n} \frac{x_ix_k}{|x^2|}
				\left(\partial_i\partial_k f(a+\tau x) - \partial_i\partial_k f(a) \right) \right|\\
		&\le \sum_{i,k=1}^{n} \left|\partial_i\partial_k f(a + \tau x) - \partial_i \partial_k f(a)\right| \to 0
	\end{align*}
	für $x \to 0$, denn $\partial_i\partial_k f$ ist stetig.
\end{proof}

Die Funktion $f:D\subset \bbR^n \to \bbR$ hat in $a \in D$ ein \emph{lokales Maximum}, falls
$B_\varepsilon(a) \subset D$ existiert, so dass
\begin{equation*}
	f(x) \le f(a)
\end{equation*}
für alle $x \in B_\varepsilon(a)$. Das lokale Maximum heißt \emph{strikt}, falls $f(x) < f(a)$ für
$x \in \dot{B}_\varepsilon(a)$. (Strikte) lokale Minima sind analog definiert und \emph{lokales Extremum}
ist der gemeinsame Oberbegriff für lokale Maxima und Minima. Ist $f$ differenzierbar und $\nabla f (a) = 0$, dann
heißt $a$ ein \emph{kritischer} oder \emph{stationärer} Punkt von $f$.

\begin{satz}
	\label{10.8.2}
	Ist die differenzierbare Funktion $f:D\to \bbR$ im Punkt $a$ lokal extremal, dann ist $a$ ein kritischer Punkt
	von $f$, d.h.\ $\nabla f(a) = 0$.
\end{satz}

\begin{proof}
	Ist $f$ in $a\in D$ lokal extremal, so ist $t \to f(a + th)$ für $h \in \bbR^n$ in $t = 0$ lokal extremal. Also
	ist nach Analysis I die Ableitung
	\begin{equation*}
		0 = \frac{\dd}{\dd t} f(a +th)\big|_{t = 0} = \partial_h f(a).
	\end{equation*}
	Insbesondere ist $\partial_k f(a) = 0$ für $k= 1,\dots,n$ und somit $\nabla f(a) = 0$.
\end{proof}

Wenn $a \in D$ ein kritischer Punkt einer $C^2$-Funktion $f:D\to \bbR$ ist, dann gilt nach Theorem~\ref{10.8.1},
dass $f(a+x) = f(a) + \frac{1}{2} \langle x,H_f(a) x\rangle + o(|x^2|)$ für $x \to 0$. Also entscheidet
$H_f$ darüber, ob ein lokales Minimum oder Maximum vorliegt. Zur Erinnerung: Eine symmetrische $n\times n$
Matrix $A$ heißt
\begin{enumerate}
	\item \emph{positiv definit}, falls $\langle x,Ax\rangle > 0$ für alle $x \ne 0$.
	\item \emph{positiv semi-definit}, falls $\langle x,Ax\rangle \ge 0$ für alle $x\in \bbR^n$.
	\item \emph{negativ (semi-)definit}, falls $-A$ positiv (semi-)definit ist.
	\item \emph{indefinit}, falls $x,y \in \bbR^n$ existieren mit $\langle x,Ax\rangle > 0$ und
		$\langle y,Ay\rangle < 0$. 
\end{enumerate}
Zu jeder symmetrischen $n\times n$ Matrix $A$ gibt es eine Orthonormalbasis $\eta_1,\dots, \eta_n$ von $\bbR^n$
bestimmt aus Eigenvektoren von $A$,
\begin{equation*}
	A \eta_k = \lambda_k \eta_k,\qquad \langle \eta_i,\eta_j\rangle \delta_{ik}.
\end{equation*}
Für $x = \sum a_k \eta_k$ folgt
\begin{equation*}
	\langle x,Ax\rangle = \sum_{k=1}^{n} \lambda_k x^2_k
\end{equation*}
und somit gilt für die Matrix $A$:
\begin{enumerate}
	\item $A$ ist positiv definit genau dann, wenn $\forall k: \lambda_k > 0$.
	\item $A$ ist positiv semi-definit genau dann, wenn $\forall k: \lambda_k \ge 0$.
	\item $A$ ist indefinit genau dann, wenn $\exists i,k: \lambda_i < 0 < \lambda_k$.
\end{enumerate}
Aus der linearen Algebra ist ausserdem bekannt, dass eine reelle symmetrische $n\times n$ Matrix $(A_{ik})$ genau
dann positiv definit ist, wenn für $k = 1,\dots, n$
\begin{equation*}
	\det
	\begin{pmatrix}
		A_{11} & \dots & A_{1k}\\
		\vdots & & \vdots \\
		A_{k1} & \dots & A_{kk}
	\end{pmatrix}
	> 0.
\end{equation*}

Beispielsweise ist
\begin{equation*}
	A =
	\begin{pmatrix}
		a & b \\ b & c
	\end{pmatrix}
\end{equation*}
genau dann positiv definit, wenn $a > 0$ und $\det A = ac - b^2 > 0$.































