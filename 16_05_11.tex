% !TEX root = Ana2.tex

Bemerkung: Die Aussage folgt aus dem $M$-Test, falls $\sum |a_k| <\infty$.

\begin{proof}
	Sei $A_n = \sum_{k=0}^{n} a_k - A$, wobei $A=\sum_{k=0}^{\infty} a_k$. Dann ist $A_n \to 0$.
	Sei $\varepsilon > 0$ und sei $N\in \bbN$ so gewählt, dass $|A_n| < \frac{\varepsilon}{4}$ für $n \ge N$.	
	Wir zeigen, dass $\left(\sum_{k=0}^{n} a_k x^k\right)_{n\in \bbN}$ eine Cauchyfolge bezüglich der
	Supremumsnorm ist. Für $n\ge m\ge N$ gilt
	\begin{align*}
		\left|\sum_{k=0}^{n} a_k x^k - \sum_{k=0}^{m} a_kx_k \right| &= \left| \sum_{k=m+1}^{n}a_kx^k\right| \\
		&= \left|\sum_{k=m+1}^{n} (A_k - A_{n-1})x^k\right| \\
		&= \left| \sum_{k=m+1}^{n}A_kx^k - \sum_{k=m}^{n-1} A_k x^{k+1}\right|\\
		&= \left| \sum_{k=m+1}^{n-1}A_k(x^k - x^{k+1})  + A_n x^n - A_m x^m\right|\\
		&\le \sum_{k=m}^{n-1} |A_k| (x^k - x^{k+1}) + \frac{\varepsilon}{2} \\
		&\le \frac{\varepsilon}{4}\sum_{k=m}^{n-1} (x^k - x^{k+1}) + \frac{\varepsilon}{2} \\
		&= \frac{\varepsilon}{4} (x^n - x^m) + \frac{\varepsilon}{2} \\
		&\le \frac{3}{4}\varepsilon < \varepsilon,
	\end{align*}
	für alle $x \in [0,1]$.
\end{proof}

Da $1 - \frac{1}{2} + \frac{1}{3} - \dots$ und $1 - \frac{1}{3} + \frac{1}{5} - \dots$ nach Leibniz konvergent sind,
folgt aus Satz~\ref{8.3.5}, dass
\begin{align*}
	1 - \frac{1}{2} + \frac{1}{3} - \dots &= \lim_{x\to 1-} \left(x - \frac{x^2}{2} + \frac{x^3}{3} - \hdots \right)\\
	&= \lim_{x\to 1-}\log(1+x) = \log(2)
\end{align*}
und ebenso
\begin{equation*}
	1 - \frac{1}{3} + \frac{1}{5} - \dots = \arctan(2) = \frac{\pi}{4}.
\end{equation*}

\subsection{Die Binomialreihe}
Für $\alpha \in \bbR$, $k\in \bbN$ definert man Binomialkoeffizienten
$\begin{pmatrix}
	\alpha \\ k
\end{pmatrix}$ durch
\begin{equation*}
	\begin{pmatrix}
		\alpha \\ 0
	\end{pmatrix}
	= 1,\quad
	\begin{pmatrix}
		\alpha \\ 1
	\end{pmatrix}
	= \alpha,\quad
	\begin{pmatrix}
		\alpha \\ k
	\end{pmatrix}
	= \frac{\alpha(\alpha-1)\dots(\alpha-k+1)}{k!}.
\end{equation*}
Die Reihe
\begin{equation*}
	\sum_{k=0}^{\infty}
	\begin{pmatrix}
		\alpha \\ k
	\end{pmatrix}
	x^k
\end{equation*}
heißt \emph{Binomialreihe}. Ist $\alpha \in \bbN$ und $k>\alpha$ (d.h. $k \ge \alpha + 1$) dann ist
$\begin{pmatrix}
	\alpha \\ k
\end{pmatrix} = 0$ und somit gilt
\begin{equation*}
	\sum_{k=0}^{\infty}
	\begin{pmatrix}
		\alpha \\ k
	\end{pmatrix}
	x^k
	=
	\sum_{k=0}^{\alpha}
	\begin{pmatrix}
		\alpha \\ k
	\end{pmatrix}
	x^k
	=
	(1+x)^\alpha.
\end{equation*}

\begin{theorem}
	\label{8.3.6}
	Sei $\alpha \in \bbR$. Dann gilt
	\begin{equation*}
		(1+x)^\alpha = \sum_{k=0}^{\infty}
		\begin{pmatrix}
			\alpha \\ k
		\end{pmatrix}
		x^k,\quad \text{für $|x| < 1$}.
	\end{equation*}
\end{theorem}

\begin{proof}
	Zur Vorbereitung berechnen wir
	\begin{equation*}
		\begin{pmatrix}
			\alpha \\ k+1
		\end{pmatrix}
		(k+1)
		=
		\frac{\alpha(\alpha-1) \dots (\alpha - k)}{(k+1)!}(k+1)
		=
		\begin{pmatrix}
			\alpha \\ k
		\end{pmatrix}
		(\alpha-k).
	\end{equation*}
	Mit dem Quotientenkriterium folgt
	\begin{equation*}
		\left|
		\frac%
		{
			\begin{pmatrix}
				\alpha \\ k+1
			\end{pmatrix}
			x^{k+1}
		} %
		{
			\begin{pmatrix}
				\alpha \\ k
			\end{pmatrix}
			x^k
		}
		\right|
		= |x| \left|\frac{\alpha-k}{k+1}\right|
		\to |x|\quad (k\to\infty).
	\end{equation*}
	Also ist die Binomialreihie für $|x|<1$ absolut konvergent und für $|x| >1$ ist die Reihe divergent. Nach
	Theorem~\ref{8.3.5} ist also $b_\alpha(x) =\sum_{k=0}^{\infty}
	\begin{pmatrix}
		\alpha \\ k
	\end{pmatrix}
	x^k$ auf $(-1,1)$ differenzierbar und
	\begin{align*}
		b_\alpha'(x) &=	\sum_{k=0}^{\infty}
			\begin{pmatrix}
				\alpha \\ k
			\end{pmatrix}
			kx^{k-1}\\
		&= \sum_{k=0}^{\infty}
			\begin{pmatrix}
				\alpha \\ k+1
			\end{pmatrix}
			(k+1)x^k\\
		&= \sum_{k=0}^{\infty}
			\begin{pmatrix}
				\alpha \\ k
			\end{pmatrix}
			(\alpha -k)x^k\\
		&= \alpha b_\alpha(x) - xb_\alpha'(x),
	\end{align*}
	womit die Differentialgleichung
	\begin{equation*}
		b_\alpha'(x) = \frac{\alpha}{1+x}b_\alpha(x)\qquad b_\alpha(0) = 1
	\end{equation*}
	folgt. Wir wissen bereits, wie wir die eindeutige Lösungen solcher homogen linearen Differentialgleichungen
	finden können. Sie lautet
	\begin{equation*}
		b_\alpha(x) = \frac{b_\alpha(0)}{1} \exp\left(\int_{0}^{x}\frac{\alpha}{1+t}\dd t\right)
		= \exp\left(\alpha\log(1+x)\right)
		= (1+x)^\alpha. \qedhere
	\end{equation*}
\end{proof}

Beispielsweise können wir damit diese wichtige Reihendarstellung für Wurzelausdrücke herleiten:
\begin{equation*}
	(1+x)^{-1/2}
	= \sum_{k=0}^{\infty}
	\begin{pmatrix}
		-\frac{1}{2} \\ k
	\end{pmatrix}
	x^k.
\end{equation*}
Damit folgt
\begin{align*}
	\arcsin(x) &= \int_{0}^{x} \frac{1}{\sqrt{1 - t^2}}\dd t\\
	&= \int_{0}^{x} \sum_{k=0}^{\infty}
		\begin{pmatrix}
			-\frac{1}{2} \\ k
		\end{pmatrix}
		(-t^2)^k \dd t\\
	&= \sum_{k=0}^{\infty}
		\begin{pmatrix}
			-\frac{1}{2} \\ k
		\end{pmatrix}
		(-1)^k
		\frac{t^{2k+1}}{2k+1}, \quad |x| <1\\
	&= x + \frac{1}{6} x^3 + \frac{3}{40} x^5 + \dots
\end{align*}


\section{Fourierreihen}
Eine Funktion $f:\bbR\to \bbC$ heißt \emph{$2\pi$-periodisch} wenn $f(x+2\pi) = f(x)$ für alle $x\in\bbR$.
Für $n\in \bbN$ sind $\cos(nx)$, $\sin(nx)$ und $e^{inx}$ $2\pi$-periodisch. Wir untersuchen die Frage, unter
welchen Umständen sich eine gegebene $2\pi$-periodische Funktion darstellen lässt als
\begin{equation}
	\label{8.4 eq:Fourier}
	f(x) = \sum_{n=-\infty}^{\infty} c_n e^{inx}, \quad c_n\in\bbC.
\end{equation}
Das ist äquivalent zu
\begin{equation*}
	f(x) = \frac{a_0}{2} + \sum_{n=1}^{\infty} \left(a_n\cos(nx) + b_n\sin(nx)\right),
\end{equation*}
wobei
\begin{align*}
	a_n &= c_n + c_{-n}\\
	b_n &= i(c_n - c_{-n}).
\end{align*}
Falls~\eqref{8.4 eq:Fourier} gilt und die Konvergenz gleichmäßig auf $\bbR$ ist, dann gilt
\begin{equation}
	\label{8.4 eq:Fouriercoeff}
	c_n = \frac{1}{2\pi} \int_{0}^{2\pi} e^{-inx}f(x)\dd x, \quad n \in \bbZ,
\end{equation}
denn
\begin{align*}
	\int_{0}^{2\pi}e^{imx}f(x) \dd x = \sum_{n=-\infty}^{\infty} c_n \int_{0}^{2\pi} e^{(n-m)x} \dd x
	= 2\pi c_m \delta_{mn},
\end{align*}
wobei $\delta_{mn} = 1$ für $m=n$ und $\delta_{mn} = 0$ sonst. Die Reihe~\eqref{8.4 eq:Fourier}, mit $c_n$
definiert durch~\eqref{8.4 eq:Fouriercoeff}, heißt \emph{Fourierreihe} von $f$. Die Zahlen $c_n$ heißen
\emph{Fourierkoeffizienten} von $f$.

\subsection{Berechnung der Partialsumme}
\begin{align*}
	S_N &= \sum_{n=-N}^{N} c_n e^{inx} \\
	&= \sum_{n=-N}^{N} \left(\frac{1}{2\pi}\int_{0}^{2\pi} e^{-int} f(t)\dd t \right) e^{inx}\\
	&= \sum_{n=-N}^{N} \left(\frac{1}{2\pi}\int_{0}^{2\pi} e^{-in(x-t)} f(t)\dd t \right)\\
	&\equiv \frac{1}{2\pi} \int_{0}^{2\pi} D_N(x-t) f(t) \dd t,
\end{align*}
wobei die Funktion
\begin{equation*}
	D_N(t) = \sum_{n=-N}^{N}e^{int} =
	\begin{cases}
		\frac{\sin\left(\left(N+\frac{1}{2}\right)t\right)}{\sin\frac{t}{2}} & t \not\in 2\pi\bbZ \\
		2N + 1 & t\in 2\pi\bbZ
	\end{cases}
\end{equation*}
eingeführt wurde. Sie wird als \emph{Dirichlet-Kern} bezeichnet. Sie hat folgende Eigenschaften
\begin{enumerate}
	\item $D_N$ ist $2\pi$-periodisch.
	\item $D_N(-t) = D_N(t)$.
	\item
	\begin{equation*}
		\frac{1}{2\pi} \int_{0}^{2\pi} D_N(t) \dd t = 1,
	\end{equation*}
	für alle $N$.
\end{enumerate}

\begin{satz}
	\label{8.4.1}
	Riemannsches Lemma: Für jede Regelfunktion $g:[a,b]\to \bbC$ gilt
	\begin{equation*}
		\lim_{\lambda\to\infty} \int_{a}^{b} g(t) \sin(\lambda t) \dd t = 0.
	\end{equation*}
\end{satz}

Für eine Regelfunktion $f:\bbR\to\bbC$ definieren wir einseitige Ableitungen $f'(x\pm)$ im Punkt $x\in\bbR$
durch
\begin{align*}
	f'(x+) = \lim_{h \to 0+}\frac{f(x+h) - f(x+)}{h},
	f'(x+) = \lim_{h \to 0+}\frac{f(x-h) - f(x-)}{-h}.
\end{align*}

\begin{theorem}
	\label{8.4.2}
	Ist $F:\bbR \to \bbC$ eine $2\pi$-periodische Regelfunktion mit Fourierkoeffizienten $(c_n)_{n\in\bbZ}$
	und sei $x\in \bbR$ ein Punkt, wo die einseitige Ableitung $f'(x\pm)$ existiert. Dann gilt
	\begin{equation*}
		\sum_{n=-\infty}^{\infty} c_n e^{inx} = \frac{1}{2}\left(f(x+) + f(x-) \right).
	\end{equation*}
\end{theorem}

\begin{proof}
	Wir berechnen
	\begin{align*}
		S_N &= \frac{1}{2\pi} \int_{0}^{2\pi} D_N(x-t) f(t) \dd t\\
		&= \frac{-1}{2\pi} \int_{x}^{x-2\pi} D_N(u) f(x-u) \dd u\\
		&= \frac{1}{2\pi} \int_{x - 2\pi}^{x} f(x-t) D_N(t) \dd t\\
		&= \frac{1}{2\pi} \int_{-\pi}^{\pi} f(x-t) D_N(t) \dd t\\
		&= \frac{1}{2\pi} \int_{0}^{\pi} f(x-t) D_N(t)\dd t + \frac{1}{2\pi} \int_{0}^{\pi} f(x+t) D_N(t)\dd t\\
		&\to \frac{1}{2} f(x-) + \frac{1}{2} f(x+) \qquad(N\to \infty).
	\end{align*}
	Denn
	\begin{align*}
		\frac{1}{2\pi} \int_{0}^{\pi} f(x+t)D_N(t)\dd t - \frac{1}{2} f(x+) &=
			\frac{1}{2\pi}\int_{0}^{\pi} (f(x+t) - f(x+)) D_N(t) \dd t\\
		&= \frac{1}{\pi} \int_{0}^{\pi} g(t)\sin\left(\left(N+\frac{1}{2}\right)t\right) \dd t,
	\end{align*}
	mit
	\begin{equation*}
		g(t) = \frac{f(x+t) - f(x+)}{2\sin(t/2)} = \frac{f(x+t) - f(x+)}{t}\frac{t/2}{\sin t/2}.
	\end{equation*}
	$g$ ist auf $(0,\pi]$ eine Regelfunktion, wobei $\lim_{t\to 0}g(t)$ existiert. Also lässt sich $g$ zu einer
	Regelfunktion auf $[0,\pi]$ fortsetzen, z.B.\ $g(0) = f'(x+)$. Also folgt mit dem Riemannschen
	Lemma~\ref{8.4.1} die Behauptung
	\begin{equation*}
		\frac{1}{2\pi} \int_{0}^{\pi} f(x-t) D_N(t)\dd t \to \frac{1}{2} f(x+) \quad (N \to \infty).
	\end{equation*}
	Analog lässt sich die Behauptung für $f(x-)$ beweisen.
\end{proof}

Als Folgerung ergibt sich: Ist $f:\bbR\to\bbC$ $2\pi$-periodisch und stückweise $C^1$, d.h.\ $f$ ist stetig und
es existieren Partitionen $0 = x_0 < \dots <x_n =2 \pi$, so dass $f$ von der Klasse $C^1$ ist auf jedem
Teilintervall$[x_k,x_{k+1}]$, dann ist
\begin{equation*}
	f(x) = \sum_{n=-\infty}^{\infty} c_n e^{inx}, \qquad c_n = \frac{1}{2\pi} \int_{0}^{2\pi} e^{-inx}f(x)\dd x.
\end{equation*}





