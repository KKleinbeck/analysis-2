% !TEX root = Ana2.tex

\section{Differenzierbare Abbildungen}
\subsubsection{Lineare Abbildungen}
Jede lineare Abbildung $A:\bbR^n \to \bbR^m$ entspricht einer $m\times n$ Matrix $(A_{ik})$, und
umgekehrt, wobei
\begin{equation*}
	(Ax)_i = \sum_{k=1}^{n} A_{ik}x_k.
\end{equation*}
Schreiben wir Vektoren in $\bbR^d$ als Spaltenvektoren ($d\times 1$ Matrizen), so können wir $Ax$ auffassen als
Matrixprodukt. Auch das Skalarprodukt wird nun zum Matrixprodukt
\begin{equation*}
	\langle x,y\rangle = x^T y, \qquad x,y\in \bbR^d.
\end{equation*}

Die Verknüpfung von linearen Abbildungen $A:\bbR^n \to \bbR^m$ und $B:\bbR^m \to \bbR^l$ ist wieder eine lineare
Abbildung, $B\circ A:\bbR^m \to \bbR^l$ mit Matrix
\begin{equation*}
	(BA)_{ik} = \sum_{j=1}^{m} B_{ij}A_{jk}.
\end{equation*}

Eine lineare Abbildung $f:\bbR^n \to \bbR$ heißt \emph{Linearform} und wird durch eine $(1\times n)$ Matrix $A$
beschrieben, bzw.\ durch einen Vektor $v = A^T \in \bbR^n$ dargestellt
\begin{equation*}
	f(k) = Ak = v^T k = \langle v,k\rangle.
\end{equation*}

Die \emph{Matrixnorm}
\begin{equation*}
	||A|| = \left(\sum_{i,j}^{} A^2_{jk} \right)^{1/2}
\end{equation*}
stimmt für $(n\times 1)$- und $(1\times n)$-Matrizen überein mit der Euklidischen Norm der entsprechenden Vektoren.
Aus $||BA|| \le ||B||\cdot ||A||$ folgt also
\begin{equation*}
	|\langle v,k\rangle| = |v^T k| \le |v| |k|
\end{equation*}
und
\begin{equation*}
	||Ak|| \le ||A||\cdot |k|
\end{equation*}
für $v,k \in \bbR^n$ und $A\in M(m\times n)$.

Notation: $L(\bbR^n,\bbR^m)$ ist die Menge der linearen Abbildungen von $\bbR^n$ nach $\bbR^m$ und
$L(\bbR^n) = (\bbR^n)^*$ der \emph{Dualraum} von $\bbR^n$.

Erinnerung: Eine \emph{Funktion} $F:D\subset \bbR\to \bbR$ ist differenzierbar in $x\in D$, wenn
\begin{equation*}
	\lambda \coloneqq \lim\limits_{h\to 0} \frac{f(x+h) - f(x)}{h}
\end{equation*}
existiert. Wir können dies nicht einfach auf höhere Dimensionen verallgemeinern, denn wir können nicht einfach
durch einen Vektor dividieren. Äquivalent dazu ist, dass $\lambda \in \bbR$ existiert, mit
\begin{equation*}
	\frac{f(x+h) - f(x) -\lambda h}{|h|} \to 0\qquad (h \to 0).
\end{equation*}

\begin{definition}
	Sei $D\subset \bbR^n$ offen. Eine Abbildung $f:D\subset\bbR^n \to \bbR^m$ heißt \emph{differenzierbar} im
	Punkt $x\in D$, wenn eine lineare Abbildung $A:\bbR^n \to \bbR^m$ existiert, so dass
	\begin{equation*}
		\frac{f(x+h) - f(x) - Ah}{|h|} \to 0 \qquad (h\to 0)
	\end{equation*}
	bzw.\ in Landauscher $\mathcal{O}$-Notation,
	\begin{equation*}
		f(x+h) - f(x) = Ah + o(|h|)\qquad (h\to 0).
	\end{equation*}
	Der Zuwachs $f(x+h) - f(x)$ von $f$ wird gut approximiert durch die lineare Abbildung $Ah$ von $h$.
\end{definition}

Die lineare Abbildung $A$ ist dann eindeutig bestimmt (s.u.) und heißt \emph{Ableitung} oder \emph{Differential}
von $f$ an der Stelle $x\in D$. Sie wird mit $\dd f(x)$ oder $\mathrm{D}f(x)$ bezeichnet. Die Matrix von $\dd f(x)$
(bezüglich der Standardbasis von $\bbR^n,\bbR^m$) heißt \emph{Jacobi-Matrix} von $f$ an der Stelle $x \in D$. Wir
bezeichnen sie mit $f'(x) \in M(m\times n)$.

Die Abbildung $f = (f_1,\dots,f_m):D\subset\bbR^n \to \bbR^m$ ist genau dann differenzierbar im Punkt $x$, wenn
die Funktionen $f_1,\dots,f_m:D\to \bbR$ in $x\in D$ differenzierbar sind und es gilt
\begin{equation*}
	\dd f_i(x)h = f_i'(x)h = (\dd f(x)h)_i.
\end{equation*}

\begin{satz}
	\label{10.4.1}
	Ist $f:D\subset \bbR^n \to \bbR^m$ differenzierbar in $x\in D$, dann gilt
	\begin{enumerate}
		\item $f$ ist stetig in $x$.
		\item $f$ ist partiell differenzierbar in $x$ und $f'(x)_{ik} = \partial_kf_i(x)$.
		\item Es existieren alle Richtungsableitungen in $x$ und
			\begin{equation*}
				\partial_k f(x) = \dd f(x)k = f'(x)k.
			\end{equation*}
	\end{enumerate}
\end{satz}

\begin{proof}
	\begin{enumerate}
		\item Nach Voraussetzung gilt
		\begin{equation*}
			R(x,h) = \frac{f(x+h) - f(x) - f'(x)h}{|h|} \to 0 \qquad (h\to0).
		\end{equation*}
		Also
		\begin{equation*}
			f(x+h ) - f(x) = f'(x)h + R(x,h)|h| \to 0\qquad (h \to 0),
		\end{equation*}
		denn $|f'(x)h| \le ||f'(x)||\cdot |h| \to 0$.
		
		\item Aus \textit{3.}\ folgt insbesondere die Existenz der partiellen Ableitungen
			\begin{equation*}
				\partial_i f(x) = \partial_{e_i}(x) = f'(x) e_i.
			\end{equation*}
			Das heißt
			\begin{equation*}
				\langle e_k, \partial_i f(x)\rangle = \langle e_k, f'(x)e_i\rangle = f'(x)_{ki}.
			\end{equation*}
		
		\item
			\begin{align*}
				\partial_h f(x) &\coloneqq \frac{\dd}{\dd t} f(x +th)\big|_{t=0}\\
					&= \lim_{t\to 0}\frac{f(x+th) - f(x)}{t} \\
					&= \lim_{t\to 0}\frac{f(x+th) - f(x) - f'(x)th}{t|h|}|h| + f'(x)h\\
					&= f'(x)h. \qedhere
			\end{align*}
	\end{enumerate}
\end{proof}

Beispiele:
\begin{enumerate}
	\item Eine Abbildung $f:(a,b) \to \bbR^m$ ist genau dann differenzierbar in $x\in (a,b)$, wenn der
		Geschwindigkeitsvektor
		\begin{equation*}
			f'(x) = \lim_{h\to 0} \frac{f(x+h) - f(x)}{h}
		\end{equation*}
		existiert. $f'(x)$ ist die Jacobi-Matrix und
		\begin{equation*}
			\dd f(x): h \mapsto f'(x) h.
		\end{equation*}
	
	\item Jede affine Abbildung $f:\bbR^n \to \bbR^m$, $x\mapsto Ax+ b$, $A\in M(m\times n)$, $b\in \bbR^m$ ist
		differenzierbar und $f'(x) = A$. Falls $b = 0$ dann ist $\dd f(x) h = f(h)$.
	
	\item Eine quadratische Form $f(x) = \langle x,Ax\rangle \in \bbR$ mit $A\in M(n\times n)$ symmetrisch ist differenzierbar
		und
		\begin{align*}
			\dd f(x) h &= 2 \langle h,Ax\rangle = 2\langle Ax,h\rangle = 2(Ax)^T h,\\
			f'(x) &= 2(Ax)^T.
		\end{align*}
		
		\begin{proof}
			\begin{align*}
				f(x+h) - f(x) &= \langle x+h,A(x+h)\rangle - \langle x,Ax\rangle\\
				&= \langle x,Ah\rangle + \langle h,Ax\rangle + \langle h,Ah\rangle\\
				&= \langle h,Ax\rangle + \langle A^T x,h\rangle + \langle h,Ah\rangle\\
				&= \langle (A+A^T)x,h\rangle + \langle h,Ah\rangle.
			\end{align*}
			Also
			\begin{align*}
				\frac{|f(x+h) - f(x) - \langle (A+A^T)x,h\rangle |}{|h|}
					&= \frac{|\langle h,Ah\rangle|}{|h|}\\
				&\le \frac{|h|^2||A||}{|h|}\\
				&= ||A||\cdot |h| \to 0.
			\end{align*}
			Damit folgt
			\begin{equation*}
				\dd f(x) h = \langle (A+A^T)x,h\rangle = 2\langle Ax,h\rangle,
			\end{equation*}
			da wir $A$ in einen symmetrischen und einen antisymmetrischen Teil zerlegen können.
		\end{proof}
\end{enumerate}

\begin{theorem}
	\label{10.4.2}
	Sei $D\subset \bbR^n$ offen. Sind $f_1,\dots,f_m :D\to \bbR$ partiell differenzierbar und sind die partiellen
	Ableitungen $\partial_i f_k$ stetig im Punkt $x\in D$, dann ist $f = (f_1,\dots,f_n) : D\to \bbR^m$
	differenzierbar im Punkt $x$.
\end{theorem}

\begin{proof}
	Es genügt den Fall $m = 1$ zu beweisen. Sei $g(h) = f(x+h)$. Wir zeigen, dass $g$ in $h=0$ differenzierbar ist
	mit $g'(0)_{ik} = \partial_kg_i(0)$.
	\begin{align*}
		g(h) - g(0) - \sum_{k=1}^{m} \partial_k g(0) h_k &=
			\sum_{k=1}^{m} \left[g(h_1,\dots,h_k,0\dots)
				- g(h_1,\dots,h_{k+1},0\dots) - \partial_k g(0) h_k  \right]\\
		&= \sum_{k=1}^{m} \partial_kg(h_1,\dots,h_{k-1},\theta_k h_k,0,\dots)h_k - \partial k g(0,0) h_k,
	\end{align*}
	nach dem Mittelwertsatz. Also
	\begin{align*}
		\frac{|g(h) - g(0) - \sum\partial_kg(0)h_k |}{|h|} &\le
			\frac{\left(\sum_{k=1}^{m} |\partial_k g(h_1,\dots,h_{k-1}, \theta_k h_k,0,\dots)
				- \partial_k g(0)|^2 \right)^{1/2} |h|}{|h|}\\
		&\to 0
	\end{align*}
	da $\partial_k g$ stetig ist im Punkt $h=0$.
\end{proof}



































