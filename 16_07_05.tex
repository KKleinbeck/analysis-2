% !TEX root = Ana2.tex

\begin{proof}
	Wir schreiben $F(x) = \Phi(b(x),a(x),x)$ mit $\Phi:J\times J \times I$ definiert durch
	\begin{equation*}
		\Phi(b,a,x) = \int_{a}^{b}f(x,t)\dd t.
	\end{equation*}
	$\Phi$ hat stetige partielle Ableitungen
	\begin{align*}
		\frac{\partial \Phi}{\partial b} &= f(x,b)\\
		\frac{\partial \Phi}{\partial a} &= f(x,a)\\
		\frac{\partial \Phi}{\partial x} &= \int_{b}^{a} \partial_x f(x,t) \dd t.
	\end{align*}
	Die Stetigkeit von $\partial_x \Phi$ wird in den Übungen bewiesen. Also ist $\Phi$ differenzierbar.
	Ausserdem ist $x \mapsto (b(x),a(x),x)$ eine differenzierbare Kurve in $\bbR^3$. Nach dem Satz über die
	Kettenregel ist somit $F$ differenzierbar und
	\begin{align*}
		\frac{\dd F}{\dd x} &= \nabla \Phi(b(x),a(x),x) \frac{\dd}{\dd x}
			\begin{pmatrix}
				b(x) \\ a(x) \\ x
			\end{pmatrix}\\
		&= \partial_b \Phi b'(x) + \partial_a\Phi a'(x) + \partial_x \Phi\\
		&= f(x,b(x))b'(x) - f(x,a(x))a'(x) + \int_{a}^{b} \partial_x f(x,t)\dd t. \qedhere
	\end{align*}
\end{proof}

\begin{satz}
	\label{10.10.4}
	Es ist
	\begin{equation*}
		\int_{-\infty}^{\infty} e^{-x^2}\dd x = \sqrt{\pi}.
	\end{equation*}
\end{satz}

\begin{proof}
	\begin{align*}
		\int_{-\infty}^{\infty} e^{-x^2}\dd x &= \int_{-\infty}^{0} e^{-x^2}\dd x + \int_{0}^{\infty} e^{-x^2}\dd x\\
		&= 2\int_{0}^{\infty} e^{-x^2}\dd x,
	\end{align*}
	wobei
	\begin{equation*}
		\int_{0}^{\infty} e^{-x^2}\dd x = \lim_{R\to \infty}\int_{0}^{R} e^{-x^2}\dd x.
	\end{equation*}
	Sei
	\begin{equation*}
		G(R) \coloneqq \left(\int_{0}^{R} e^{-x^2} \dd x\right)^2,
	\end{equation*}
	dann ist
	\begin{equation*}
		G'(R) = 2\int_{0}^{R} e^{-x^2} \dd x e^{-R^2} = 2\int_{0}^{R} e^{-(x^2 + R^2)} \dd x.
	\end{equation*}
	Wir substituieren $x = Rt$ und $\dd x = R\dd t$. Damit ist
	\begin{align*}
		G'(R) &= \int_{0}^{1}2R e^{-R^2(1+t^2)} \dd t\\
		&= - \int_{0}^{1} \frac{\partial}{\partial R} e^{-R^2(1+t^2)}\frac{1}{1+t^2} \dd t\\
\intertext{Wir verwenden Satz~\ref{10.10.2}}\\
		&= - \frac{\dd}{\dd R}\int_{0}^{1} e^{-R^2(1+t^2)} \frac{1}{1 + t^2} \dd t = -F'(R).
	\end{align*}
	Also $G'(R) + F'(R) = 0$ und
	\begin{align*}
		G(R) + F(R) &= G(0) + F(0) \\
		&= F(0) \\
		&= \int_{0}^{1} \frac{1}{1 + t^2} \dd t\\
		&= \arctan(1) \\
		&= \frac{\pi}{4}.
	\end{align*}
	Somit ist $G(R) = \pi/4 - F(R) \to \pi/4$ für $R\to \infty$, denn
	\begin{equation*}
		F(R) \le \int_{0}^{1} e^{-R^2} \dd t = e^{-R^2} \to 0 \qquad (R\to \infty).
	\end{equation*}
	Also ist
	\begin{align*}
		\int_{0}^{\infty} e^{-x^2} \dd x = \lim_{R\to\infty}\sqrt{G(R)} = \frac{\sqrt{\pi}}{2}. & \qquad \qedhere
	\end{align*}
\end{proof}

\section{Die Euler-Gleichung der Variationsrechnung}
\subsection{Brachistochrone}
Das Problem der Brachistochrone wurde von Johann Bernoulli 1696 gestellt. Die Frage ist, was ist die kürzeste
Verbindungsstrecke zwischen zwei Punkten, abhängig von der Geometrie des Problems (auf einer Kugeloberfläche ist die
kürzeste Strecke nicht eine "`gerade Linie"')? Das Problem ist in Abbildung~\ref{fig:Brachistochorne} skizziert.
Hier ist die Frage, welche Strecke, die die kürzeste Zeit benötigt um von dem Punkt $(0,0)$ zu dem Punkt $(L,h)$ zu
kommen, während das Gravitationspotential $V(y) = g\cdot y$ anliegt.

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines =middle,
					xlabel=x,
					xmin=0,
					xmax=5.2,
					xtick={0,5},
					xticklabel={$L$},
					ylabel=y,
					ytick={0,-2},
					yticklabels={0,$h$},]
			\addplot[blue,samples=200,domain=0:5] {-sqrt(x)};
		\end{axis}
	\end{tikzpicture}
	\caption{Skizze einer Brachistochrone im Beispiel des Freien Falls}
	\label{fig:Brachistochorne}
\end{figure}

Die Zeit, die das Teilchen benötigt um zum Ziel zu gelangen ist
\begin{equation*}
	T = \int_{0}^{L} \frac{\sqrt{1 + y'(x)^2}}{\sqrt{2gy(x)}}\dd x.
\end{equation*}
Welche Funktion $y(x)$ minimiert die Zeit $T$? Allgemeiner: Gesucht sei das Minimum oder Maximum einer
Funktion
\begin{equation*}
	F(\gamma) = \int_{a}^{b} L(x,\gamma(x),\gamma'(x))\dd x
\end{equation*}
bezüglich allen Kurven $\gamma:[a,b]\to \bbR$, differenzierbar, mit festen Werten $\gamma(a) = \gamma_1$
und$\gamma(b) = \gamma_2$. Die Funktion
\begin{align*}
	L:[a,b]\times \bbR\times \bbR &\to \bbR \\
	(x,y,y') &\mapsto L(x,y,y')
\end{align*}
heißt \emph{Lagrange-Funktion}. Im folgenden sei $L$ zwei mal stetig differenzierbar und $\gamma_1,\gamma_2 \in \bbR$
sind fest. Wir definieren
\begin{equation*}
	M \coloneqq \{\gamma:[a,b] \to \bbR\mid \text{$\gamma$ ist stetig differenzierbar und $\gamma(0) =\gamma_1$,
		$\gamma(b) = \gamma_2$}\}.
\end{equation*}
Falls $\gamma_0 \in M$ die Funktion $F:M\to \bbR$ minimiert, d.h.
\begin{equation*}
	F(\gamma_0) \le F(\gamma),\qquad \forall \gamma \in M,
\end{equation*}
dann gilt insbesondere
\begin{equation*}
	F(\gamma_0) \le F(\gamma_0 + \varepsilon h),\qquad \varepsilon\in\bbR
\end{equation*}
und $h:[a,b]\to \bbR$, stetig differenzierbar und $h(a) = h(b) = 0$ (dann ist $\gamma_0 + \varepsilon h \in M$ für
alle $\varepsilon \in \bbR$). Also
\begin{equation*}
	\delta F(\gamma_0,h) \coloneqq \frac{\dd}{\dd \varepsilon} F(\gamma_0 + \varepsilon h)\big|_{\varepsilon = 0}
		= 0
\end{equation*}
falls $\varepsilon \mapsto F(\gamma_0 + \varepsilon h)$ differenzierbar ist. $\delta F(\gamma_0,h)$ ist zu
verstehen als \emph{Richtungsableitung} von $F$ in $\gamma$ in Richtung $h$, wobei $h$ aus dem $\infty$-dimensionalen
Vektorraum
\begin{equation*}
	M_0 = \{h\in C^2([a,b])\mid h(a) =h(b) =0\}
\end{equation*}
zu nehmen ist. Die Abbildung
\begin{align*}
	\delta F(\gamma_0): M_0 &\to \bbR \\
	h &\mapsto \delta F(\gamma_0,h)
\end{align*}
heißt \emph{erste Variation} von $F$ in $\gamma_0$. $\delta F(\gamma_0,h)$ ist die erste Variation von $F$ in
$\gamma_0$ und Richtung $h$. Das Verschwinden der ersten Variation
\begin{equation*}
	\delta F(\gamma_0) = 0
\end{equation*}
ist also notwendig dafür, dass $F$ in $\gamma_0 \in M$ extremal ist. Dann heißt $\gamma_0$ ein stationärer oder
kritischer "`Punkt"' von $F$.

\begin{lemma}
	\label{10.11.1}
	Fundamentallemma der Variationsrechnung: Sei $f\in C([a,b])$. Falls
	\begin{equation*}
		\int_{a}^{b} f(x) h(x) \dd x = 0
	\end{equation*}
	für alle $h \in C^2([a,b])$ mit $h(a) = h(b) = 0$. dann gilt $f(x) = 0$ für alle $x \in [a,b]$.
\end{lemma}

\begin{theorem}
	\label{10.11.2}
	Folgende Aussagen über $\gamma \in M$ sind äquivalent:
	\begin{enumerate}
		\item $\delta F(\gamma) = 0$.
		\item $\gamma$ löst die \emph{Euler-Gleichung}
			\begin{equation*}
				\frac{\dd}{\dd x}\left(\frac{\partial L}{\partial y'}\right) (x,\gamma(x),\gamma'(x))
					= \frac{\partial L}{\partial y} (x,\gamma(x),\gamma'(x)).
			\end{equation*}
	\end{enumerate}
\end{theorem}

\begin{proof}
	Die Funktion
	\begin{equation*}
		\varepsilon \mapsto \int_{a}^{b} L(x,\gamma(x) +\varepsilon h(x), \gamma'(x) + \varepsilon h'(x))
	\end{equation*}
	ist differenzierbar nach Satz~\ref{10.10.2}, denn
	\begin{equation*}
		(x,\varepsilon) \mapsto L(x,\gamma(x) + \varepsilon h(x),\gamma'(x) +\varepsilon h'(x))
	\end{equation*}
	ist stetig auf $[a,b]\times \bbR$ und die partielle Ableitung nach $\varepsilon$ davon ist
	\begin{equation*}
		(x,\varepsilon) \mapsto \frac{\partial L}{\partial y} h(x) + \frac{\partial L}{\partial y'}h'(x)
	\end{equation*}
	ist stetig auf $[a,b]\times \bbR$. Es folgt
	\begin{align*}
		\delta f(\gamma_0, h) &= \int_{a}^{b} (\partial_y L) h(x) + (\partial_{y'} L)h'(x) \dd x \\
		&= \usb{=0}{(\partial_{y'} L) h(x) \big|^{b}_{a}}
			+ \int_{a}^{b}\left[(\partial_y L) - \frac{\dd}{\dd x} (\partial_{y'} L) \right] h(x) \dd x.
	\end{align*}
	Aussage \textit{1.}\ ist äquivalent zu $\delta F(\gamma_0,h) = 0$ für alle $h \in M_0$ was genau dann gilt,
	wenn die eckigen Klammern verschwinden für alle $x\in[a,b]$.
\end{proof}

Folgerung
\begin{enumerate}
	\item Hängt $L$ nicht von $y$ ab, dann ist für jede Lösung $\gamma$ die Euler-Gleichung
		\begin{equation*}
			\frac{\partial L}{\partial y'}(x,\gamma'(x)) = \mathrm{const}.
		\end{equation*}
	
	\item Hängt $L$ nicht von $x$ ab, dann ist für jede Lösung $\gamma$ die Euler-Gleichung
		\begin{equation*}
			E_\gamma = \frac{\partial L}{\partial y '}(\gamma(x),\gamma'(x)) \gamma'(x) - L(\gamma(x),\gamma'(x))
				= \mathrm{const}.
		\end{equation*}
\end{enumerate}

Beispiel: Beim Problem der Brachistochrone ist $L(y,y') = \sqrt{1 + (y')^2} / \sqrt{y}$ unabhängig von $x$. Also ist
\begin{equation*}
	C \coloneqq y' \frac{\partial L}{\partial y'} - L
		= \frac{(y')^2}{\sqrt{1 + (y')^2}\sqrt{y}} - \frac{\sqrt{1 + (y')^2}}{\sqrt{y}} = \mathrm{const}
\end{equation*}
für Lösungen $x\mapsto y(x)$ der Euler-Gleichungen. Auflösen nach $y'$ liefert
\begin{equation*}
	y' = \sqrt{\frac{1}{c^2 y} - 1}~,
\end{equation*}
was eine separierbare Differentialgleichung ist.







































