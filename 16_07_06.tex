% !TeX root = Ana2.tex

Die Anfangsbedingung ist $y(0) = 0$ und $c$ ist aus $y(L) = h$ zu bestimmen. Sei $b \equalDef 1/c^2 > 0$. Dann ist $y(x)$ implizit bestimmt durch (Satz \ref{7.4.1})
\begin{IEEEeqnarray*}{+rCl+x*}
\int_0^y \left(\frac{u}{b-u}\right)^{1/2} \diff u & = & x~,
\end{IEEEeqnarray*}
denn die DGL lässt sich umschreiben als 
\begin{IEEEeqnarray*}{+rCl+x*}
\frac{\dd y}{\dd x} = \left(\frac{b-y}{y}\right)^{1/2}~.
\end{IEEEeqnarray*}
Mit der Substitution $u = b\sin^2(\varphi/2) = \frac{b}{2}(1 - \cos \phi)$ lässt sich das Integral berechnen (Übung). Diese Lösung macht das Integral
\begin{IEEEeqnarray*}{+rCl+x*}
\int_0^L \left(\frac{1 + (y')^2}{y}\right)^{1/2} \dd x
\end{IEEEeqnarray*}
stationär. Es bleibt aber noch zu zeigen, dass ein Minimum vorliegt.

\begin{remark}
Ist die Lagrangefunktion $L$ definiert auf $[a, b] \times \bbR^n \times \bbR^n$ mit $n > 1$ und zwei mal stetig differenzierbar, dann ist das Verschwinden der ersten Variation von
\begin{IEEEeqnarray*}{+rCl+x*}
\int_a^b L(x, \gamma(x), \gamma'(x)) \dd x
\end{IEEEeqnarray*}
bezüglich allen $C^2$-Kurven $\gamma: [a, b] \to \bbR^n$ mit festen Endpunkten äquivalent dazu, dass $\gamma$ die $n$ Eulergleichungen
\begin{IEEEeqnarray*}{+rCl+x*}
\frac{\diff}{\diff x} \frac{\partial L}{\partial y_k'} - \frac{\partial L}{\partial y_k} & = & 0 \quad (k \in \{1, \hdots, n\}) & (EL)
\end{IEEEeqnarray*}
löst. Das folgt aus
\begin{IEEEeqnarray*}{+rCl+x*}
\delta F(\gamma, h) & = & \sum_{k=1}^n \int_a^b \left[ \frac{\partial L}{\partial y_k} - \frac{\diff}{\diff x}\left(\frac{\partial L}{\partial y_k'}\right)\right] h_k(x) \diff x
\end{IEEEeqnarray*}
und aus Lemma \ref{10.11.1}.

Sei $\gamma$ eine Lösung der Eulergleichung (EL). Wenn $L$ nicht von $y_k$ abhängt, dann ist $\partial L/\partial y_k'$ eine Konstante und wenn $L$ nicht von $x$ abhängt, dann ist
\begin{IEEEeqnarray*}{+rCl+x*}
\sum_{k=1}^n \frac{\partial L}{\partial y_k'} y_k - L
\end{IEEEeqnarray*}
konstant.
\end{remark}

\begin{remark}
Die Newtonsche Gleichung
\begin{IEEEeqnarray*}{+rCl+x*}
m \ddot{x} = -\nabla V(x) \quad \left(\dot{x} = \frac{\diff x}{\diff t}\right)
\end{IEEEeqnarray*}
für ein Teilchen mit Masse $m$ und Position $x(t) \in \bbR^n$ ist die Eulergleichung (bzw. das System von $n$ Eulergleichungen) zur Lagrangefunktion
\begin{IEEEeqnarray*}{+rCl+x*}
L(x, \dot{x}) & = & \frac{m}{2}\dot{x}^2 - V(x)~. \qquad (\dot{x}^2 \equalDef \dProd{\dot{x}}{\dot{x}})
\end{IEEEeqnarray*}
Jede Lösung $\gamma: I \to \bbR^n$ der Newtonschen Gleichung macht also die \emph{Wirkung}
\begin{IEEEeqnarray*}{+rCl+x*}
S(\gamma) & = & \int \left(\frac{m}{2} \dot{\gamma}^2 - V(\gamma(t))\right) \diff t
\end{IEEEeqnarray*}
für beliebige $t_0, t_1 \in I$ stationär (Prinzip der kleinsten Wirkung).
\end{remark}

Da $L$ nicht von $t$ abhängt, ist die \emph{Energie}
\begin{IEEEeqnarray*}{+rCl+x*}
\sum_{k=1}^n \frac{\partial L}{\partial \dot{x}_k} \dot{x}_k - L & = & m\dot{x}^2 - L(x, \dot{x}) \\
& = & \frac{m}{2}\dot{x}^2 + V(x) 
\end{IEEEeqnarray*}
erhalten, d. h. $t \mapsto \frac{m}{2} \dot{\gamma}(t)^2 + V(\gamma(t))$ ist konstant für jede Lösung $\gamma$ der Newtonschen Gleichung.

\section{Vektorfelder und Kurvenintegrale}

Sei $D \subseteq \bbR^n$ offen. Eine Abbildung $v: D \to \bbR^n$ (Achtung: gleiche Dimensionen des Urbild- und Bildbereichs) heißt \emph{Vektorfeld} auf $D$, wenn die Vorstellung nahegelegt werden soll, dass $v(x)$ im Punkt $x \in D$ angeheftet ist. $v$ heißt \emph{Gradientenfeld}, wenn $v(x) = \nabla \phi(x)$ mit einer differenzierbaren Funktion $\phi: D \to \bbR$ und $\phi$ heißt \emph{Poetntial} von $v$.

Ist $v = \nabla \phi: D \to \bbR^n$ ein \emph{stetig differenzierbares} Gradientenfeld, dann gilt nach dem Satz von Schwarz:
\begin{IEEEeqnarray*}{+rCl+x*}
\partial_i v_k & = & \partial_i \partial_k \phi = \partial_k \partial_i \phi = \partial_k v_i~.
\end{IEEEeqnarray*}
Also 
\begin{equation*}
\boxed{\partial_i v_k - \partial_k v_i = 0}
\end{equation*}
für alle $i, k$. Dieses System von Gleichungen nennt man \emph{Integrabilitätsbedingungen} an $v$.

Für $v: D \subseteq \bbR^2 \to \bbR^2$ lauten die IB:
\begin{IEEEeqnarray*}{+rCl+x*}
\partial_1 v_2 - \partial_2 v_1 = 0~.
\end{IEEEeqnarray*}
Für $v: D \subseteq \bbR^3 \to \bbR^3$ lauten die IB:
\begin{IEEEeqnarray*}{+rCl+x*}
\partial_2 v_3 - \partial_3 v_2 & = & 0 \\
\partial_3 v_1 - \partial_1 v_3 & = & 0 \\
\partial_1 v_2 - \partial_2 v_1 & = & 0
\end{IEEEeqnarray*}
bzw. $\rot v = 0$ (\enquote{Rotation von $v$}), wobei $\rot v$ formal das Kreuzprodukt von $\nabla$ und $v$ ist, d. h. 
\begin{IEEEeqnarray*}{+rCl+x*}
\rot v & = & \nabla \times v = \begin{pmatrix}
\partial_1 \\ \partial_2 \\ \partial_3
\end{pmatrix} \times \begin{pmatrix}
v_1 \\ v_2 \\ v_3
\end{pmatrix} \\
& = & \begin{pmatrix}
\partial_2 v_3 - \partial_3 v_2 \\
\partial_3 v_1 - \partial_1 v_3 \\
\partial_1 v_2 - \partial_2 v_1
\end{pmatrix}~.
\end{IEEEeqnarray*}
(Prof. Griesemer schreibt hier $\land$ statt $\times$, um Verwechslung von $\times$ und $x$ zu vermeiden.)

Eine Teilmenge $D \subseteq \bbR^n$ heißt \emph{sternförmig} (bezüglich $a \in D$), wenn ein Punkt $a \in D$ existiert, sodass $[a, x] \subseteq D$ für alle $x \in D$.

\begin{theorem}
Sei $D \subseteq \bbR^n$ offen und sternförmig und sei $v: D \to \bbR^n$ ein $C^1$-Vektorfeld auf $D$, welches die IB erfüllt. Dann ist $v$ ein Gradientenfeld.

\begin{proof}
Wir nehmen an, $D$ sei sternförmig bezüglich $0 \in \bbR^n$. Sei
\begin{IEEEeqnarray*}{+rCl+x*}
\phi(x) & \equalDef & \int_0^1 \dProd{v(tx)}{x} \diff t~.
\end{IEEEeqnarray*}
Nach Voraussetzung an $v$ ist $\varphi$ stetig (Satz \ref{10.10.1}) und nach Satz \ref{10.10.2} gilt
\begin{IEEEeqnarray*}{+rCl+x*}
\partial_k \phi(x) & = & \int_0^1 \frac{\partial}{\partial x_k} \sum_{i=1}^n \dProd{v(tx)}{x} \diff t \\
& = & \int_0^1 \left(\sum_{i=1}^n \partial_k v_i(tx)tx_i + v_k(tx)\right) \diff t \\
& \stackrel{\text{IB}}{=} & \int_0^1 \left(\sum_{i=1}^n \partial_i v_k(tx) x_i t + v_k(tx) \right) \diff t \\
& = & \int_0^1 \frac{\diff}{\diff t} (v_k(tx)t) \diff t \\
& = & v_k(xt)t \Big|_0^1 = v_k(x)~. & \qedhere
\end{IEEEeqnarray*}
\end{proof}
\end{theorem}

Sei $v: D \subseteq \bbR^n \to \bbR^n$ ein stetiges Vektorfeld und $\gamma: [a, b] \to \bbR^n$ eine $C^1$-Kurve mit $\Spur(\gamma) \subseteq D$. Dann heißt
\begin{IEEEeqnarray*}{+rCl+x*}
\int_\gamma v(x) \dotP \diff x & \equalDef & \int_\gamma \sum_{k=1}^n v_k(x) \diff x_k = \int_a^b \dProd{v(\gamma(t))}{\dot{\gamma}(t)} \diff t
\end{IEEEeqnarray*}
\emph{Kurvenintegral} von $v$ längs $\gamma$. Ist $\gamma$ nur stückweise stetig differenzierbar und auf den Teilintervallen der Partition $a = t_0 < t_1 < \hdots < t_n = b$ stetig differenzierbar, dann
\begin{IEEEeqnarray*}{+rCl+x*}
\int_\gamma v(x) \dotP \diff x & \equalDef & \sum_{k=1}^n \int_{t_{k-1}}^{t_k} \dProd{v(\gamma(t))}{\dot{\gamma}(t)} \diff t~.
\end{IEEEeqnarray*}
Solche Kurven nennen wir $PC^1$-Kurven (piecewise continuously differentiable). Sei $\sigma: [a, b] \to [c, d]$ eine $C^1$-Parametertransformation. Falls $\sigma$ orientierungstreu ist ($\sigma(c) = a, \sigma(d) = b$), dann
\begin{IEEEeqnarray*}{+rCl+x*}
\int_{\gamma \circ \sigma} v(x) \dotP \diff x & = & \int_\gamma v(x) \dotP \diff x~.
\end{IEEEeqnarray*}
Falls $\sigma$ orientierungsumkehrend ist, dann 
\begin{IEEEeqnarray*}{+rCl+x*}
\int_{\gamma \circ \sigma} v(x) \dotP \diff x & = & -\int_\gamma v(x) \dotP \diff x~.
\end{IEEEeqnarray*}

\begin{proof}
Sei $\gamma$ eine $C^1$-Kurve. Dann gilt
\begin{IEEEeqnarray*}{+rCl+x*}
\int_{\gamma \circ \sigma} v(x) \dotP \diff x & = & \int_c^d \dProd{v(\gamma(\sigma(s))}{\frac{\diff}{\diff s} \gamma(\sigma(s))} \diff s \\
& = & \int_c^d \dProd{v(\gamma(\sigma(s)))}{\dot{\gamma}(\sigma(s)) \dot{\sigma}(s)} \diff s \\
& = & \int_{\sigma(c)}^{\sigma(d)} \dProd{v(\gamma(t))}{\dot{\gamma(t)}} \diff t \\
& = & \pm \int_a^b \dProd{v(\gamma(t))}{\dot{\gamma(t)}} \diff t = \pm \int_\gamma v(x) \dotP \diff x~.
\end{IEEEeqnarray*}
Das Kurvenintegral $\int_\gamma v(x) \dotP \diff x$ hängt folglich nur von der orientierten Kurve $[\gamma]$ ab und nicht von der Parametrisierung (vgl. dazu Aufgabe 11.5). Eine orientierte Kurve nennt man auch \emph{Integrationsweg}. Insbesondere können wir durch eine Parametertransformation $\sigma: [0, 1] \to [a, b], t \mapsto a + t(b - a)$ jede parametrisierte Kurve $\gamma: [a, b] \to \bbR^n$ auf $[0, 1]$ zurückziehen, ohne dass sich dabei das Kurvenintegral $\int_\gamma v(x) \dotP \diff x$ ändert.
\end{proof}
