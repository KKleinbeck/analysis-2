% !TeX root = Ana2.tex

\section{Homotopie von Kurven}
Zwei geschlossenen Kurven $\gamma_0, \gamma_1: [a,b]\to D\subset\bbR^n$ heißen \emph{homotop in $D$}, wenn es eine
\emph{stetige} Abbildung
\begin{equation*}
	M:[0,1]\times [a,b] \to D
\end{equation*}
gibt mit
\begin{align*}
	M(0,t) &= \gamma_0(t) \\
	M(1,t) &= \gamma_1(t) \\
	M(s,a) &= M(s,b).
\end{align*}
M heißt \emph{Homotopie}.
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\draw[-{Latex[length=5pt]}] (0,0) node[anchor=east] {$a$} -- (0,2) node[anchor=east] {$b$};
		\draw (0,0) node[anchor=north] {0} -- (2,0) node[anchor=north] {1};
		\draw[-{Latex[length=5pt]}] (2,0) -- (2,1) -- (2,2);
		\draw (0,2) -- (2,2);
	\end{tikzpicture}
	\caption{}
	\label{fig:homotopie}
\end{figure}

Die Kurve $\gamma_0$ wird stetig in die Kurve $\gamma_1$ deformiert, ohne dass dabei $D$ verlassen wird.

Beispiel: Sind $\gamma_0, \gamma_1:[a,b]\to D$ geschlossene Kurven mit der Eigenschaft, dass 
$[\gamma_0(t),\gamma_1(t)]\subset D$ für alle $t\in [a,b]$, dann ist
\begin{equation*}
	M(s,t) = (1- s)\gamma_0(t) + s\gamma_1(t)
\end{equation*}
eine Homotopie zwischen $\gamma_0$ und $\gamma_1$ ($\gamma_0$ und $\gamma_1$ sind \emph{linear homotop}).

\begin{satz}
	\label{10.13.1}
	Sei $D\subset \bbR^n$ ein Gebiet und $v:D \to \bbR^n$ ein $C^1$-Vektorfeld, welches die Integrabilitätsbedingungen
	erfüllt. Sind zwei geschlossene $PC^1$-Kurven $\gamma_0,\gamma_1:[a,b]\to D$ homotop in $D$, dann gilt
	\begin{equation*}
		\int_{\gamma_0}v(x)\dd x = \int_{\gamma_1}v(x) \dd x.
	\end{equation*}
\end{satz}

\begin{proof}
	Wir geben erst einen Beweis für einen Spezialfall, dann betrachten wir den allgemeinen Fall.
	\begin{enumerate}
		\item Wir nehmen an $\gamma_0, \gamma_1$ sind linear homotope $C^1$-Kurven. Sei
			\begin{equation*}
				\gamma_s(t) = (1-s)\gamma_0(t) + s\gamma_1(t) \equiv \gamma_0(t) + s\delta(t).
			\end{equation*}
			Sei
			\begin{equation*}
				\varphi(s) = \int_{\gamma_s} v(x) \dd x
					= \int_{a}^{b} \langle v(\gamma_s(t)), \dot{\gamma}_s(t) \rangle \dd t.
			\end{equation*}
			Ableiten liefert
			\begin{equation*}
				\varphi'(s) = \int_a^b \langle v'(\gamma_s(t)) \delta(t), \dot{\gamma}_s(t)\rangle
					+ \langle v(\gamma_s(t)), \dot{\delta}(t)\rangle \diff t.
			\end{equation*}
			Die Integrabilitätsbedingung $\partial_i v_k = \partial_k v_i$ impliziert, dass die Jacobimatrix $v'(x)$
			symmetrisch ist. Also
			\begin{align*}
				\varphi'(s) &= \int_a^b\frac{\dd }{\dd s} \langle v(\gamma_s(t)), \dot{\gamma}_s(t)\rangle \diff t \\
				&= \int_a^b \langle \delta(t), v'(\gamma_s(t)) \dot{\gamma}_s(t) \rangle
					+ \langle \dot{\delta}(t), v(\gamma_s(t)) \rangle \diff t \\
				&= \int_a^b \frac{\dd }{\dd t} \langle \delta(t), v(\gamma_s(t))\rangle \diff t.
			\end{align*}
			Damit erhalten wir
			\begin{align*}
				\varphi'(s) &= \int_{a}^{b} \frac{\dd}{\dd s}\langle v(\gamma_s(t)), \dot{\gamma}_s(t) \rangle \dd t\\
				&= \int_{a}^{b} \frac{\dd }{\dd t} \langle \delta(t), v(\gamma_s(t))\rangle \dd t\\
				&= \langle \delta(a), v(\gamma_s(a))\rangle - \langle \delta(b) , v(\gamma_s(b))\rangle = 0,
			\end{align*}
			denn $v(\gamma_s(a)) = v(\gamma_s(b))$ und $\delta(b) - \delta(a) = 0$, da die Kurven $\gamma_0$ und
			$\gamma_1$ geschlossen sind. Das Schöne an diesem Beweis ist, dass wir nur die Integrabilitätsbedingung
			benötigt haben.
		
		\item Sei $Q = [0,1] \times [a,b]$. $Q$ ist kompakt und $H$ ist stetig, also ist $H(Q) \subset \bbR^n$
			kompakt. Da $H(Q) \subset D$ und $\bbR^n\setminus D$ abgeschlossen ist, folgt
			\begin{equation*}
				\varepsilon = \operatorname{dist} (H(Q), \bbR^n\setminus D) > 0.
			\end{equation*}
			Auf $Q$ ist $H$ gleichmäßig stetig (Satz~\ref{10.1.4}), also existiert $\delta >0$, so dass
			\begin{equation*}
				| (\varepsilon', t') - (\varepsilon,t)| < \delta \Rightarrow| H(s',t') - H(s,t)| <\varepsilon.
			\end{equation*}
			Wir definieren die Partition $0 \eqqcolon s_0 < \dots <s_m \coloneqq 1$ und
			$a \eqqcolon t_0 < \dots < t_n \eqqcolon b$ mit $t_k - t_{k-1} = (b-a)/k$ und $n\in \bbN$ so groß, dass
			der Abstand von zwei Punkten in $[s_{i-1},s_i] \times [t_{j-1},t_j]$ kleiner als $\delta$ ist.
			Sei $P_{ij} = H(s_i,t_j)$. Dann ist $P_{i\pm1,j\pm1} \in B_\varepsilon(P_{ij}) \subset D$. Da
			$B_\varepsilon(P_{ij})$ konvex ist, ist die Kurve $\delta\Omega_{ij}$ (siehe
			Abbildung~\ref{fig:delta Gamma}) in $B_\varepsilon(P_{ij})$ wo $v$ ein Potential hat geschlossen
			(Satz~\ref{10.12.1}) . Also
			\begin{equation*}
				\int_{\delta\Omega} v(x) \dd x = 0.
			\end{equation*}
			Sei $a_{i,j}$ $i=0,\dots,n$ der Streckenzug durch die Punkte $P_{i,0},\dots, P_{i,n}$. Dann gilt
			\begin{equation*}
				\int_{\alpha_i} v(x)\dd x - \int_{\alpha_{i-1}} v(x)\dd x
					= \sum_{j = 1}^{n} \int_{\delta\Omega_ij} v(x) \dd x = 0.
			\end{equation*}
			Es bleibt zu zeigen, dass
			\begin{equation*}
				\int_{\gamma_0} v(x) \dd x = \int_{\alpha_i} v(x) \dd x
			\end{equation*}
			und
			\begin{equation*}
				\int_{\gamma_1} v(x) \dd x = \int_{\alpha_n} v(x) \dd x.
			\end{equation*}
			Die Kurvenstücke $\gamma_0 [t_{j-1},t_j]$ und $[P_{0,j-1}, P_{0,j}]$ liegen in $B_\varepsilon(P_{0,j})$, 
			wo $v$ ein Potential hat und $\gamma_0(t_{j-1}) = H(0, t_{j-1}) = H(t_0, t_{j-1}) = P_{0,j-1}$,
			$\gamma_0(t_j) = P_{0,j}$. Also
			\begin{equation*}
				\int_{\gamma_0} v(x) \dd x = \sum_{j=1}^{n}\int_{\gamma_0 [t_{j-1},t_j]} v(X)\dd x
					= \int_{[P_{0,j-1},P_{0,j}]} v(x) \dd x = \int_{\alpha_0} v(x) \dd x.
			\end{equation*}
			Ebenso sieht man
			\begin{equation*}
				\int_{\gamma_1}v(x) \dd x = \int_{\alpha_1}v(x) \dd x.
			\end{equation*}
			Damit folgt die Behauptung. \qedhere
	\end{enumerate}
\end{proof}

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}[scale=1.5]
		\draw (0,0) rectangle (1,1);
		\draw[dashed] (0,0.2) -- (1,0.2)
			(0,0.3) -- (1,0.3)
			(0.2,0) -- (0.2,1)
			(0.3,0) -- (0.3,1);
		
		\draw[->] (1.2,0.5) -- (1.8,0.5);
		
		\node[fill=none] at (2,0) (H1) {};
		\node[anchor=north] at (H1) {$P_{i-1,j-1}$};
		\node[fill=none] at (2.6,1) (H2) {};
		\node[anchor=south,fill=none] at (H2) {$P_{i-1,j}$};
		\node[fill=none] at (3.4,0.7) (H3) {};
		\node[anchor=west] at (H3) {$P_{i,j}$};
		\node at (2.7,0.2) (H4) {};
		\node[anchor=north west] at (H4) {$P_{i,j-1}$};
		
		\draw[smooth] (H1) -- +(0.2,0.5) -- (H2);
		\draw[smooth] (H2) -- +(0.4,-0.1) -- (H3);
		\draw[smooth] (H3) -- +(-0.4,-0.3) -- (H4);
		\draw[smooth] (H4) -- (H1);
		
		\node at (2.7,0.4) {$\delta\Omega$};
	\end{tikzpicture}
	\caption{Skizze zu $\delta\Omega$}
	\label{fig:delta Gamma}
\end{figure}

Eine geschlossene Kurve $\gamma:[0,1] \to \bbR^n$ heißt \emph{Nullhomotop} in $D\subset \bbR^n$. wenn sie homotop zu
einer Punktkurve $\gamma_p:[a,b] \to D$, $\gamma_p(t) = p$ zusammen gezogen werden kann. Ein Gebiet $D \subset \bbR^n$
heißt \emph{einfach zusammenhängend}, wenn jede geschlossene Kurve in $D$ nullhomotop in $D$ ist ($D$ hat keine
"`Löcher"').

Beispielsweise ist $\bbR^2\setminus\{(0,0)\}$ \emph{nicht} einfach zusammenhängend, denn $\gamma:[0,2\pi] \to \bbR^2$,
$\gamma(t) = (\cos t, \sin t)$ ist nicht nullhomotop in $\bbR^2\setminus \{(0,0)\}$.

Ein sternförmiges Gebiet $D \in \bbR^n$ ist einfach zusammenhängend
\begin{proof}
	Sei $D$ sternförmig bezüglich $P \in D$ und $\gamma:[a,b]\to D$ geschlossen, dann ist $\gamma$ nullhomotop, denn
	\begin{equation*}
		M(s,t) = (1-s)\gamma(t) + sp
	\end{equation*}
	ist eine Homotopie von $\gamma$ auf $D$
\end{proof}

\begin{theorem}
	\label{10.13.2}
	Auf einem einfach zusammenhängenden Gebiet $D\subset \bbR^n$ ist ein $C^1$-Vektorfeld, das die
	Integrabilitätsbedingungen erfüllt, ein Gradientenfeld.
\end{theorem}

\begin{proof}
	Für jede geschlossene Kurve $\gamma$ in $D$ gilt
	\begin{equation*}
		\int_{\gamma} v(x) \dd x = 0.
	\end{equation*}
	Also ist $v$ ein Gradientenfeld nach Theorem~\ref{10.12.4}.
\end{proof}


































