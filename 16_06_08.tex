% !TEX root = Ana2.tex

\begin{satz}
	\label{10.1.3}
	Sei $f: D(f)\subset \bbR^n \to \bbR^m$ stetig im Punkt $a$ und $g: D(g) \subset\bbR^m \to \bbR^l$ stetig in
	$b = f(a) \in D(g)$. Dann ist $g\circ f$ stetig in $a$.
\end{satz}

\begin{proof}
	Sei $(x_n)$ eine Folge in $D(g\circ f) = D(f) \cap f^{-1}(D(g))$, mit $x_n \to a$. Da $f$ stetig in $a$
	gilt $f(x_n) \to f(a) = b$. Da $g$ stetig in $b$ ist, folgt
	\begin{equation*}
		(g\circ f)(x_n) = g(f(x_n)) \to g(b) = g(f(a)) = (g\circ f) (a), \quad n\to \infty. \qedhere
	\end{equation*}
\end{proof}

\begin{theorem}
	\label{10.1.4}
	Sei $K \subset \bbR^n$ kompakt, $K\ne \emptyset$, und $f: K\to \bbR^m$ stetig. Dann gilt
	\begin{enumerate}
		\item $f(K) \subset \bbR^m$ ist kompakt.
		\item $f$ ist gleichmäßig stetig.
		\item Falls $m = 1$, dann nimmt $f$ auf $K$ ihr Maximum und ihr Minimum an. D.h. es existieren $a,b \in K$ mit
			\begin{equation*}
				f(a) \le f(x) \le f(b),
			\end{equation*}
			für alle $x\in K$.
	\end{enumerate}
\end{theorem}

\begin{proof}
	Wir zeigen, dass $f(K)$ folgenkompakt ist. Sei $(y_k)$ eine Folge in $f(K)$ und $x_k \in K$ mit $f(x_k) = y_k$ für
	$k \in \bbN$. Da $K$ kompakt ist, hat die Folge $(x_k)$ eine konvergente Teilfolge $(x_{n_k})$ mit
	$x_{n_k}\to x \in K$. Da $f$ stetig ist, gilt $f(x_{n_k}) \to f(x)$. Aber $f(x_{n_k}) = y_{n_k}$. Das heißt
	$y_{n_k} \to f(x) \in f(K)$. Also ist $f(K)$ folgenkompakt und somit kompakt.
	
	Der zweite und der dritte Punkt lassen sich wie im Fall $m=n=1$ beweisen.
\end{proof}

\begin{satz}
	\label{10.1.5}
	Ist $f:K\subset \bbR^n \to \bbR^m$ stetig und injektiv auf einer kompakten Menge $K$, dann ist auch
	$f^{-1}: f(K) \to K$ stetig.
\end{satz}

\begin{proof}
	Wir zeigen, dass $f^{-1}$ folgenstetig ist. Annahme: in $y \in f(K)$ ist $f^{-1}$ nicht stetig. Dann existiert
	eine Folge $(y_k)$ mit $y_k \to y$ aber $f^{-1}(y_k) \not \to f^{-1}(y)$. Nach Übergang zu einer Teilfolge
	(die wir auch mit $(y_k)$ bezeichnen) gilt
	\begin{equation*}
		|f^{-1}(y_k) - f^{-1}(y) | \ge \varepsilon
	\end{equation*}
	für alle $k \geq N$ mit $\varepsilon > 0$. Sei $x_k = f^{-1}(y_k) \in K$. Da $K$ kompakt ist, existiert eine Teilfolge
	$(x_{n_k})$ mit $x_{n_k}\to x \in K$. Da $f$ stetig ist, folgt $f(x_{n_k}) \to f(x)$. Andererseits
	$f(x_{n_k}) = y_{n_k} \to y$ und somit $y = f(x)$. Das heißt $x = f^{-1}(y)$ und somit
	\begin{equation*}
		|x_{n_k} - x| = |f^{-1}(y_{n_k}) - f^{-1}(y)| \ge \varepsilon
	\end{equation*}
	für alle $k \geq N$. Das ist im Widerspruch zu $x_{n_k} \to x$. Also war die Annahme falsch und $f^{-1}$ ist stetig.
\end{proof}

\begin{satz}
	\label{10.1.6}
	Ist $f:D\subset\bbR^n \to \bbR^m$, der gleichmäßige Limes stetiger Funktionen $f_k$, $k \in \bbN$, dass heißt
	\begin{equation*}
		\sup_{x\in D}|f_k(x) - f(x)| \to 0\qquad (k\to\infty),
	\end{equation*}
	dann ist $f$ stetig.
\end{satz}

Der Beweis der Satzes ist eine Kopie des Beweises im Fall $n = m = 1$.

\subsection{Grenzwerte}
$f:D\subset \bbR^n \to \bbR^m$ hat im Punkt $x_a\in \bbR^n$ den Grenzwert $a \in \bbR^n$, in Zeichen
\begin{equation*}
	\lim_{x \to x_a} f(x) = a
\end{equation*}
wenn zu jedem $\varepsilon > 0$ ein $\delta > 0$ existiert, so dass
\begin{equation*}
	x \in D , 0 < |x - x_a| < \delta \Rightarrow |f(x) - a| < \varepsilon.
\end{equation*}
Falls $x_a$ ein Häufungspunkt von $D$ ist, dann ist $\lim_{x\to x_a} f(x)$ eindeutig.

\begin{satz}
	\label{10.1.7}
	Eine Funktion $f:D\subset \bbR^n \to \bbR^m$ ist genau dann stetig im Punkt $x_0\in D$, wenn
	\begin{equation*}
		\lim_{x\to x_0} f(x) = f(x_0).
	\end{equation*}
\end{satz}

\begin{satz}
	\label{10.1.8}
	Folgende Aussagen über $f:D\subset \bbR^n\to \bbR^m$, $x_0\in \bbR^n$ und $a \in \bbR^m$ sind äquivalent:
	\begin{enumerate}
		\item $\lim_{x\to x_0} f(x) = a$.
		\item $\lim_{x\to x_0} f_k(x) = a_k$ für $k = 1\dots m.$
		\item $\lim_{k\to \infty} f(x_k) = a$ für jede Folge $(x_k)$ in $D\setminus\{x_0\}$ mit $\lim x_k = x_0$.
		\item Die Funktion $\tilde{f} : D\cup \{x_0\}\to \bbR^m$ definiert durch
			\begin{equation*}
				\tilde{f} \coloneqq
				\begin{cases}
					f(x) & x \in D\setminus\{x_0\}\\
					a & x = x_0
				\end{cases}
			\end{equation*}
			ist stetig in $x_0$.
	\end{enumerate}
\end{satz}

\begin{proof}
	Wir zeigen \textit{1. $\Leftrightarrow$ 3. $\Leftrightarrow$ 4.} wie für $n = m = 1$.
	Für $x \in D\setminus\{x_0\}$ gilt
	\begin{equation*}
		|f(x) - a| = |\tilde{f}(x) - \tilde{f}(x_0)|.
	\end{equation*}
	Also
	\begin{equation*}
		\lim_{x\to x_0} f(x) = a \Leftrightarrow \lim_{x\to x_0} \tilde{f}(x) = \tilde{f}(x_0)
	\end{equation*}
	ist stetig in $x_0$.
	Daraus folgt $\lim_{k\to \infty} \tilde{f}(x_k) = \tilde{f}(x_0)$ für jede Folge $(x_k)\in D\setminus\{x_0\}$ mit
	$x_k \to x_0$, woraus $\lim f(x_k) = a$ folgt.
\end{proof}

Für Grenzwerte von Funktionen $f:D\subset \bbR^n \to \bbC$ gelten diese Rechenregeln wie im Fall $n=1$. Vergleiche
dafür den Satz~4.4.4 aus der Analysis~1, z.B.
\begin{equation*}
	\lim_{x\to x_0} f(x) = a, \quad \lim_{ x\to x_0}g(x) = b \Rightarrow \lim_{x\to x_0}(f+g)(x) = a+ b
\end{equation*}
und so weiter.

\begin{satz}
	\label{10.1.9}
	Cauchy-Kriterium: Sei $f:D\subset\bbR^n \to \bbC$ und $x_0\in\bbR^n$. Dann sind äquivalent:
	\begin{enumerate}
		\item $\lim_{x\to x_0} f(x)$ existiert.
		
		\item Für jedes $\varepsilon >0$ existiert ein $\delta > 0$ mit
			\begin{equation*}
				x,y\in \dot{B}_{\delta}(x_0) \cap D \Rightarrow |f(x)-f(y)| < \varepsilon.
			\end{equation*}
	\end{enumerate}
\end{satz}

Der Beweis ist wie in einer Dimension.

\section{Kurven in $\bbR^n$}
Eine (parametrisierte) Kurve in $\bbR^n$ ist eine stetige Abbildung
\begin{equation*}
	\gamma: I \to \bbR^n\qquad t\mapsto (x_1(t),\dots,x_n(t))
\end{equation*}
auf einem Intervall $I$ ($t$ kommt vom englischen time). Die \emph{Spur} von $\gamma$,
$\Spur \gamma \coloneqq \{\gamma(t)\mid t\in I\}$ ist das Bild von $\gamma$ (nicht der Graph:
$\Gamma(\gamma) = \{(t,\gamma(t)) \mid t\in I\}$).

Beispiel: Ist $f:I\to \bbR$ stetig, dann ist $\gamma(t) = (t, f(t) )$, $t\in I$ eine Kurve in $\bbR^2$ und
$\Spur\gamma = \Gamma(f)$.

\begin{figure}[!b]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle]
			\addplot[domain=-2:2,blue,samples=200] (x^2, x^3);
		\end{axis}
	\end{tikzpicture}
	\caption{Neilsche Parabel}
	\label{fig:Niel}
\end{figure}

$\gamma$ heißt \emph{differenzierbar} in Punkt $t\in I$, wenn
\begin{equation*}
	\dot{\gamma}(t) \coloneqq \lim_{h \to 0} \frac{1}{h} (\gamma(t+h) - \gamma(t))
\end{equation*}
existiert. Nach Satz~\ref{10.1.8} ist das genau dann der Fall, wenn $x_1, \dots, x_n$ in $t$ differenzierbar sind
und
\begin{equation*}
	\dot{\gamma}(t) = (\dot{x}_1(t),\dots,\dot{x}_n(t)).
\end{equation*}
Dieser Vektor heißt \emph{Tangentialvektor} oder \emph{Geschwindigkeitsvektor} von $\gamma$ in $t$.
Eine Kurve $\gamma:I\to \bbR^n$ heißt \emph{regulär}, wenn $\dot{\gamma}(t) \ne 0$ für alle $t\in I$. Wenn
$\dot{\gamma}(t_0) = 0$, dann heißt $\gamma$ \emph{singulär} in $t_0$.

Beispiel dafür ist die Neilsche Parabel
\begin{equation*}
	\gamma(t) = (t^2, t^3)~.
\end{equation*}
Sie ist singulär in $t = 0$, denn $\dot{\gamma} = (2t,3t^2) = 0$ für $t = 0$. Die Kurve ist in
Abbildung~\ref{fig:Niel} skizziert.

Ein weiteres Beispiel ist $\gamma(t) = (t^3,0)$. Die Kurve ist auch singulär in $t=0$, aber die Spur ist
$\Spur \gamma = \{(x,0)\mid x \in\bbR\}$. Man sieht der Spur die Singularität nicht an.












