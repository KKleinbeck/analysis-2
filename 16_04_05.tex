% !TEX root = Ana2.tex

\setcounter{chapter}{5}
\chapter{Integralrechnung}
\section{Treppenfunktionen}
Eine Funktion $\varphi: [a,b] \to \bbC$ heißt \emph{Treppenfunktion}, wenn es eine Partition
\begin{equation*}
	a = x_0 < x_1 < \dots < x_n = b
\end{equation*}
des Intervalls $[a,b]$ gibt, so dass $\varphi$ auf jeden \emph{offenen} Teilintervall $(x_i,x_{i+1})$ konstant
ist. Die Werte $\varphi(x_i)$ sind keinen Einschränkungen unterworfen.
\begin{figure}[!b]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle]
			\addplot[domain=-1:0,red] {1};
			\addplot[domain= 0:1,red] {2};
			\addplot[domain= 1:2,red] {-1};
		\end{axis}
	\end{tikzpicture}
	\caption{Skizze einer Treppenfunktion}
\end{figure}
Falls $\varphi(x) = c_i$ auf $(x_i,x_{i+1})$, dann ist das Integral definiert als
\begin{equation*}
	\int_{a}^{b}\varphi(x)\dd x \coloneqq \sum_{i = 0}^{n} c_i (x_i - x_{i-1}).
\end{equation*}
Die Wahl der Partition ist irrelevant, formell müssten wir aber beweisen, dass die Definition davon unabhängig
ist. In Königsberger oder Forster kann der Beweis nachgelesen werden.

\begin{lemma}
	\label{6.1.1}
	Für Treppenfunktionen $\varphi,\gamma:[a,b] \to \bbC$ und $\alpha, \beta \in \bbC$ gilt
	\begin{enumerate}[label=\alph*)]
		\item \[\int_{a}^{b} \alpha\varphi + \beta\gamma \dd x = \alpha \int_{a}^{b}\varphi \dd x
			+ \beta \int_{a}^{b} \gamma\dd x~, \]
		
		\item \[\left|\int_{a}^{b}\varphi\dd x \right| \le \int_{a}^{b} |\varphi|\dd x \le (b-a)
			\sup(|\varphi|)~, \]
		
		\item Falls $\varphi \le \gamma$, dann gilt \[\int_{a}^{b} \varphi \dd x
			\le \int_{a}^{b} \gamma \dd x~.\]
	\end{enumerate}
\end{lemma}

\begin{proof}
	Sei $a=x_0 < x_1 < \dots < x_n = b$ eine Partition von $[a,b]$ mit $\varphi(x) = c_k$, $\gamma(x) = x_k$
	auf $(x_{k-1},x_{k})$. Sei $\Delta x = x_k - x_{k-1}$.
	\begin{enumerate}[label=\alph*)]
		\item Es gilt
			\begin{align*}
				\int_{a}^{b} \alpha\varphi + \beta\gamma \dd x
					&\coloneqq \sum_{k=1}^{n} (\alpha c_k + \beta d_k) \Delta x\\
					&= \alpha \sum c_k \Delta x + \beta \sum d_k \Delta x \\
					&= \alpha \int_{a}^{b} \varphi \dd x + \beta \int_{a}^{b} \gamma \dd x.
			\end{align*}
		\item
			\begin{equation*}
				\left|\int_{a}^{b} \varphi \dd x \right| = \left|\sum_{k=1}^{n} c_k \Delta x\right|
				\le \sum_{k=0}^{n} |c_k| \Delta x = \int_{a}^{b} |\varphi|\dd x,
			\end{equation*}
			wobei $\sum |c_k|\Delta x \le \sup(\varphi(x)) \sum \Delta x$.
		\item Aus $\varphi \le \gamma$ folgt $c_k \le d_k$ für $k = 1 \dots n$. Also
			\begin{equation*}
				\int_{a}^{b} \varphi \dd x = \sum_{k=1}^{n} c_k \Delta x \le \sum_{k=1}^n d_k \Delta x = \int_{a}^{b}
					\gamma \dd x. \qedhere
			\end{equation*}
	\end{enumerate}
\end{proof}

\section{Regelfunktionen}
Eine Funktion $f: I \to \bbC$ heißt \emph{Regelfunktion}, wenn sie keine Unstetigkeiten zweiter Art hat,
d.\ h.\ wenn für jedes $x\in I$ die beiden einseitigen Grenzwerte $f(x-)$ und $f(x+)$ existieren. Die Menge der
Regelfunktionen auf $I$ bezeichnen wir mit $R(I,\bbC) = R(I)$.

\begin{example}
	Jede stetige Funktion und jede reellwertige monotone Funktion ist eine Regelfunktion (vgl.\ mit Theorem
	4.4.11 aus Analysis I).
\end{example}

\begin{satz}
	\label{6.2.1}
	Sind $f,g: I\to \bbC$ Regelfunktionen und $\alpha, \beta \in \bbC$, dann sind auch $\alpha f + \beta g$,
	$fg$, $\overline{f}$ und $|f|$ und (wenn $f$, $g$ reellwertig sind) auch $\max(f,g)$ und $\min(f,g)$
	Regelfunktionen.
\end{satz}

\begin{proof}
	Aus Satz 4.4.4 aus Analysis 1 folgt, dass $\alpha f + \beta g$, $fg$, $\overline{f}$ und $|f|$
	Regelfunktionen sind. Aus $\max(f,g) = \frac{1}{2} (f(x) + g(x)) + \frac{1}{2}|f(x) - g(x)|$ und analog
	$\min(f,g) = \frac{1}{2} (f(x) + g(x)) - \frac{1}{2}| f(x) - g(x)|$ und den vorherigen Teil
	folgt, dass auch $\max(f,g)$ und $\min(f,g)$ Regelfunktionen sind.
\end{proof}

\begin{theorem}
	\label{6.2.2}
	Eine Funktion $f$ auf einem kompakten Intervall $[a,b]$ ist genau dann eine Regelfunktion, wenn es zu jedem
	$\varepsilon > 0$ eine Treppenfunktion $\varphi$ auf $[a,b]$ gibt mit
	\begin{equation*}
		\sup_{x\in [a,b]} | f(x) - \varphi(x)| < \varepsilon.
	\end{equation*}
\end{theorem}
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle]
			\addplot[domain=-2:2] {0.5*x*x + x};
			\addplot[domain=-2:0,red] {-0.25};
			\addplot[domain=0:1,red] {0.5};
			\addplot[domain=1:1.5,red] {1.75};
			\addplot[domain=1.5:2,red] {3.25};
		\end{axis}
	\end{tikzpicture}
	\caption{Skizze zu Theorem \ref{6.2.2}}
\end{figure}

\begin{proof}
	Sei $f$ eine Regelfunktion, sei $\varepsilon >0$ und seien $a = x_0 < x_1 < \dots$ rekursiv definiert durch
	\begin{itemize}
		\item $x_{k+1} \coloneqq b$ falls $|f(x) - f(x_k+)| < \varepsilon$ für alle $x \in (x_k,b)$,
		\item $x_{k+1} \equalDef \inf \{x\in (x_k,b): |f(x)-f(x_k+)| \ge \varepsilon \}$ sonst.
	\end{itemize}
	Für $x \in (x_k, x_{k+1})$ gilt dann $|f(x) - f(x_k+)| < \varepsilon$ und wenn $x_{k+1} < b$, dann gilt:
	$x_k < x_{k+1} < x_{k+2}$ und es existiert ein $x'\in(x_k,x_{k+1})$, so dass $x''\in [x_{k+1},x_{k+2})$
	mit
	\begin{equation*}
		|f(x') - f(x_k+)| < \frac{\varepsilon}{2}
	\end{equation*} 
	und
	\begin{equation*}
		|f(x'')-f(x_k+)| \ge \varepsilon.
	\end{equation*}
	
	Behauptung: $\exists n\in \bbN$ mit $x_n = b$. Wir nehmen gegenteilig an, $x_k < b$ für alle $k \in \bbN$.
	Sei $x^* = \lim_{k\to N} x_k \le b$. Dann gibt es zu jedem $\delta>0$ ein $k\in \bbN$ mit
	$x_k \in (x^* -\delta,x^*)$. Nach obigen $\exists x',x''\in (x^*-\delta,x^*)$ mit
	\begin{align*}
		|f(x')-f(x'')| &= |f(x') - f(x_k+) +f(x_k+) - f(x'')|\\
		&\ge |f(x'')-f(x_k+)| - |f(x')-f(x_k+)| \ge \varepsilon - \frac{\varepsilon}{2}\\
		&= \frac{\varepsilon}{2}.
	\end{align*}
	Also kann $\lim\limits_{x \to x^*-} f(X)$ nicht existieren (Cauchy-Kriterium für Funktionen nach Satz
	4.4.6). Das ist im Widerspruch zur Annahme, dass $f$ eine Regelfunktion ist. Es gilt somit die Behauptung.
	
	Wir definieren $\varphi:[a,b] \to \bbC$ durch $\varphi(x) = f(x_k+)$ für $x\in (x_k,x_{k+1})$,
	$k=0,\dots,n$ und $\varphi(x_k) = f(x_k)$ für $k=0,\dots,n$. Dann gilt $|f(x)-\varphi(x)| \le \varepsilon$
	für alle $x\in [a,b]$, wobei $\varphi$ eine Treppenfunktion ist.
	
	Umkehrung: Zu jedem $\varepsilon >0$ existiere eine Treppenfunktion $\varphi$ mit\\
	$\sup_x |f(x)- \varphi(x)| < \frac{\varepsilon}{2}$. Es bleibt zu zeigen, dass $f$ eine Regelfunktion ist.
	Wir zeigen, dass $f(x+) = \lim_{h\to 0+} f(x+h)$ existiert für alle $x\in[a,b)$. Wir verwenden das
	Cauchy-Kriterium für Funktionen. Sei $\varepsilon > 0$ und sei $\varphi$ eine Treppenfunktion, mit
	$|f(x)-\varphi(x)|<\varepsilon/2$, dann existiert ein $\delta > 0$, so dass
	$\varphi$ in $(x,x+\delta)$ konstant ist. Also gilt für $x', x'' \in (x,x+\delta)$
	\begin{align*}
		|f(x')-f(x'')| &= |f(x') - \varphi(x') + \varphi(x') - f(x'')|\\
		&\le |f(x') -\varphi(x')| + |\varphi(x'') - f(x'')| < \frac{\varepsilon}{2} + \frac{\varepsilon}{2}\\
		&< \varepsilon.\qedhere
	\end{align*}
\end{proof}



