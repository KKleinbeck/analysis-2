% !TEX root = Ana2.tex

Beispiel: Sei $$f(r,\varphi) = \begin{pmatrix}
	r\cos\varphi \\ r\sin\varphi
\end{pmatrix};$$
$D = (0,\infty)\times \bbR \to \bbR^2$. Die partiellen Ableitungen sind
\begin{equation*}
	\partial_r f(r,\varphi) =
	\begin{pmatrix}
		\cos \varphi\\ \sin\varphi
	\end{pmatrix},
	\qquad
	\partial_\varphi f(r,\varphi) =
	\begin{pmatrix}
		-r\sin\varphi \\ r\cos\varphi
	\end{pmatrix}.
\end{equation*} 
Sie sind stetig, also ist $f$ differenzierbar mit Jacobi-Matrix
\begin{equation*}
	f'(r,\varphi) = (\partial_r f, \partial_\varphi f) =
	\begin{pmatrix}
		\cos \varphi & -r \sin\varphi \\ \sin\varphi & r\cos\varphi
	\end{pmatrix}.
\end{equation*}

\begin{figure}[!b]
	\centering
	\begin{tikzpicture}
		\draw[->] (-1,0) -- (5,0) node[anchor=north west] {$r$}
			(0,-1) -- (0,5) node[anchor=south east] {$\varphi$};
		
		\foreach \x in {1,2}
		{
			\foreach \y in {1,2}
			{
				\draw[gray,thin] (\x,0) -- (\x,4)
					(0,\y) -- (4,\y);
			}
		}
		\node[orange,anchor=north east] at (1,1) (p) {$p$};
		\node[blue,anchor=east] at (0,1.5) {$\Delta\varphi$};
		\node[blue,anchor=north] at (1.5,0) {$\Delta r$};
		
		\draw[->,blue]  (1,1) -- +(1,0);
		\draw[->,blue] (1,1) -- +(0,1);
		
		\begin{scope}[xshift=8cm,yshift=1cm]
			\draw[->] (-2,0) -- (3,0) node[anchor=north west] {$r$}
				(0,-2) -- (0,3) node[anchor=south east] {$\varphi$};
			\draw[thin] (0,0) circle (1);
			\draw[thin] (0,0) circle (1.5);
			\begin{scope}[rotate=30]
				\draw[dashed,thin] (0,0) -- (3,0);
				\draw[->,orange] (1,0) -- (2,0) node[anchor=north west] {$\partial_r f(p)$};
				\draw[->,orange] (1,0) -- (1,1) node[anchor=south west] {$\partial_\varphi f(p)$};
			\end{scope}
		\end{scope}
	\end{tikzpicture}
	\caption{Tangentiallinien der Funktion $f(r,\varphi)$ aus dem Beispiel.}
	\label{fig:tang bsp}
\end{figure}

\subsubsection{Ein Gegenbeispiel zu Theorem 2}
Stetigkeit der Funktion und Existenz der partiellen Ableitungen genügt nicht! Sei $f:\bbR^2 \to \bbR$ definiert
durch $f(0,0) = 0$ und
\begin{equation*}
	f(x,y) = \frac{xy^2}{x^2 + y^2},\qquad (x,y) \ne (0,0).
\end{equation*}
Dann ist $f$ stetig auf $\bbR^2$, insbesondere in $(0,0)$, denn $|f(x,y)| \le \frac{1}{2}|y| \to 0$ für
$(x,y) \to (0,0)$ und es existieren alle Richtungsableitungen: für $h= (h_1,h_2) \in \bbR^2 \setminus\{(0,0)\}$
ist
\begin{align}
	\notag
	\partial_h f(0,0) &= \frac{\dd}{\dd t}f(th_1, th_2) \big|_{t=0}\\
	\notag
	&= \frac{\dd}{\dd t}\left(\frac{t^3 h_1h_2^2}{t^2(h_1^2 + h_2^2)}\right) \Big|_{t=0}\\
	&= \frac{h_1h_2^2}{h_1^2 + h_2^2}.
	\label{10.4 eq:H1}
\end{align}
Insbesondere gilt $\partial_1 f(0,0) = 0 = \partial_2 f(0,0)$. Also ist $f'(0,0) = (0,0)$. Wäre~$f$ differenzierbar,
dann $\partial_h f(0,0) = \dd f(0,0) h = f'(0,0) h = 0$ für alle $h \in \bbR^2$, im Widerspruch zu~\eqref{10.4 eq:H1}.

\section{Ableitungsregeln}
\begin{satz}
	\label{10.5.1}
	Seien $f,g:D\subset \bbR^n \to \bbR^m$ differenzierbar im Punkt $x\in D$ und sei $A\in \bbR$. Dann sind
	$(f+g)$ und $(\lambda f)$ differenzierbar in $x$ und
	\begin{align*}
		\dd (f+g)(x) &= \dd f(x) + \dd g(x),\\
		\dd (\lambda f)(x) &= \lambda \dd f(x).
	\end{align*}
\end{satz}

\noindent
Der Beweis dafür ist eine einfache Übung.

\begin{satz}
	\label{10.5.2}
	Sei $f:U\subset \bbR^n \to \bbR^m$ differenzierbar in $x \in U$, und sei $g:V\subset \bbR^m \to \bbR^l$
	differenzierbar in $y = f(x) \in V$. Dann ist $g\circ f:U\subset \bbR^n \to \bbR^l$
	differenzierbar in $x$ und
	\begin{equation*}
		\dd (g\circ f) (x) = \dd g(f(x)) \circ \dd f(x).
	\end{equation*}
	Entsprechend gilt für die Jacobi-Matrix
	\begin{equation*}
		(g\circ f)'(x) = g'(f(x)) f'(x).
	\end{equation*}
\end{satz}

\begin{proof}
	Nach Voraussetzung gilt
	\begin{equation*}
		f(x+h) = f(x) + Ah + |h| R(x,h),
	\end{equation*}
	wobei $R(x,h)\to 0$ für $h \to 0$ und
	\begin{equation*}
		g(y+h) = g(y) + Bd + |d|S(y,d),
	\end{equation*}
	wobei $S(y,d) \to 0$ für $d \to 0$. Dabei ist $A \in L(\bbR^n,\bbR^m)$, $B\in L(\bbR^m,\bbR^l)$. Mit
	$d\coloneqq f(x+h) - f(x) = Ah + |h|R(x,h)$ folgt
	\begin{align*}
		g(f(x+h)) - g(f(x)) &= g(y+d) - g(y)\\
		&= Bd + |d|S(y,d)\\
		&= B(Ah + |h|R(x,h)) + S(y,d)|Ah + |h|R(x,h)|\\
		&= BAh + |h| BR(x,h) + |Ah + |h| R(x,h)| S(y,d). 
	\end{align*}
	Also
	\begin{align*}
		\frac{|g(f(x+h)) - g(f(x)) - BAh|}{|h|} &\le |BR(x,h)| + \left(\frac{|Ah|}{|h|} + |R(x,h)|\right)|S(y,d)|\\
		&\le ||B||\cdot |R(x,h)|\\ &\quad + \left(||A|| + |R(x,h)|\right) |S(y,f(x+h)-f(x))|\\
		&\to 0 \qquad (h\to 0),
	\end{align*}
	da $R(x,h) \to 0$ und $S(y,f(x+h)-f(x))\to 0$ für $h \to 0$.
\end{proof}

Bemerkung: Für die partiellen Ableitungen bedeutet die Kettenregel
\begin{equation*}
	\frac{\partial}{\partial x_k} (g\circ f)(x)
		= \sum_{i=1}^{m} (\partial_i g)(f(x)) \frac{\partial f_i}{\partial x_k}(x).
\end{equation*}
Mit der Notation $y(x) = f(x)$ wird das zu
\begin{equation*}
	\frac{\partial}{\partial x_k}g(y(x))
		= \sum_{i=1}^{n} \frac{\partial g}{\partial y_i}(y) \frac{\partial y_i}{\partial x_k}(x)
\end{equation*}

\section{Der Gradient der Funktion}
Die Ableitung einer differenzierbaren Funktion $f:D\subset \bbR^n \to \bbR$ ($m=1$) ist eine Linearform
$\dd f(x) \in (\bbR^n)^*$, welche durch den Vektor
\begin{equation*}
	\nabla f(x) \coloneqq f'(x)^T
\end{equation*}
dargestellt wird:
\begin{equation*}
	\dd f(x) = f'(x) h = \langle f'(x)^T, h \rangle = \langle \nabla f(x),h\rangle.
\end{equation*}
Das Symbol $\nabla$ heißt Nabla; man spricht \emph{Nabla $f$} oder \emph{Nabla von $f$}.
Der Vektor
\begin{equation*}
	\nabla f(x) \coloneqq
	\begin{pmatrix}
		\partial_1 f(x) \\ \vdots \\ \partial_n f(x)
	\end{pmatrix}
\end{equation*}
heißt \emph{Gradient von $f$} an der Stelle $x$.

Beispiel
\begin{itemize}
	\item $f(x) = \langle a, x\rangle + b$ mit $a\in \bbR^n$, $b\in \bbR$; dann ist $\nabla f = a$.
	\item $f(x) = \langle x,Ax\rangle$ mit $x\in \bbR^n$, $A = A^T \in M(n\times n, \bbR)$. Dann ist
		$\nabla f(x) = 2Ax$.
\end{itemize}

\subsubsection{Geometrische Interpretation von $\nabla f(x)$}
Der Gradient $\nabla f(x)$ zeigt in die Richtung des größten Anstiegs von $f$ an der Stelle $x$ und $|\nabla f(x)|$
ist die Steigung in diese Richtung. Der Gradient $\nabla f(x)$ steht senkrecht auf der Niveaufläche
$\{x\mid f(x) = f(a)\}$.
\begin{proof}
	Sei $h \in \bbR^n$ mit $|h| = 1$ und sei $\nabla f(x) \neq 0$. Dann ist
	\begin{equation*}
		\partial_h f(x) = \dd f(x) h = \langle \nabla f(x), h\rangle
			\le |\nabla f(x)|\cdot \underset{=1}{\underbrace{|h|}} = |\nabla f(x)|
	\end{equation*}
	mit Gleichheit nur für $h = \nabla f(x) / |\nabla f(x)|$.
	
	Sei $\gamma: (-\varepsilon,\varepsilon) \to \bbR^n$ eine differenzierbare Kurve mit $\gamma (0) = a$ und
	$f(\gamma(a)) = f(a)$ für alle $f$. Dann gilt
	\begin{equation*}
		0 = \frac{\dd}{\dd t} f(\gamma(t))\big|_{t= 0} = \dd f(\gamma(0)) \dot{\gamma}(0)
			= \langle \nabla f(\usb{a}{\gamma(0)}), \dot{\gamma}(0)\rangle.
	\end{equation*}
	Also
	\begin{equation*}
		\nabla f(a) \perp \dot{\gamma}(0). \qedhere
	\end{equation*}
\end{proof}

Das Beispiel $f = \sqrt{1 - x^2 - y^2}$ ist in Abbildung~\ref{fig:Kreiskurven} skizziert.

\begin{figure}[!t]
	\centering
	\tdplotsetmaincoords{0}{0}
	\begin{tikzpicture}[scale=3]
		\draw[->] (0,0,0) -- (0,0,1.2) node[anchor=north east] {$x$};
		\draw[->] (0,0,0) -- (1.2,0,0) node[anchor=west] {$y$};
		\draw[->] (0,0,0) -- (0,1.2,0);
		
		\draw[dashed,gray] (1,0,0) -- (1,1,0) -- (0,1,0) -- (0,1,1) -- (0,0,1) -- (1,0,1) -- cycle;
		
		\tdplotdrawarc[canvas is yz plane at x=0,blue]{(0,0,0)}{1}{0}{90}{}{};
		\tdplotdrawarc[canvas is xz plane at y=0,blue]{(0,0,0)}{1}{0}{90}{}{};
		\tdplotdrawarc[canvas is xy plane at z=0,blue]{(0,0,0)}{1}{0}{90}{}{};
		
		\pgfmathsetmacro{\cs}{cos(60)} % cosine sixty
		\tdplotdrawarc[canvas is xz plane at y=sin(60),orange]{(0,0,0)}{\cs}{0}{90}{}{};
		\pgfmathsetmacro{\ct}{cos(30)} % cosine thirty
		\tdplotdrawarc[canvas is xz plane at y=sin(30),orange]{(0,0,0)}{\ct}{0}{90}{}{};
		\node[orange,anchor=east] at (0,\cs,\ct) {$f(x,y) = \mathrm{const}$};
		
		\tdplotdrawarc[canvas is xz plane at y=0,green!50!black]{(0,0,0)}{0.75}{0}{90}{}{};
		\tdplotdrawarc[canvas is xz plane at y=0,green!50!black]{(0,0,0)}{0.5}{0}{90}{}{};
		
		\pgfmathsetmacro{\rad}{sqrt(9/32)} % Radius = 3/4 am äußeren Ring
		\draw[->,violet] (\rad,0,\rad) node[anchor=west] {$\nabla f$} -- (0.2,0,0.2);
		
		\begin{scope}[scale=0.3,xshift=6cm]
			\draw[->] (-1,0) -- (4,0) node[anchor=north west] {$x$};
			\draw[->] (0,-1) -- (0,4) node[anchor=south east] {$y$};
			
			\foreach \rad in {1.5,2.25,3}
			{
				\draw[green!50!black] (\rad,0) arc (0:90:\rad);
			}
			\node[anchor=west,green!50!black] at (3,1) {$f = \mathrm{const}$};
			\draw[violet,->] (1.6,1.6) -- (0.5,0.5) node[anchor=west,inner sep=7pt,] {$\nabla f$};
		\end{scope}
				
	\end{tikzpicture}
	\caption{Grafische Repräsentation des Gradienten und der Niveauflächen von $f(x,y) = \sqrt{1 - x^2 - y^2}$.
		Links: Die Höhe der blauen Kurve spiegelt die Funktionswerte von $f$ entlang der $(x,y)$ Werte wider.
			Orange Linien sind die Niveauflächen von $f$, ihre Projektion auf den "`Boden"' des Schaubilds ist
			in grün dargestellt. Sie bilden Kreise um den Punkt $(0,0)$ aus. Der Gradient zeigt senkrecht von den
			Niveauflächen zu dem Mittelpunkt (Koordinatenursprung) der von $f$ aufgespannten Kugel. Rechts:
			Aufsicht auf das Schaubild von $f$. Alle Aussagen gelten hier äquivalent.}
	\label{fig:Kreiskurven}
\end{figure}

\subsubsection{Potentialfelder}
Ein Vektorfeld $F:D\subset\bbR^n \to \bbR^n$ heißt \emph{konservativ}, wenn eine differenzierbare Funktion
$V:D\to \bbR$ existiert mit
\begin{equation*}
	F(x) \equiv - \nabla V(x).
\end{equation*}
In diesem Fall ist für jede Lösung $t\mapsto x(t)$ der Newtonschen Gleichung
\begin{equation*}
	m \ddot{x}(t) = F(x(t)) = -\nabla V(x(t))
\end{equation*}
die Energie
\begin{equation*}
	E = \frac{m}{2} \dot{x}^2(t) + V(x(t))
\end{equation*}
erhalten (zeitlich konstant).

\begin{proof}
\begin{align*}
	\frac{\dd}{\dd t} \left(\frac{m}{2} \dot{x}^2(t) +V(x(t)) \right) &=
		m\dot{x}(t) \ddot{x}(t) + \nabla V(x(t))\cdot \dot{x(t)} \\
	&= (m\ddot{x}(t) + \nabla V(x))\dot{x}(t) = 0. \qedhere
\end{align*}
\end{proof}

\section{Mittelwertsatz}
Eine Strecke von $a$ nach $b$ in $\bbR^n$ bezeichnen wir mit
\begin{equation*}
	[a,b] \coloneqq \{ a + t(b-a) \mid t \in [0,1]\}.
\end{equation*}

\begin{satz}
	\label{10.7.1}
	Mittelwertsatz: Sei $f:D\subset \bbR^n \to \bbR$ differenzierbar und $[a,b] \subset D$. Dann existiert
	$x\in [a,b]$ mit
	\begin{equation*}
		f(b) - f(a) = \langle \nabla f(x),  (b - a) \rangle .
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $g(t) \coloneqq f(a + t(b-a))$ für $t \in [0,1]$. Dann ist $g$ differenzierbar. Also existiert
	$\tau \in (0,1)$ mit
	\begin{equation*}
		f(b) - f(a) = g(1) - g(0) = g'(\tau) = \nabla f(a + \tau (b-a))\cdot (b-a)
			= \nabla f(x) (b-a). \qedhere
	\end{equation*}
\end{proof}













