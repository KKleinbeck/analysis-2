% !TEX root = Ana2.tex

\section{Vertauschungssätze}
\begin{theorem}
	\label{8.2.1}
	Ist $f_n:D\to \bbC$ eine Folge stetiger Funktionen mit $f_n \to f$ gleichmäßig, dann ist auch $f$ stetig.
\end{theorem}

Folglich gilt
\begin{equation*}
	\lim_{x\to x_0}\lim_{n \to \infty} f_n(x) = \lim_{x\to x_0} f(x) = f(x_0) = \lim_{n\to\infty}\lim_{x\to x_0}f_n(x).
\end{equation*}

\begin{proof}
	Sei $x \in D$ und $\varepsilon > 0$. Sei $n\in \bbN$, so groß, dass
	\begin{equation}
		\label{eq: 8.2.H1}
		\sup_{x}|f_n(x) - f(x)| < \frac{\varepsilon}{3}.
	\end{equation}
	Dieses $n$ halten wir nun fest. Da $f_n$ in $x_0 \in D$ stetig ist, existiert ein $\delta > 0 $, so dass
	\begin{equation}
		\label{eq: 8.2.H2}
		x\in B_\delta(x_0) \cap D \Rightarrow |f(x) - f_n(x_0)| <\frac{\varepsilon}{3}.
	\end{equation}
	Aus den Gleichungen \eqref{eq: 8.2.H1} und \eqref{eq: 8.2.H2} folgt
	\begin{IEEEeqnarray*}{+rCl+x*}
	x \in B_\delta(x_0) \cap D \Rightarrow |f(x) - f(x_0)| & \le & |f(x) - f_n(x)| + |f_n(x) - f_n(x_0)| \\
			&& \quad + |f_n(x_0) - f(x_0)| \\
			& < & 3\frac{\varepsilon}{3} = \varepsilon & \qedhere
	\end{IEEEeqnarray*}
\end{proof}

Bemerkung: Das Beispiel~1 ($f_n = x^n$) zeigt, dass punktweise Konvergenz in Theorem \ref{8.2.1} nicht genügt.

\begin{theorem}
	\label{8.2.2}
	Sei $I\subset \bbR$ ein Intervall und sei $f_n:I \to \bbC$ eine Folge differenzierbarer Funktionen. Falls
	$f_n \to f$ punktweise und $f_n' \to g$ \emph{gleichmäßig} konvergieren, dann ist $f$ differenzierbar und
	$f'=g$.
\end{theorem}

Bemerkung: Das Beispiel~3 zeigt, dass punktweise Konvergenz der Ableitung nicht genügt.

\begin{proof}
	Seien zunächst alle $f_n$ reellwertig. Sei $x_0 \in I$ und
	\begin{equation*}
		\Phi_n(x) =
		\begin{cases}
			\frac{f_n(x) - f_n(x_0)}{x-x_0}, & x\ne x_0.\\
			f_n'(x_0) & x = x_0
		\end{cases}
	\end{equation*}
	und
	\begin{equation*}
		\Phi(x) =
		\begin{cases}
			\frac{f(x) - f(x_0)}{x-x_0}, & x\ne x_0.\\
			g(x_0) & x = x_0
		\end{cases}.
	\end{equation*}
	Wir wollen zeigen, dass $f'(x_0) = g(x_0)$. Das ist äquivalent zur Stetigkeit von von $\Phi$ in $x_0$.
	$\Phi_n$ ist stetig, da $f_n$ differenzierbar ist. Nach Theorem~\ref{8.2.1} genügt es also zu zeigen, dass
	$\Phi_n\to\Phi$ gleichmäßig konvergiert. Wir benutzen das Cauchy-Kriterium für gleichmäßige Konvergenz,
	Satz~\ref{8.1.1}.
	
	Sei $f_{nm} = f_n - f_m$. Dann gilt
	\begin{equation*}
		\Phi_n(x)-\Phi_m(x) =
		\begin{cases}
			\frac{f_{nm}(x) - f_{nm}(x_0)}{x-x_0} & x\ne x_0\\
			f_{nm}'(x_0) & x= x_0
		\end{cases}.
	\end{equation*}
	Also gilt nach dem Mittelwertsatz
	\begin{equation}
		\label{eq: 8.2.H3}
		\Phi_n(x) - \Phi_m(x) = f_{nm}'(\xi_x)
	\end{equation}
	mit $\xi_x$ zwischen $x_0$ und $x$.
	Da $f_n'\to g$ gleichmäßig ist, ist $(f'_n)$ eine gleichmäßige Cauchy-Folge. D.h.\ zu $\varepsilon > 0$ gibt
	es ein $N\in \bbN$, so dass
	\begin{equation}
		\label{eq: 8.2.H4}
		|f_n'(x)-f_m'(x)| < \varepsilon
	\end{equation}
	für alle $x$ und $n,m>N$.
	
	Aus \eqref{eq: 8.2.H3} und \eqref{eq: 8.2.H4} folgt, dass
	\begin{equation*}
		|\Phi_n(x) - \Phi_m(x)| < \varepsilon
	\end{equation*}
	für alle $x\in I$ und $n,m\ge N$. Somit ist $\Phi_n$ gleichmäßig konvergent nach Satz~\ref{8.1.1}. Aus
	der Definition von $\Phi_n$ und Voraussetzung des Theorems gilt $\Phi_n \to \Phi$ punktweise. Also
	ist $\Phi$ der gleichmäßige Limes der Folge $\Phi_n$.
	
	
	Sind die Funktionen $F_n$ komplexwertig, dann können wir das bereits bewiesene auf $\rePart f_n$ und $\imPart f_n$
	anwenden und bekommen so den vollständigen Beweis für Theorem~\ref{8.2.2}.
\end{proof}

\begin{theorem}
	\label{8.2.3}
	Sei $f_n:[a,b]\to \bbC$ eine Folge von Regelfunktionen, welche gleichmäßig gegen $f:[a,b]\to\bbC$
	konvergieren. Dann ist $f$ eine Regelfunktion und
	\begin{equation*}
		\int_{a}^{b} f\dd x = \lim\limits_{n\to \infty} \int_{a}^{b} f_n \dd x.
	\end{equation*}	
\end{theorem}

\begin{proof}
	Sei $\varepsilon > 0$ und sei $n\in \bbN$ so groß, dass
	\begin{equation}
		\label{eq: 8.2.H5}
		\supnorm{f_n - f} < \frac{\varepsilon}{2}.
	\end{equation}
	Da $f_n$ eine Regelfunktion ist, existiert eine Treppenfunktion $\varphi:[a,b]\to\bbC$ mit
	\begin{equation}
		\label{eq: 8.2.H6}
		\supnorm{f_n - \varphi} < \frac{\varepsilon}{2}.
	\end{equation}
	Aus \eqref{eq: 8.2.H5} und \eqref{eq: 8.2.H6} folgt
	\begin{align*}
		\supnorm{f-\varphi} &= \supnorm{(f-f_n) + (f_n-\varphi)}\\
		&\le \supnorm{f-f_n} + \supnorm{f_n - \varphi}\\
		&< \varepsilon.
	\end{align*}
	Des beweist, dass $f$ eine Regelfunktion ist.
	
	Ferner ist
	\begin{align*}
		\left|\int_{a}^{b}f\dd x - \int_{a}^{b}f_n\dd x\right| &\le
			\int_{a}^{b} |f-f_n|\dd x\\
		&\le (b-a)\supnorm{f_n-f} \to 0.\qedhere
	\end{align*}
\end{proof}

Bemerkung: Für Theorem~\ref{8.2.3} genügt punktweise Konvergenz \emph{nicht}. Für
\begin{equation*}
	f_n =
	\begin{cases}
		\min(n^2x,2n-n^2x) & x\in [0,2/n]\\
		0 & \text{sonst}
	\end{cases}
\end{equation*}
gilt $f_n(x) \to 0$ punktweise für $n\to \infty$, aber es ist
\begin{equation*}
	\int_{0}^{n}f_n(x)\dd x = 1
\end{equation*}
und somit
\begin{equation*}
	\int_{0}^{n} f_n(x) \dd x \not\to \int_{0}^{n} 0\dd x = 0.
\end{equation*}

\subsection{Funktionenreihen}
Eine Reihe $\sum u_k(x)$ von Funktionen $u_k:D\to\bbC$ heißt punktweise (gleichmäßig) konvergent, wenn die Folge
der Partialsummen
\begin{equation*}
	f_n(x) = \sum_{k=1}^{n} u_k(x)
\end{equation*}
auf $D$ punktweise (gleichmäßig) konvergent ist.

\begin{satz}
	\label{8.2.4}
	Die Summe einer auf $[a,b]$ gleichmäßig konvergenten Reihe $\sum_k u_k(x)$ von stetigen Funktionen
	$u_k:[a,b]\to\bbC$ ist stetig und
	\begin{equation*}
		\int_{a}^{b} \sum_{k=1}^{\infty} u_k(x) \dd x = \sum_{k=1}^{\infty}\int_{a}^{b} u_k(x)\dd x.
	\end{equation*}
\end{satz}

Der Beweis folgt direkt aus Theorem~\ref{8.2.1} und \ref{8.2.3}.

\begin{satz}
	\label{8.2.5}
	Die Funktionenreihe $\sum_{k=0}^{\infty} u_k(x)$ sei punktweise konvergent und die Reihe der Ableitungen
	$\sum u_k'(x)$ sei gleichmäßig konvergent auf $[a,b]$. Dann ist $\sum u_k$ differenzierbar und
	\begin{equation*}
		\left(\sum_{k=1}^{\infty} u_k(x)\right)' = \left(\sum_{k=1}^{\infty} u_k'(x)\right).
	\end{equation*}
\end{satz}

\noindent
Das folgt aus Theorem~\ref{8.2.2}.

\begin{theorem}
	\label{8.2.6}
	Sei $u_k:D\to \bbC$ eine Folge von Funktionen und $M_k \ge \sup_{x\in D}|u_k(x)|$. Falls
	\begin{equation*}
		\sum_{k=1}^{\infty} M_k < \infty,
	\end{equation*}
	dann ist $\sum_{k=1}^{\infty} u_k(x)$ gleichmäßig und absolut konvergent.
\end{theorem}
Das wird Weierstraß'scher $M$-Test genannt.

\begin{proof}
	Für alle $x\in D$ gilt
	\begin{align*}
		\sum_{k=1}^{\infty} |u_k(x)| \le \sum_{k=1}^{\infty} M_k < \infty.
	\end{align*}
	Damit ist $\sum u_k(x)$ absolut konvergent. Nach Annahme ist $\sum_{k=1}^{n} M_k$ eine Cauchy-Folge, wenn
	also $\varepsilon > 0$, dann existiert $N\in \bbN$ mit
	\begin{equation*}
		n > m \ge N \Rightarrow \left|\sum_{k=m+1}^{n}M_k\right| = \left|\sum_{k=1}^{n}M_k - \sum_{k=1}^{m} M_k\right|
			<\varepsilon.
	\end{equation*}
	Also
	\begin{align*}
		n > m \ge N \Rightarrow \left|\sum_{k=1}^{n} u_k(x) - \sum_{k=1}^{m} u_k(x)\right|
			&= \left|\sum_{k=m+1}^{n} u_k(x)\right|\\
			&\le \sum_{k=m+1}^{n}|u_k(x)|\\
			& \le \sum_{k=m+1}^{n} M_k < \varepsilon.
	\end{align*}
	Also ist $\sum u_k(x)$ nach Satz~\ref{8.1.1} gleichmäßig konvergent.
\end{proof}

Beispiel: Die Reihe
\begin{equation*}
	\sum_{n=1}^{\infty}\frac{1}{n^2}\cos(nx)
\end{equation*}
ist auf ganz $\bbR$ gleichmäßig konvergent, denn $|\cos(nx)/n^2| < 1/n^2$ und
\begin{equation*}
	\sum_{n=1}^{\infty} \frac{1}{n^2} < \infty.
\end{equation*}
Nach Satz~\eqref{8.2.4} wird durch die Funktionenreihe eine stetige Funktion definiert. Die Summe der Ableitung
ist
\begin{equation*}
	\left(\sum_{n=1}^{\infty} \frac{1}{n^2} \cos(nx)\right)' = - \sum_{n=1}^{\infty} \frac{1}{n} \sin(nx).
\end{equation*}
Wir werden sehen, dass diese Reihe \emph{nicht} gleichmäßig konvergent ist, damit ist Satz~\ref{8.2.5} nicht
anwendbar. Tatsächlich ist die Reihe $\sum 1/n^2 \cos(nx)$ nicht differenzierbar.















