% !TEX root = Ana2.tex

\begin{proof}
	Nach Satz~\ref{6.7.2} ist die Reihe $\sum f(k)$ genau dann konvergent, wenn
	\begin{equation*}
		\lim\limits_{n\to\infty} \int_{1}^{n} f(x) \dd x
	\end{equation*}
	in $\bbR$ existiert. Da $R\mapsto \int_{1}^{R} f(x) \dd x$ monoton wachsend ist, existiert
	$\int_{1}^{\infty} f(x) \dd x$ immer als uneigentlicher Grenzwert $\le \infty$. Wegen
	\begin{align*}
		\int_{1}^{R} f(x) \dd x &\le \lim\limits_{n\to\infty} \int_{1}^{n} f(x) \dd x\\
		&\le \int_{1}^{\infty} f(x) \dd x
	\end{align*}
	gilt
	\begin{equation*}
		\lim\limits_{n\to \infty}\int_{1}^{n} f(x)\dd x = \int_{1}^{\infty} f(x) \dd x. \qedhere
	\end{equation*}
\end{proof}




\chapter[Elementar integrierbare Differentialgleichungen]{Elementar integrierbare Differentialgleichungen}
\section{Motivation}
Sei $v(t) \in \bbR$ die Geschwindigkeit einer fallenden Kugel mit Luftwiderstand. Die Bewegungsgleichung der
Kugel lautet dann
\begin{equation*}
	\dot{v} = g - \lambda v,
\end{equation*}
wobei $\dot{v} = \frac{\dd}{\dd t} v(t)$ als Kurznotation eingeführt wurde. Die Bewegungsgleichung stellt
einen Zusammenhang zwischen der Geschwindigkeit und ihrer Ableitung her. Wir wollen eine Lösung für die
Gleichung finden, vorausgesetzt, sie existiert. Ist die Lösung dann eindeutig?

Das Problem --- die obige Gleichung --- nennen wir eine \emph{gewöhnliche Differentialgleichung erster
Ordnung.} Die Lösung ist eindeutig, wenn die Anfangsgeschwindigkeit $v(0)$ bestimmt ist. Für $v(0) = 0$ ist
\begin{equation*}
	v(t) = \frac{g}{\lambda} \left(e^{-\lambda t} - 1 \right),
\end{equation*}
was sich durch Einsetzen zeigen lässt.

Wir werden uns jetzt systematisch eine Methodik erschließen um solche Differentialgleichungen zu lösen.

\section{Grundbegriffe und Beispiele}
Sei $D\subset \bbR^2$ und $f:D\to \bbR$. Dann nennt man
\begin{equation}
	y' = f(x,y)
	\label{eq:DGL}
\end{equation}
eine gewöhnliche Differentialgleichung erster Ordnung. $y'$ bezeichnet wie gehabt $\frac{\dd y}{\dd x}$,
also ist die Definition ausführlich geschrieben
\begin{equation*}
	\frac{\dd y}{\dd x} = f(x,y(x)).
\end{equation*}

Als Lösung von \eqref{eq:DGL} akzeptiert man jede Funktion $\varphi: I \to \bbR$, $I\subset \bbR$ ein
Intervall, mit
\begin{enumerate}
	\item $\Gamma(\varphi) = \left\{(x,\varphi(x) ) \mid x \in I \right\}\subset D$.
	
	\item $\varphi$ ist differenzierbar und $\varphi'(x) = f(x,\varphi(x))$ für alle $x \in I$.
\end{enumerate}

\noindent
Beispiele sind dafür
\begin{enumerate}
	\item $y' = a y$ mit $a\in\bbR$ hat die Lösung $y(t) = ce^{at}$, $c\in\bbR$.
	
	\item $y' = \frac{ny}{x}$ hat die Lösung $y = cx^n$, $c\in\bbR$.
	
	\item $y' = xy$ hat die Lösung $y = c e^{x^2/2}$.
	
	\item $y' = y^2$ wird gelöst von $y(x) = \frac{1}{c-x}$ mit $c\in\bbR$ und $x<c$ oder $x>c$.
	
	\item $y'(x) = g(x)$, $g:I\to \bbR$ stetig, hat die Stammfunktion von $g$ als Lösung.
\end{enumerate}

\subsection{Richtungsfelder}
Eine differenzierbare Funktion $\varphi: I \to \bbR$ mit $\Gamma(\varphi) \subset D$ ist genau dann Lösung
von \eqref{eq:DGL}, wenn die Tangente in jedem Punkt $(x,y) \in \Gamma$ vom Graphen die Steigung $f(x,y)$ hat.
Man zeichne in vielen Punkten $(x,y) \in D$ ein kleines Geradenstück mit Steigung $f(x,y)$ um die Lösungen
zu erraten.

Wir machen ein einfaches Beispiel
\begin{equation*}
	y' = - \frac{x}{y}
\end{equation*}
wie in Bild~\ref{7 fig:Richtungsfeld} skizziert.
\begin{figure}[!b]
	\centering
	\begin{tikzpicture}
		\draw[->] (-2,0) -- (2.5,0);
		\draw[->] (0,-2) -- (0,2.5);
		
		\draw (2,2) -- (-2,-2);
		\foreach \p in {-2,-1.5,...,2.1}
		{
			\draw[blue] (\p-0.1,\p+0.1) -- (\p+0.1,\p-0.1);
		}
		
		\draw (2,-2) -- (-2,2);
		\foreach \p in {-2,-1.5,...,2.1}
		{
			\draw[blue] (\p-0.1,-\p-0.1) -- (\p+0.1,-\p+0.1);
		}
		
		\foreach \p in {-2,-1.5,...,2.1}
		{
			\draw[blue] (\p,0.1) -- (\p,-0.1);
			\draw[blue] (-0.1,\p) -- (0.1,\p);
		}
		
		\draw[red] (0,0) circle(1);
	\end{tikzpicture}
	\caption{Richtungsfeld und Lösung der Differentialgleichung}
	\label{7 fig:Richtungsfeld}
\end{figure}
Die Tangenten erinnern an Kreise und tatsächlich wird die Differentialgleichung durch den Ansatz
\begin{equation*}
	\varphi(x) = \pm \sqrt{c - x^2}
\end{equation*}
gelöst.

Ein \emph{Anfangswertproblem} zur Differentialgleichung $G$, ist ein System von Gleichungen
\begin{equation}
	y' = f(x,y) \qquad y(x_0) = y_0,
	\label{eq:AWP}
\end{equation}
wobei $f$ und $x_0,y_0 \in D$ gegeben sind. Die Gleichung $y(x_0) = y_0$ heißt \emph{Anfangsbedingung}.

Eine implizite Lösung von \eqref{eq:AWP} ist eine Gleichung
\begin{equation*}
	U(x,y) = 0
\end{equation*}
mit einer bekannten Funktion $U$ , welche von den Lösungen erfüllt wird

Beispielhaft ist
\begin{equation*}
	y^2 + x^2 - c^2 = 0
\end{equation*}
eine implizite Form des obigen Beispiels
\begin{equation*}
	y' = - \frac{x}{y}.
\end{equation*}

\section{Lineare Differentialgleichungen}
Eine Differentialgleichung heißt \emph{linear}, wenn
\begin{equation*}
	f(x,y) = a(x) y + b(x).
\end{equation*}
Wir nehmen im folgenden immer an $a$ und $b$ seien stetig und $I\subset \bbR$.

Zuerst betrachten wir den \emph{homogenen} Fall
\begin{equation}
	y' = a(x) y
	\label{eq:erst Ord hom}
\end{equation}
zu gegebenem Anfangswert $y(x_0) - y_0$.

\begin{satz}
	\label{7.3.1}
	Sei $a:I\to \bbR$ stetig und $A$ eine Stammfunktion von $a$. Dann ist die Menge der Lösungen von
	$y' = a(x)y$ gegeben durch
	\begin{equation}
		\varphi(x) = c e^{A(x)},
		\label{eq:erst Ord hom Lsg}
	\end{equation}
	wobei $c$ beliebig aus $\bbR$ ist. Dies heißt \emph{allgemeine Lösung} von $y' = a(x) y$.
	
	Das Anfangswertproblem wird gelöst durch
	\begin{equation*}
		\varphi(x) = y_0 \exp\left(\int_{x_0}^{x} a(t) \dd t \right).
	\end{equation*}
\end{satz}

\begin{proof}
	\eqref{eq:erst Ord hom Lsg} ist eine Lösung von \eqref{eq:erst Ord hom}, denn
	\begin{align*}
		\frac{\dd}{\dd x} c e^{A(x)} = ca(x)e^{A(x)}.
	\end{align*}
	Sei $\varphi:I\to \bbR$ eine Lösung von \eqref{eq:erst Ord hom}. Dann gilt 
	\begin{equation*}
		\frac{\dd }{\dd x}e^{-A(x)} \varphi(x) = e^{-A(x)} (\varphi' - a(x)\varphi) = 0.
 	\end{equation*}
 	Da $I$ ein Intervall ist existiert demnach ein $c\in\bbR$ mit $e^{-A}\varphi = c$. Daraus folgt
 	$\varphi = c e^{A(x)}$.
 	
 	Durch die Bedingung $y(x_0) = y_0$ lässt sich die Konstante $c$ bestimmen.
\end{proof}

Um die inhomogene Gleichung
\begin{equation}
	y' = a(x)y + b(x)
	\label{eq:erst Ord inh}
\end{equation}
zu lösen, schreiben wir die hypothetische Lösung $\varphi$ in der Form
\begin{equation*}
	\varphi(x) = u(x) \varphi_h(x),
\end{equation*}
wobei $\varphi_h \ne 0$ eine Lösung von $y' = a(x) y$ ist, z.B.
\begin{equation*}
	\varphi_h(x) = \exp \int_{x_0}^{x} a(t) \dd t,
\end{equation*}
$x_0\in I$ beliebig. Da $\varphi_h \ne 0$ für alle $x \in I$ ist $u = \frac{\varphi}{\varphi_h}$
differenzierbar. Einsetzen von $\varphi = u \varphi_h$ in Gleichung~\eqref{eq:erst Ord inh}
ergibt
\begin{equation*}
	u'\varphi_h + u \varphi_h' = a(u\varphi_h) + b.
\end{equation*}
Wir können die Terme die links und rechts vorkommen streichen und erhalten
\begin{equation*}
	u' = \frac{b}{\varphi_h}
\end{equation*}
und damit
\begin{equation*}
	u = \int \frac{b}{\varphi_h}\dd x.
\end{equation*}

\begin{satz}
	\label{7.3.2}
	Seien $a,b: I \to \bbR$ stetig und sei $\varphi_h: I \to \bbR$ eine Lösung der homogenen Gleichung
	$y' = a(x)y$. Dann ist
	\begin{equation*}
		\varphi(x) = \varphi_h(x) \left(c + \int_{x_0}^{x}\frac{b(t)}{\varphi_h(t)} \dd t \right)
	\end{equation*}
	die allgemeine Lösung von Gleichung~\eqref{eq:erst Ord inh}. Mit der Wahl $c = y_0$ bekommt man die
	eindeutige Lösung von~\eqref{eq:erst Ord inh} mit dem Anfangswert $y(x_0) = y_0$.
\end{satz}

Bemerkung: Der Ansatz $\varphi = u \varphi_h$ heißt \emph{variation der Konstanten}, denn die Konstante $c$
in der allgemeinen Lösung $c\varphi_h$ der homogenen Gleichung $y' = ay$ wurde durch die Funktion $u$ ersetzt.

Als Beispiel betrachten wir
\begin{equation*}
	y' = \frac{y}{x} + \sin(x)
\end{equation*}
auf $(0,\infty)$. Die homogene Gleichung $y' = \frac{y}{x}$ hat die Lösung $x$. Also ist nach Satz~\ref{7.3.2}
die Lösung
\begin{equation*}
	\varphi(x) = x\left(c + \int_{1}^{x} \frac{\sin t}{t} \dd t\right)
\end{equation*}
die allgemeine Lösung der inhomogenen Gleichung.

Eine weitere Bemerkung. Ist $\varphi_p(x)$ eine partikuläre Lösung der inhomogenen Gleichung $y' =ay + b$ und
$\varphi_h$ eine Lösung der homogenen Gleichung, dann ist $\varphi_p + \beta\varphi_h$ die allgemeine
Lösung der inhomogenen Gleichung, mit $\beta\in\bbR$. Dies wird \emph{Superpositionsprinzip} genannt und
gilt für Differentialgleichungen \emph{erster} Ordnung.
