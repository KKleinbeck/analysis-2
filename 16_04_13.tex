% !TEX root = Ana2.tex

Das bedeutet \emph{nicht}, dass wir $F$ durch elementare Funktionen ausdrücken können.

Beispiele dafür sind
\begin{itemize}
	\item die Stammfunktion der Gauss'schen Glockenkurve, die Gauss'sche Fehlerfunktion
		\begin{equation*}
			\operatorname{Erf}(x) \coloneqq \frac{2}{\sqrt{\pi}}\int_{0}^{x} \exp\left(-t^2\right) \dd t,
		\end{equation*}
	
	\item die Fresnel'schen Integrale
		\begin{equation*}
			\int_{0}^{x} \cos\left(t^2\right) \dd t \qquad \int_{0}^{x} \sin\left(t^2\right)\dd t,
		\end{equation*}
	
	\item Besselfunktionen.
\end{itemize}

\begin{satz}
	\label{6.4.2}
	Ist $f: I \to \bbC$ stetig und $F:I\to \bbC$ eine Stammfunktion von $f$, dann gilt für alle $a,b\in I$
	\begin{equation*}
		\int_{a}^{b} f(x) \dd x = F(b) - F(a).
	\end{equation*}
\end{satz}

Für diesen Satz werden wir zwei Beweise zeigen. Der erste verwendet Theorem~\ref{6.4.1} und lautet
\begin{proof}
	Die Funktion $G: I \to \bbC$, definiert durch
	\begin{equation*}
		G(x) = \int_{a}^{x} f(t) \dd t,
	\end{equation*}
	eine nach Theorem~\ref{6.4.1} eine Stammfunktion von $f$. Da $F$ auch eine Stammfunktion ($I$ ein
	Intervall) ist, folgt $G = F+ c$, mit $c\in\bbC$. Also ist
	\begin{equation*}
		\int_{a}^{b} f \dd t = G(b) = G(b) - \underset{0}{\underbrace{G(a)}} = F(b) - F(a). \qedhere
	\end{equation*}
\end{proof}

Der zweite Beweis verwendet Riemannsummen und Satz~\ref{6.3.6}.
\begin{proof}
	Sei $n \in \bbN$, $\Delta x = \frac{b-a}{n}$ und $X_k = a + \frac{k}{n} \Delta x$, $k=1,\dots, n$.
	Dann folgt aus dem Mittelwertsatz der Differentialrechnung und aus Satz~\ref{6.3.6}, dass
	\begin{align*}
		F(b) - F(a) &= \sum_{k=1}^{n}\left(F(x_k) - F(x_{k-1}) \right) \\
		&= \sum_{k=1}^{n} F'(t_k)\Delta x \qquad \text{mit }t_k \in (x_{k-1},x_k)\\
		&= \sum_{k=1}^{n} f(t_k) \Delta x \to \int_{a}^{b} f(t) \dd t\quad (n\to\infty).
	\end{align*}
	Also $F(b) - F(a) = \int_{a}^{b} f(t) \dd t$.
\end{proof}

Nach Theorem~\ref{6.4.2} lässt sich das Problem der \emph{Quadratur} (Berechnung von $\int_{a}^{b} f \dd x$)
durch \emph{Integration} (= Bestimmung der Stammfunktion) lösen.

Betrachten wir nochmal das Beispiel $f(x) = x^2$. $f(x)$ hat die Stammfunktion $F(x) = \frac{1}{3} x^3$. Also
ist
\begin{equation*}
	\int_{0}^{1} x^2 \dd x = F(1) - F(0) = \frac{1}{3},
\end{equation*}
was offensichtlich gegenüber der Rechnung im vorherigen Abschnitt eine enorme Vereinfachung ist.

Notation: Statt $F(b) - F(a)$ schreibt man oft auch
\begin{equation*}
	F\big|_{a}^{b},\ F(x)\big|_{x=a}^{x=b},\text{ oder } \big[F(x)\big]_{a}^{b}.
\end{equation*}

Die Menge aller Stammfunktionen $\{F+c\mid c\in\bbC\}$ von $f$ wird mit dem Symbol
\begin{equation*}
	\int f \dd x
\end{equation*}
bezeichnet und heißt unbestimmtes Integral von $f$. Statt $\int f \dd x = \{F+c\mid c\in\bbC\}$ schreibt
man
\begin{equation*}
	\int f \dd x = F + c,
\end{equation*}
wobei $F' = f$ und $c$ eine beliebige Konstante ist (Integrationskonstante). Somit wird Theorem~\ref{6.4.1}
zu
\begin{equation*}
	\int_{a}^{b} f \dd x = \int f \dd x \big|_{a}^{b},
\end{equation*}
was die obige Notation erklärt.

Bemerkung: Bei der Berechnung eines bestimmten Integrals darf man die Integrationskonstante weglassen. Die
Menge $\{F +  c\mid c\in\bbC\}$ lässt sich als Äquivalenzklasse $[F]$ von Funktionen, bezüglich der
Äquivalenzrelation $F\sim G$, falls $F - G = \text{konst}$, auffassen. Sei $\nicefrac{C^1(I)}{\sim}$ die Menge
der $C^1$ Funktionen bezüglich $\sim$, dann ist die Abbildung
\begin{align*}
	\frac{\dd}{\dd x}: \nicefrac{C^1(I)}{\sim} &\to C(I)\\
	[F] &\mapsto F'
\end{align*}
nach Theorem~\ref{6.4.1} \emph{bijektiv}. Die Formulierung über die Quotientenmenge wird benötigt, damit die
Abbildung injektiv wird. Die Umkehrung ist der Ableitung ist die unbestimmte Integration
\begin{align*}
	C(I) &\to \nicefrac{C^1(I)}{\sim} \\
	f &\mapsto \int f \dd x.
\end{align*}
Es gilt also
\begin{align*}
	\int f' \dd x &= [f] = f + c\\
	\frac{\dd}{\dd x} \int f \dd x &= f.
\end{align*}

Man definiert
\begin{align*}
	[F] + [G] &\coloneqq [F+G]\\
	\lambda[F] &\coloneqq [\lambda F]
\end{align*}
und bekommt, dass für alle stetigen $f,g$:
\begin{align*}
	\tag{a}
	\int f \dd x + \int g \dd x &= \int f + g \dd x\\
	\tag{b}
	\lambda \int f \dd x &= \int \lambda f \dd x.
\end{align*}

\begin{proof}
	\begin{align*}
		\frac{\dd}{\dd x} \int f + g \dd x &= f + g\\
		&= \frac{\dd}{\dd x} \int f \dd x + \frac{\dd}{\dd x} \int g \dd x\\
		&= \frac{\dd}{\dd x}\left(\int f\dd x + \int g\dd x \right).
	\end{align*}
	Daraus folgt (a).
	
	\begin{align*}
		\frac{\dd}{\dd x} \int \lambda f \dd x &= \lambda f\\
		&= \lambda \frac{\dd}{\dd x} \int f \dd x \\
		&= \frac{\dd}{\dd x}\left(\lambda\int f \dd x \right),
	\end{align*}
	woraus (b) folgt.
\end{proof}

Wie die Integration ist auch die \emph{Evaluation} $F\mapsto F\big|_{a}^{b}$ linear
\begin{align*}
	(F+G)\big|_{a}^{b} &= F\big|_{a}^{b} + G\big|_{a}^{b} \\
	(\lambda F)\big|_{a}^{b} &= \lambda \left(F\big|_{a}^{b}\right).
\end{align*}

Es folgt eine Liste einiger wichtiger Stammfunktionen
\begin{align*}
	\int x^\alpha \dd x &= \frac{1}{\alpha + 1} x ^{\alpha + 1} + C
	& \int \cos(x) \dd x &= -\sin(x) + C \\
	\int \sin(x) \dd x &= \cos(x) + C & \int e^x \dd x &= e^x + C.
\end{align*}

\section{Integrationstechniken}
\subsection{Partielle Integration}
\begin{theorem}
	\label{6.5.1}
	Sind $f,g\in C^1(I), I\subset \bbR$ ein Intervall, dann gilt
	\begin{equation*}
		\int fg' \dd x = fg - \int f'g\dd x
	\end{equation*}
	und für $a, b \in I$
	\begin{equation*}
		\int_{a}^{b} fg' \dd x = fg\big|_{a}^{b} - \int_{a}^{b}f' g\dd x.
	\end{equation*}
\end{theorem}

\begin{proof}
	Nach Produktregel ist
	\begin{equation*}
		(fg)' = f'g + fg'.
	\end{equation*}
	Also ist
	\begin{align*}
		fg &= \int f'g + fg' \dd x \\
		&= \int f'g \dd x + \int fg'\dd x,
	\end{align*}
	und es folgt Theorem~\ref{6.5.1} durch Umstellen.
	
	Die zweite Aussage folgt aus der ersten durch Evaluation
	\begin{align*}
		\int_{a}^{b} fg' \dd x &= \int fg' \dd x \big|_{a}^{b}\\
		&= fg\big|_{a}^{b} - \int f'g \dd x\big|_{a}^{b} \\
		&= fg\big|_{a}^{b} - \int_{a}^{b} f'g \dd x. \qedhere
	\end{align*}
\end{proof}

Beispiele:
\begin{enumerate}
	\item Für jedes $\lambda \in \bbC\setminus\{0\}$ gilt
		\begin{align*}
			\int x e^{\lambda x} \dd x&= \frac{x}{\lambda} e^{\lambda x} - \frac{1}{\lambda}\int e^{\lambda x} \dd x\\
			&= \frac{x}{\lambda}e^{\lambda x} - \frac{1}{\lambda^2} e^{\lambda x} + c. 
		\end{align*}
	
	\item Die Stammfunktion des Logarithmus lässt sich bestimmen durch partielle Integration
		\begin{align*}
			\int \log(x) \dd x &= \int 1\cdot \log(x) \dd x\\
			&= x \log(x) - \int x\frac{1}{x} \dd x\\
			&= x \log(x) - x + c.
		\end{align*}

	\item Sei $n \in \bbN, n \ge 2$. Dann ist
		\begin{align*}
			\int (\cos x)^n \dd x &= \int (\cos x)^{n-1}\cos x \dd x\\
			&= (\cos x)^{n-1} \sin x + (n-1) \int (\cos x)^{n-2} (\sin x)^2 \dd x\\
			&= (\cos x)^{n-1} \sin x - (n-1) \int (\cos x)^{n} \dd x \\
			& \qquad + (n-1)\int (\cos x)^{n-1}\dd x.
		\end{align*}
		Also folgt durch Umformen
		\begin{align*}
			n\int (\cos x)^n \dd x &= (\cos x)^{n-1} \sin x + (n-1)\int (\cos x)^{n-2} \\
			\Rightarrow \int (\cos x)^n \dd x &= \sin x\frac{(\cos x)^{n-1}}{n}
				+ \frac{n-1}{n}\int (\cos x)^{n-2} \dd x.
		\end{align*}
		Das letzte auftretende Integral lässt sich durch rekursive Anwendung dieser Formel lösen.
		Für $a,b\in \frac{\pi}{2}\bbZ$ gilt
		\begin{align*}
			\int_{a}^{b} (\cos x)^n \dd x &= \frac{n - 1}{n} \int_{a}^{b} (\cos x)^{n-2} \dd x\\
			\int_{a}^{b} (\sin x)^n \dd x &= \frac{n - 1}{n} \int_{a}^{b} (\sin x)^{n-2} \dd x\\
		\end{align*}
		
		Daraus ergibt sich die Anwendung
		\begin{equation*}
			\frac{\pi}{2} = \lim\limits_{n\to\infty}\prod_{k=1}^{n} \frac{4k^2}{4k^2 - 1},
		\end{equation*}
		die Wallis'sche Produktdarstellung von $\pi$.
\end{enumerate}

\begin{theorem}
	\label{6.5.2}
	Taylorsche Formel mit Integralrestglied:
	Sei $f:I\to \bbC$ eine $n$-mal stetig differenzierbare Funktion und $a \in I$. Dann gilt für alle
	$x \in I$
	\begin{align*}
		f(x) = \sum_{k=0}^{n-1} \frac{f^{(k)}(a)}{k!}(x-a)^k
			+ \frac{1}{(n-1)!}\int_{a}^{x} (x-t)^{n-1} f^{(n)}(t) \dd t.
	\end{align*}
\end{theorem}

\begin{proof}
	Induktion in $n$.
	
	Fall $n=1$:
	\begin{equation*}
		f(x) = f(a) + \int_{a}^{x} f'(t) \dd t.
	\end{equation*}
	
	Wir führen erst den Induktionsschritt von $n=1$ zu $n=2$, indem wir das auftretende Integral partiell
	integrieren
	\begin{align*}
		\int_{a}^{x}f'(t) \dd t &= \int_{a}^{x} 1\cdot f'(t) \dd t \\
			&= (t-x)f'(t)\big|_{a}^{x} - \int_{a}^{x} (t-x) f''(t)\dd t\\
			&= (x-a)f'(a) - \int_{a}^{x} (t-x) f''(t)\dd t.
	\end{align*}
	Jetzt, nachdem wir verstanden haben, was im Integrationsschritt geschieht, können wir die Induktion
	schließlich wie gewohnt beenden.
\end{proof}







