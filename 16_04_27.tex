% !TEX root = Ana2.tex

Mit dem jetzigen Wissen wenden wir uns wieder dem Beispiel der fallenden Kugel, beschrieben durch die
Differentialgleichung
\begin{equation*}
	\dot{v} = g - \lambda v,
\end{equation*}
zu. Die partikuläre Lösung ist in diesem Fall besonders einfach: $v_h = \frac{g}{\lambda}$. Die homogene
Differentialgleichung ergibt sich nach Integration zu
\begin{equation*}
	v_h = c e^{-\lambda t}.
\end{equation*}

\subsection{Die Bernoullische Differentialgleichung}
Die Klasse der Bernoullischen Differentialgleichungen sind gegeben durch
\begin{equation*}
	y' = a(x) y + b(x) y^\alpha, \qquad \alpha\in\bbR\setminus\{0,1\}.
\end{equation*}
Sie sind nicht linear, aber können durch Substitution $u = y^{1-\alpha}$ zur linearen Differentialgleichung
\begin{equation*}
	u' = (1-\alpha)\left(a(x) u + b(x)\right)
\end{equation*}
umgeformt werden.

\section{Separierbare Differentialgleichungen}
Eine Differentialgleichung der Form
\begin{equation}
	y' = f(x) g(y)
	\label{7 eq:sep}
\end{equation}
heißt \emph{separierbar}. Die Funktionen $f$, $g$ seien auf dem Intervall $I, J \subset \bbR$ definiert und
stetig. Jede Nullstelle $y_0 \in J$ von $g$ liefern eine konstante Lösung $\varphi(x) = y_0$.
Diese konstanten Lösungen zerlegen $I\times J$ in horizontale Streifen.

Ein Beispiel mit $f(x) = 1$ und konstanten Lösungen $y_0 \in \{0, 1\}$ ist
\begin{equation}
	y' = y (1- y).
	\label{7 eq:Bsp}
\end{equation}

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,
					xmin=-1,xmax=2,
					ymin=-0.5,
					ymax=1.5]
			\addplot[blue,domain=-1:1.5,] {0} node[anchor=south west] {$\varphi_0(x) = 0$};
			
			\addplot[blue,domain=-1:1.5,] {1} node[anchor=south west] {$\varphi_1(x) = 1$};
		\end{axis}
	\end{tikzpicture}
	\caption{Die Differentialgleichung $y' = y(1-y)$ zerlegt die gezeichneten Intervalle durch zwei Geraden
		mit konstantem Wert.}
\end{figure}
Die anderen Lösungen von \eqref{7 eq:Bsp} findet man durch \emph{Separation der Variablen}: Sei $g \ne 0$ in
$J$. In 
\begin{equation*}
	\frac{\dd y}{\dd x} f(x) g(y)
\end{equation*}
bringen wir $x$ formal nach rechts und $y$ nach links
\begin{equation*}
	\frac{\dd y}{g(y)} = f(x)\dd x
\end{equation*}
und "`integrieren"' darüber
\begin{equation*}
	\int \frac{\dd y}{g(y)} = \int f(x) \dd x.
\end{equation*}
Diese Gleichung bestimmt die Lösung \emph{implizit}, D.h., ist $G$ eine Stammfunktion von $\frac{1}{g}$ und
$F$ eine Stammfunktion von $f$, dann sind alle Lösungen bestimmt durch $G(y) = F(x) + c$, $c\in \bbR$.
Wegen $G' = \frac{1}{g} \ne 0$ auf $J$, ist $G: J \to G(J)$ bijektiv. Wenn also $I\subset \bbR$ ein Intervall
ist mit $F(I) + c \subset G(J)$, dann ist
\begin{equation*}
	\varphi(x) = G^{-1}(F(x) + c) \qquad x \in I
\end{equation*}
Lösung von \eqref{7 eq:Bsp}.

\begin{proof}
	Wir leiten $\varphi(x)$ ab und zeigen, dass es die Differentialgleichung erfüllt.
	\begin{align*}
		\varphi'(x) &= \frac{1}{G'(G^{-1}(F(x) + c))} F'(x)\\
		&= g(G^{-1}(F(x) + c)) f(x)\\
		&= g(\varphi(x)) f(x),
	\end{align*}
	was gerade die Differentialgleichung~\eqref{7 eq:sep} ergibt.
	
	Umgekehrt: Ist $\varphi: I \to \bbR$ eine Lösung von \eqref{7 eq:sep}, mit $\varphi(I)\subset J$,
	$G' = \frac{1}{g}$ und $F' = f$, dann gilt
	\begin{equation*}
		\varphi'(x) = f(x)g(\varphi(x)).
	\end{equation*}
	Umformen ergibt
	\begin{align*}
		\frac{\varphi'(x)}{g(\varphi(x))} &= f(x) \\
		\Leftrightarrow \quad G'(\varphi(x)) \varphi'(x) = F'(x).
	\end{align*}
	Damit folgt $G(\varphi(x)) = F(x) +c$.
\end{proof}

\begin{satz}
	\label{7.4.1}
	Seien $f:I\to \bbR$, $g:J\to \bbR$ stetig und $x_0\in I$, $y_0\in J$ keine Randpunkte, sowie
	$g(y_0) \ne 0$. Dann hat das Anfangswertproblem $y' = f(x) g(y)$, $y(x_0) = y_0$ in einer genügend
	kleinen Umgebung $B_\varepsilon(x_0)$ genau eine Lösung. Sie ist implizit gegeben durch
	\begin{equation*}
		\int_{y_0}^{y}\frac{\dd v}{g(v)} = \int_{x_0}^{x} f(k) \dd k.
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $G(y) \coloneqq \int_{y_0}^{y}\frac{\dd v}{g(v)}$ und $F(x) \coloneqq \int_{x_0}^{x} f(k)\dd k$.
	Wegen $G'(y_0) = \frac{1}{g(y_0)}$ und $G(y_0) = 0$ existiert ein $\varepsilon > 0$ mit
	$B_\varepsilon(0) \subseteq G(J)$. Da $F(x_0) = 0$ und $F$ stetig ist, existiert ein $\delta > 0$ mit
	$F(B_\delta(x_0)) \subset B_\varepsilon(0) \subset G(J)$. Sei $\varphi(x) \coloneqq G^{-1}(F(x))$,
	$x \in B_\delta(x_0)$. Dann ist $\varphi$ die eindeutige Lösung des Anfangswertproblems in $B_\delta(x_0)$.
\end{proof}

Wir verdeutlichen uns das Gesagte anhand eines Beispiels
\begin{equation*}
	y' = x(1+y^2),
\end{equation*}
was wir schreiben als
\begin{equation*}
	\frac{\dd y}{1 + y^2} = x \dd x.
\end{equation*}
Integration liefert
\begin{equation*}
	\arctan y = \frac{1}{2}x^2 + c
\end{equation*}
und die allgemeine Lösung ist somit
\begin{equation*}
	y(x) = \tan\left(\frac{1}{2} x^2 + c\right).
\end{equation*}

Wir betrachten als nächstes Beispiel
\begin{equation*}
	y' = \frac{x}{1-y^2}
\end{equation*}
liefert nach separieren und integrieren
\begin{equation*}
	y - \frac{1}{3}y^3 = \frac{1}{3} x^3 + c.
\end{equation*}
Das ist eine implizite Lösung, die wir mit unserem bisherigen Wissen nicht weiter auflösen können.

\subsection{Autonome Differentialgleichungen}
Eine Differentialgleichung der Form
\begin{equation*}
	y' = g(y)
	\label{7 eq:autonom}
\end{equation*}
heißt autonom oder $x$-frei (in der Physik oft $t$-frei). Solche Differentialgleichungen sind separierbar, die
Nullstellen von $g$ führen auf konstante Lösungen und nicht-konstante Lösungen in Streifen der $x$-$y$-Ebene
sind implizit bestimmt durch
\begin{equation*}
	\int \frac{\dd y}{g(y)} = x + c,
\end{equation*}
die Lösung ist also immer in der Form $x(y)$ gegeben. Die Konstanten $c$ lassen sich in diesem Bild als Shift
entlang der $x$-Achse interpretieren. Die Lösungen unterscheiden sich also alle nur durch eine Translation.

Als Beispiel betrachten wir die sogenannte logistische Differentialgleichung
\begin{equation*}
	\dot{p} = (a-bp)p,\qquad a,b\in \bbR.
\end{equation*}
Sie modelliert beispielsweise das Wachstum einer Bakterienpopulation in einer beschränkten Gebiet, z .B. auf
einer Agarplatte, und ist ein fundamentales Beispiel für nichtlineare Gleichungen. Sie hat die konstanten
Lösungen $p=0$ und $p = \frac{a}{b}$. Die übrigen Lösungen ergeben sich aus
\begin{equation*}
	\int\frac{\dd p}{(a - b p)p} = t + c.
\end{equation*}
Wir zerlegen zuallererst die rationale Funktion in Partialbrüche
\begin{equation*}
	\frac{1}{(a-bp)p} = \frac{1}{ap} - \frac{1}{a(p-\frac{a}{b})},
\end{equation*}
also ergibt sich
\begin{equation*}
	\int\frac{\dd p}{(a - b p)p} = \frac{1}{a}\log\left|\frac{p}{p-\frac{a}{b}}\right| = t + c.
\end{equation*}
Sei zuerst $c = 0$. Für $0 < p < \frac{a}{b}$ folgt
\begin{equation*}
	p(t) = \frac{a}{b}\frac{1}{1 + e^{-at}}.
\end{equation*}
Für $t\to \infty$ konvergiert $p$ zu $\frac{a}{b}$ und für $t \to -\infty$ geht $p$ gegen $0$.
Für $p>0$ erhalten wir
\begin{equation*}
	p(t) = \frac{a}{b} \frac{1}{1 - e^{-at}}.
\end{equation*}
Der Grenzwert für große Zeiten ($t \to\infty$) ist $\frac{a}{b}$ und für $t \to -\infty$ divergiert
$p \to\infty$.
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,
					xmin=-3,xmax=5,
					xticklabels={,,},
					ymin=-0.5,ymax=1.5,
					ytick={0,1},
					yticklabels={0,$a/b$},]

			\addplot[blue,domain=-3:5] {0};
			\addplot[blue,domain=-3:5] {1};
			
			\addplot[orange,domain=-3:5,samples=40] {1/(1+e^(-x))};
			\addplot[orange,domain=-3:5,samples=40] {1/(1+e^(-x + 1))};
			\addplot[orange,domain=-3:5,samples=40] {1/(1+e^(-x + 2))};
			\addplot[orange,domain= 0:5,samples=40] {1/(1-e^(-x))};
			\addplot[orange,domain= 1:5,samples=40] {1/(1-e^(-x + 1))};
		\end{axis}
	\end{tikzpicture}
	\caption{Lösungen der logistischen Differentialgleichung in den verschiedenen Streifen. Die verschiedenen
		Konstanten verschieben die Lösungen entlang der $x$-Achse.}
\end{figure}

\subsection{Newtonsche Gleichung}
Wir betrachten
\begin{equation*}
	m\ddot{x} = F(x),\qquad m >0,
\end{equation*}
die Newtonsche Differentialgleichung (Bewegungsgleichung) in einer Dimension. Sei $F:I\to\bbR$ stetig und
$V$ eine Stammfunktion von $-F$. Dann gilt
\begin{align*}
	m\ddot{x} &= F\\
	\Leftrightarrow m \ddot{x}\dot{x} + V'(x)\dot{x} &= 0 \\
	\Leftrightarrow \frac{\dd}{\dd t}\left(\frac{m}{2}\dot{x}^2 + V(x) \right) &= 0.
\end{align*}
Also ist
\begin{equation*}
	\frac{m}{2}\dot{x}^2 + V(x) = E = const
\end{equation*}
und wir bekommen eine \emph{autonome Differentialgleichung}
\begin{equation*}
	\dot{x} = \pm \sqrt{\frac{2}{m}(E-V(x))}.
\end{equation*}
Die Lösungen sind implizit bestimmt durch
\begin{equation*}
	\pm \int \frac{\dd x}{\sqrt{\frac{2}{m} (E-V(x))}} = t + c.
\end{equation*}

\subsection{Homogene Differentialgleichungen}
Differentialgleichungen der Form
\begin{equation}
	y' = f\left(\frac{y}{x}\right)
	\label{7 eq:homo}
\end{equation}
heißen \emph{homogen} (nicht zu verwechseln mit linear homogen). Solche Differentialgleichungen lassen sich
durch die Substitution $u = \frac{y}{x}$ auf separierbare Differentialgleichungen transformieren. Ist
$\varphi$ eine Lösung von \eqref{7 eq:homo} und $u(x) = \frac{\varphi(x)}{x}$, dann gilt
\begin{equation*}
	f(u) = \varphi'(x) = (xu(x))' = u(x) + xu'(x).
\end{equation*}
Also
\begin{equation}
	u'(x) = \frac{f(u) - u}{x},
	\label{7 eq:homo2}
\end{equation}
was separierbar ist. Wenn umgekehrt $u$ eine Lösung von \eqref{7 eq:homo2} ist, dann ist $\varphi(x) = xu(x)$
eine Lösung von \eqref{7 eq:homo}.

Ein einfaches Beispiel ist
\begin{equation*}
	y' = \frac{y^2 + x^2 e^{-y/x}}{xy}.
\end{equation*}
Typisch in dem Sinne, dass man der  Differentialgleichung die Homogenität nicht sofort ansieht.
Substitution liefert
\begin{equation*}
	f(u) = u + \frac{1}{u}e^{-u}.
\end{equation*}







