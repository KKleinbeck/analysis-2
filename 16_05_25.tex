% !TEX root = Ana2.tex
\begin{satz}
	\label{8.4.8}
	Sei $f: \bbR\to \bbC$ $2\pi$-periodisch und stückweise glatt mit einer Sprungstelle bei $x=a$. Sei
	$\delta \coloneqq f(a+) - f(a-)$ und $x_N = a + \frac{\pi}{N+1/2}$. Dann gilt
	\begin{equation*}
		S_Nf(x_N) - f(x_N) \ge 0.1789 \frac{\delta}{2} + o(1)\quad (N\to\infty).
	\end{equation*}
\end{satz}

\begin{proof}
	Durch Verschieben von $f$ erreichen wir, dass $a = 0$. Sei $g(x) = f(x) - \frac{\delta}{\pi}\sigma(x)$ für
	$x \ne 2\pi n$ und $g(x) = g(0+) = g(0-)$ für $x = 2\pi n$, $n\in \bbZ$. Dann ist $g$ $2\pi$-periodisch,
	stückweise glatt und stetig im Punkt $x=0$. Also gilt nach Theorem~\ref{8.4.7}
	\begin{equation*}
		S_Ng\to g
	\end{equation*}
	gleichmäßig für $N\to\infty$ und auf einem Intervall $[-\varepsilon,\varepsilon]$ mit $\varepsilon >0$
	klein genug.
	
	Wegen $f = g + \frac{\delta}{\pi}\sigma$ folgt
	\begin{equation*}
		S_N f - f = S_N(g+\frac{\delta}{\pi} \sigma) - g+\frac{\delta}{\pi} \sigma = S_N g - g
		+ \frac{\delta}{\pi} \left(S_N \sigma - \sigma \right) =
		\frac{\delta}{\pi} \left(S_N \sigma - \sigma \right) + o(1)
	\end{equation*}
	gleichmäßig auf $[-\varepsilon,\varepsilon]$. Es genügt also die gewünschte Abschätzung für
	$\frac{\delta}{\pi} \left(S_N \sigma - \sigma \right)$ zu beweisen, d.h.\ den Satz für $f=\sigma$ zu
	zeigen.
	
	Es gilt
	\begin{align*}
		S_N \sigma(x) &= \sum_{k=1}^{N} \frac{\sin kx}{k} \\\
		&= \sum_{k=1}^{N} \int_{0}^{x} \cos (kt) \dd t \\
		&= \sum_{k=-N}^{N} \frac{1}{2} \int_{0}^{x}e^{ikt}\dd t - \frac{x}{2}\\
		&= \frac{1}{2} \int_{0}^{x} \underset{D_N(t)}{\underbrace{\sum_{k=-N}^{N} e^{ikt}}} \dd t
			- \frac{x}{2}.
	\end{align*}
	Auf $[0,x_N]$ ist
	\begin{equation*}
		D_N(t) = \frac{\sin ((N+ 1/2) t)}{\sin t/2} \ge \frac{\sin ((N+ 1/2)t)}{t/2}
	\end{equation*}
	und somit 
	\begin{align*}
		S_N\sigma(x) - \sigma(x) &\ge \frac{1}{2} \int_{0}^{\frac{\pi}{N + 1/2}} \frac{\sin ((N + 1/2)t)}{t/2} \dd t
			\underset{\frac{\pi}{2}}{\underbrace{- \frac{x_N}{2} - \sigma(x_N)}}\\
		&= \int_{0}^{\pi} \frac{\sin u}{u} \dd u - \frac{\pi}{2} \\
		&\ge 0.1789\frac{\pi}{2}.
	\end{align*}
	Der letzte Schritt folgt aus einer numerischen Abschätzung.
\end{proof}

Schlussbemerkung:
\begin{enumerate}
	\item Die Fourierreihe einer $L$-periodischen Funktion $F:\bbR \to \bbC$ lautet
		\begin{equation*}
			\sum_{n=-\infty}^{\infty} c_n e^{in \frac{2\pi}{L} x}
		\end{equation*}
		wobei
		\begin{equation*}
			c_n \coloneqq \frac{1}{2} \int_{0}^{L} e^{-in\frac{2\pi}{L}x} f(x)\dd x.
		\end{equation*}
		Das Theorem~\ref{8.4.2} und \ref{8.4.7} gelten auch für $L$-periodische Funktionen.
	
	\item Jede Funktion $F:[0,L) \to \bbC$ kann zu einer $L$-periodischen Funktion $f: \bbR\to\bbC$ fortgesetzt
		werden durch $f(x +nL) = F(x)$ für $x\in[0,L)$ und $k\in\bbZ$. Die Theoreme~\ref{8.4.2} und \ref{8.4.7}
		können dann auf $f$ angewendet werden. Die Konvergenz der Fourierreihen von $f$ in $x \coloneqq kL$
		hängt von der Stetigkeit von $f$ in diesen Punkten ab, also davon ob $F(0) = \lim\limits_{x\to L} F(x)$.
\end{enumerate}




\chapter[Top.\ und met.\ Strukturen]{Topologische und metrische Strukturen in $\bbR^d$}
\section{$\bbR^d$ als euklidischer Vektorraum}
Die Menge $\bbR^d = \{ (x_1,\dots,x_d) \mid \forall k \in \{1, \hdots, d\}: x_k \in \bbR\}$ versehen mit der Addition und der skalaren
Multiplikation 
\begin{align*}
	(x + y)_k &= x_k + y_k & x,y \in \bbR^d\\
	(\lambda x)_k &= \lambda x_k & \lambda\in \bbR
\end{align*}
ist ein reeller Vektorraum mit Dimension $d$. Die Elemente $x= (x_1,\dots,x_d)\in \bbR^d$ heißen
\emph{Vektoren} und die Zeilen $x_1,\dots,x_N$ sind die \emph{Komponenten} von $x$. Der Nullvektor
$(0,\dots,0)$ wird mit $0$ bezeichnet. Das (euklidische) \emph{Skalarprodukt} zweier Vektoren $x,y \in \bbR$ ist
die Zahl
\begin{equation*}
	\langle x,y\rangle = x\bullet y = \sum_{k=1}^{d} x_k y_k.
\end{equation*}
Die (euklidische) Länge (Norm, Betrag) von $x\in \bbR^d$ ist definiert durch
\begin{equation*}
	|x| \coloneqq \langle x,x \rangle^{1/2} = \sqrt{\sum_{k=1}^{d} x_k x_k}.
\end{equation*}

Bemerkungen:
\begin{enumerate}
	\item Als Menge und als reeller Vektorraum gilt $\bbC = \bbR^2$. Auch die Beträge stimmen überein.
	\item Für $d = 2, 3$ stimmt $\langle x, y\rangle$ mit dem aus der Schule bekannten Skalarprodukt
		\begin{equation*}
			\langle x, y\rangle = |x| |y| \cos \varphi
		\end{equation*}
		überein, wobei $\varphi$ der Winkel zwischen den Vektoren $x$ und $y$ ist und $|x|$ ist nach
		Pythagoras die Länge der Strecke von $0$ nach $x\in\bbR^d$.
	\item Für alle $x\in\bbR^d$ gilt
		\begin{equation*}
			|x_k| \le |x| \le \sum_{k=1}^{d} |x_k|.
		\end{equation*}
\end{enumerate}

\begin{satz}
	\label{9.1.1}
	Für alle $x,y,z\in\bbR^d$, $\lambda \in \bbR$ gilt
	\begin{enumerate}
		\item $\langle x+y,z\rangle = \langle x,z\rangle + \langle y, z \rangle$ und
			$\langle \lambda x, y\rangle = \lambda\langle x,y\rangle$.
		\item $\langle x,y\rangle = \langle y,x\rangle$.
		\item $\langle x,x\rangle \ge 0$ und $ \langle x,x\rangle = 0$ nur dann, wenn $x$ der Nullvektor ist.
	\end{enumerate}
\end{satz}

\begin{satz}
	\label{9.1.2}
	Für alle $x,y\in\bbR^d$ und alle $\lambda\in\bbR$ gilt
	\begin{enumerate}
		\item $\langle x, y\rangle \le |x| |y|$ und Gleichheit gilt nur dann, wenn $x$ und $y$ linear abhängig
			sind (Schwarzsche Ungleichung).
		\item $|x| \ge 0$ und Gleichheit nur wenn $x$ der Nullvektor ist.
		\item $|\lambda x| = |\lambda||x|$
		\item $|x+y| \le |x| + |y|$
		\item $||x|-|y|| \le |x - y|$
	\end{enumerate}
\end{satz}

Zwei Vektoren $x,y\in \bbR^d$ heißen \emph{orthogonal}, wenn $\langle x, y\rangle = 0$.

\begin{satz}
	\label{9.1.3}
	Satz von Pythagoras: Sind die Vektoren $v_1,\dots,v_n \in \bbR^d$ paarweise orthogonal, dann gilt
	\begin{equation*}
		\left| \sum_{k=1}^{n} v_k \right|^2 = \sum_{k=1}^{n}|v_k|^2.
	\end{equation*}
\end{satz}

















