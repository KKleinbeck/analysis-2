% !TEX root = Ana2.tex

Ein weiteres Beispiel ist die Zerlegung von
\begin{equation*}
	\frac{x^2 + 1}{x^3 -1} = \frac{A}{x - 1} + \frac{Bx + C}{x^2 + x + 1}.
\end{equation*}
Nach obigen Theorem ergibt sich $A = \frac{2}{3}$. $B$, $C$ können bestimmt werden durch
\begin{align*}
	x^2 + 1 &= A(x^2 + x + 1) + (x-1)(Bx + C) \\
	&= x^2(A+B) + x(A-B + C) + (A-C).
\end{align*}
Die Lösung dieses Gleichungssystems in den Potenzen von $x$ liefert
\begin{align*}
	A &= \frac{2}{3}\\
	B &= \frac{1}{3}\\
	C &= -\frac{1}{3}.
\end{align*}

\subsection{Integration von Partialbrüchen}
Nach Kapitel 6.4 ist
\begin{equation*}
	\int \frac{A}{(x - \alpha)^m}\dd x =
	\begin{cases}
		 A \log|x-\alpha| & m = 1\\
		 \frac{A}{-m + 1} (x-\alpha)^{-m+1} & m > 1
	\end{cases}.
\end{equation*}
Damit ist die erste Art von Termen in der Partialbruchzerlegung nach unseren Können integrierbar. Es fehlt
noch die Integration von:
\begin{equation*}
	\frac{Bx + C}{(x^2 - 2\beta x + \gamma)^m} = \frac{B}{2} \frac{2x - 2\beta}{(x^2 - 2\beta x +\gamma)^m}
		+ (C + \beta B)\frac{1}{(x^2 - 2\beta + \gamma)^m}.
\end{equation*}
Der erste Summand lässt sich durch die Substitution $u = x^2 - 2\beta x +\gamma$ integrieren
\begin{align*}
	\int \frac{2x - 2\beta}{(x^2 - 2\beta x +\gamma)^m} \dd x &= \int \frac{\dd u}{u^m}\\
	&= \begin{cases}
			 \log|x^2 - 2\beta x +\gamma| & m = 1\\
			 -\frac{1}{m - 1} (x^2 - 2\beta x +\gamma)^{-m+1} & m > 1
		\end{cases}.
\end{align*}

Für den zweiten Term müssen wir etwas anders vorgehen und schreiben den Nenner mittels quadratischer Ergänzung
zunächst um in
\begin{equation*}
	x^2 - 2\beta x + \gamma = (x-\beta)^2 + \lambda^2,
\end{equation*}
wobei wir $\lambda^2 \coloneqq \gamma - \beta^2$ definiert haben. Nach Satz~\ref{6.6.4} ist $\lambda^2 > 0$.
Also
\begin{align*}
	\int \frac{1}{x^2 - 2\beta x + \gamma} \dd x = \int \frac{1}{(u^2 + \lambda^2)^m}\dd u \Big|_{u=x-\beta}
	\equiv I_M,
\end{align*}
wobei
\begin{equation*}
	I_1 = \frac{1}{\lambda}\arctan\left(\frac{x}{\lambda}\right)
\end{equation*}
und für $m \ge 1$
\begin{equation*}
	I_{m+1} = \frac{1}{2m\lambda^2}\left(\frac{u}{(u^2 + \lambda^2)^m} + (2m-1)I_m \right),
\end{equation*}
was durch partielle Integration bewiesen werden kann.



\section{Uneigentliche Integrale}
Zur Erinnerung: Wir kennen bereits aus Kapitel 6.1 das Integral
\begin{equation*}
	\int_{a}^{b} f(x) \dd x
\end{equation*}
für Regelfunktionen $f:[a,b] \to \bbC$ auf einem \emph{kompakten} Intervall $[a,b]\subset \bbR$ kennen gelernt.
Was geschieht, wenn wir es mit anderen Funktionen als Regelfunktionen zu tun haben, oder das Intervall nicht
kompakt ist, beispielsweise $[0,\infty)$ oder $(0,1]$ der Funktion $\sqrt{x}^{-1}$, wie in Bild~\ref{fig:6.7}
skizziert?
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,
					xmin=0,xmax=5,
					ymin=0]
			\addplot[domain=0.1:1,samples=100,blue,name path=hyperbola] {1/sqrt(x)};
			\addplot[domain=0.1:5,samples=100,blue] {1/sqrt(x)};

			\path[name path=axis] (0.1,0) -- (1,0);
			\draw[blue,fill=blue,opacity=0.05] (0,0) rectangle (0.09,5);
			\addplot[blue,fill=blue,fill opacity=0.05] fill between[of=axis and hyperbola];
		\end{axis}
	\end{tikzpicture}
	\caption{Skizze des diskutierten Problems. Was ist der Flächeninhalt der eingefärbten Fläche?}
	\label{fig:6.7}
\end{figure}

\begin{definition}
	Sei $f:I\to \bbC$ eine Regelfunktion auf einem Intervall $I\subset \bbR$ mit Randpunkten
	$a,b\subset \overline{\bbR}$. Falls $I = [a,b)$, dann ist
	\begin{equation}
		\label{eq:uneigentlich11}
		\int_{a}^{b} f(t) \dd t = \lim\limits_{x \to b} \int_{a}^{x} f(t) \dd t,
	\end{equation}
	und falls $I= (a,b]$
	\begin{equation}
		\label{eq:uneigentlich12}
		\int_{a}^{b} f(t) \dd t = \lim\limits_{x \to a} \int_{x}^{b} f(t) \dd t,
	\end{equation}
	und letztlich $I = (a,b)$
	\begin{equation}
		\label{eq:uneigentlich2}
		\int_{a}^{b} f(t) \dd t = \int_{a}^{c} f(t) \dd t + \int_{c}^{b} f(t) \dd t,
	\end{equation}
	was nach den obigen Definitionen integriert werden kann für jedes $c \in (a,b)$.
	
	Diese Integrale heißen \emph{uneigentliche Integrale} von Typ 1
	(\eqref{eq:uneigentlich11},\eqref{eq:uneigentlich12}) oder Typ 2 (\eqref{eq:uneigentlich2}). Sie können
	konvergent oder divergent sein, je nachdem ob die entsprechenden Grenzwerte existieren. Im
	Fall~\eqref{eq:uneigentlich2} ist die Konvergenz beider Integrale notwendig.
\end{definition}

\emph{Beispiel}
\begin{enumerate}
	\item Für $s > 1$ ist
		\begin{equation*}
			\int_{1}^{\infty} \frac{1}{x^s} \dd x = \frac{1}{1 - s}
		\end{equation*}
		Für $s\le 1$ ist das Integral divergent.
		
		\begin{proof}
			\begin{align*}
				\int_{1}^{\infty} \frac{1}{x^s}\dd x &=
					\lim\limits_{R\to\infty} \int_{1}^{R}\frac{1}{x^s}\dd x\\
				&= \lim\limits_{R\to\infty} \frac{1}{1-s} x^{-s+1} \Big|_{1}^{R}\\
				&= \lim_{R\to\infty} \frac{1}{s-1}\left(\frac{1}{R^{s-1}} - 1\right) \\
				&= \frac{1}{1-s}. \qedhere
			\end{align*}
		\end{proof}

		\begin{figure}[!t]
			\centering
			\begin{tikzpicture}
				\begin{axis}[axis lines=middle,
							clip=false,
							xmin=0,xmax=3,
							ymin=-2,ymax=6,]
					\addplot[blue,thick,domain=1.02:3,samples=100,name path=hyperbola2] {1/(x*x)};
					\addplot[blue,thick,domain=0.4:3,samples=100,] {1/(x*x)} node[anchor=south west] {$s>1$};
					\addplot[domain=0.2:3,samples=100,] {1/x};
					\addplot[orange,domain=0.15:0.98,samples=100,name path=hyperbola] {1/sqrt(x)};
					\addplot[orange,domain=0.15:3,samples=100,] {1/sqrt(x)};
					
					\path[name path=axis1] (0.15,0) -- (0.98,0);
					\path[name path=axis2] (1.02,0) -- (3,0);
					
					\draw[orange!50!white,fill=orange,opacity=0.2] (0,0) rectangle (0.145,2.55)
						node[anchor=south west,orange,opacity=1,inner sep=2pt] {$s < 1$};
					\addplot[orange,fill=orange,fill opacity=0.2] fill between[of=axis1 and hyperbola];

					\addplot[blue,fill=blue,fill opacity=0.15] fill between[of=axis2 and hyperbola2];
				\end{axis}
			\end{tikzpicture}
			\caption{Skizze des diskutierten Problems. Was ist der Flächeninhalt der eingefärbten Flächne?}
		\end{figure}
		
	\item Für $s < 1$ ist
		\begin{equation*}
			\int_{0}^{1} x^{-s} \dd x = \frac{1}{1-s}.
		\end{equation*}
	
	\item Das Integral über den Kehrwert der Exponentialfunktion konvergiert in $[0,\infty)$:
		\begin{equation*}
			\int_{0}^{\infty} \exp(-cx)\dd x = \frac{1}{c}.
		\end{equation*}
	
	\item Die sogenannte Lorentzkurve hat über allen reellen Zahlen integriert den Wert
		\begin{align*}
			\int_{-\infty}^{\infty} \frac{1}{1 + x^2} \dd x &= 2\int_{0}^{\infty} \frac{1}{1+x^2}\\
			&= \lim_{R\to\infty} 2\arctan(x)\Big|_{0}^{R} = 2\lim\limits_{R\to\infty} \arctan(R)\\
			&= \pi.
		\end{align*}
\end{enumerate}

\begin{satz}
	\label{6.7.1}
	Sei $I\subset \bbR$ ein Intervall mit Randpunkten $a,b\in \overline{\bbR}$ und seine $f,g:I\to \bbC$
	zwei Regelfunktionen.
	\begin{enumerate}
		\item Ist $|f| \le g$ und ist $\int_{a}^{b} g\dd x$ konvergent, dann ist auch $\int_{a}^{b} f \dd x$
			konvergent.
		
		\item Ist $f \ge g \ge 0$ und ist $\int_{a}^{b} g \dd x$ divergent, dann ist auch
			$\int_{a}^{b} f \dd x$ divergent.
	\end{enumerate}
\end{satz}

\begin{proof}
	\textit{2.}\ ist die Kontraposition von einem Spezialfall von \textit{1.}. Es bleibt also \textit{1.}\ zu
	beweisen. Wir betrachten den Fall $I=[a,b)$ und wir verwenden das Cauchy-Kriterium für Funktionen. Sei
	\begin{equation*}
		F(x) = \int_{a}^{x} f(t) \dd t
	\end{equation*}
	und
	\begin{equation*}
		G(x) = \int_{a}^{x} g(t) \dd t
	\end{equation*}
	und sei $\varepsilon > 0$. Nach Annahme existiert der Grenzwert $\lim\limits_{x \to b^-} G(x)$,
	also existiert ein $c < b$, so dass
	\begin{equation*}
		x, y \in (c,b) \Rightarrow |G(y) - G(x)| < \varepsilon.
	\end{equation*}
	Also gilt für $x,y \in (c,b)$ mit $y> x$
	\begin{align*}
		|F(y) - F(x)| &= \left|\int_{x}^{y} f(t) \dd t \right| \\
		&\le \int_{x}^{y} |f(t)| \dd t\\
		&\le \int_{x}^{y} g(t) \dd t\\
		&= G(y) - G(x)\\
		&< \varepsilon.
	\end{align*}
	Also existiert $\lim\limits_{x\to b^-}F(x)$ nach dem Cauchy-Kriterium.
\end{proof}

\begin{definition}
	Ein uneigentliches Integral $\int_{a}^{b} f \dd x$ heißt \emph{absolut konvergent}, falls
	$\int_{a}^{b} |f|\dd x$ konvergent ist. Dann ist $\int_{a}^{b} f \dd x$ konvergent nach Satz~\ref{6.7.1}
	mit $f \le |f|$.
\end{definition}

\emph{Beispiel:}
$\int_{0}^{\infty} \frac{\sin x}{x}\dd x$ ist konvergent, aber nicht absolut konvergent.

\subsection{Die Gammafunktion}
Die Gammafunktion $\Gamma:(0,\infty) \to \bbR$ ist definiert durch
\begin{equation*}
	\Gamma(x) \coloneqq \int_{0}^{\infty} t^{x-1} e^{-t}\dd t.
\end{equation*}
Es gilt die besondere Eigenschaft $\Gamma(n+1) = n!$ für jede beliebige natürliche Zahl $n$. Damit lässt sich
die Fakultät auf beliebige reelle Zahlen verallgemeinern. Das Integral ist für alle $x > 0$ konvergent.

\begin{proof}
	Für $x \in (0,1]$ ist
	\begin{equation*}
		\Gamma(x) = \int_{0}^{1} t^{x-1} e^{-t} + \int_{1}^{\infty} t^{x-1} e^{-t} \dd t,
	\end{equation*}
	wobei das erste Integral konvergent ist, denn $t^{x-1}e^{-t} \le t^{x-1}$ und $x-1 < 1$ ist
	\begin{equation*}
		\int_{0}^{1} t^{x-1} \dd t < \infty.
	\end{equation*}
	und $\int_{1}^{\infty} t^{x-1}e^{-t} \dd t$ ist konvergent, wegen $t^{x-1}e^{-t} \le ce^{-t/2}$,
	mit $c = \sup_{t\ge 1} t^{x-1} e^{-t/2} < \infty$. Verwende dann Teil \textit{1.}\ von Satz~\ref{6.7.1}.
\end{proof}

\emph{Bemerkung} Es gilt
\begin{equation*}
	\Gamma(x + 1) = x \Gamma(x).
\end{equation*}

\begin{satz}
	\label{6.7.2}
	Sei $f:[1,\infty) \to \bbR$ monoton fallend und $f\le 0$. Dann existiert
	\begin{equation*}
		\gamma \coloneqq \lim\limits_{n \to \infty} \left(\sum_{k=1}^{n} f(k) -
			\int_{1}^{n+1} f(x) \dd x\right)
	\end{equation*}
	und $0\le \gamma \le f(1)$.
\end{satz}

\begin{proof}
	Die Folge
	\begin{align*}
		\gamma_n &\coloneqq \sum_{k=1}^{n} f(k) - \int_{1}^{n+1} f(x) \dd x\\
		&= \sum_{k=1}^{n} \left(f(k) -  \int_{k}^{k+1} f(x) \dd x\right)
	\end{align*}
	ist monoton wachsend denn $f(k) \ge \int_{k}^{k+1} f(t) \dd t$. Außerdem gilt
	\begin{equation*}
		\int_{k}^{k+1} f(x) \dd x \ge f(k+1),
	\end{equation*}
	also
	\begin{equation*}
		\gamma_n \ge \sum_{k=1}^{n} \left(f(k) -  f(k+1)\right) = f(1) - f(n+1) \le f(1).
	\end{equation*}
	Also existiert $\gamma = \lim \gamma_n$ und $0 \le \gamma \le f(1)$.
\end{proof}

\begin{cor}
	\label{6.7.3}
	Integralkriterium für Reihen: Sei $f:[1,\infty) \to \bbR$ monoton fallend und $f \ge 0$. Dann ist die
	Reihe $\sum_{k=1}^{\infty} f(k)$ genau dann konvergent, wenn das Integral $\int_{0}^{\infty} f(t) \dd t$
	konvergent ist.
\end{cor}