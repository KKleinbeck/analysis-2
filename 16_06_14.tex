% !TEX root = Ana2.tex

Eine Kurve $\gamma: I \to \bbR^n$ heißt \emph{rektifizierbar}, wenn
\begin{equation*}
	L(\gamma)\coloneqq \sup_{t_0 <\dots< t_n \in I} \sum_{k=1}^{n} |\gamma(t_k) - \gamma(t_{k-1})| <\infty,
\end{equation*}
wobei das Supremum über beliebige endliche, geordnete Tupel $t_0 < \dots < t_n$ genommen wird. Ein Beispiel
ist in Abbildung~\ref{fig:length curve} gegeben. $L(\gamma)$ heißt \emph{Länge} der Kurve $\gamma$. Ist $I = [a,b]$
kompakt, genügt es, die Mengen $\{t_0,\dots,t_n\}$ zu berücksichtigen, mit $t_0 = a$, $t_n = b$. Ist $I = [a,b]$ und
$\gamma:I\to \bbR^n$ Lipschitz-stetig mit Konstante $L$, dann ist $\gamma$ rektifizierbar und $L(\gamma) \le L \cdot (b-a)$.

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\draw plot[smooth] coordinates {(0,0) (1,1) (2,1) (4,0.5) (4,1.5) (3,2.5) (3,3.5) (5,5)}
			node[anchor=west] {$\gamma$};
		\draw[blue] (0,0) node[anchor=north] {$\gamma(t_0)$} -- (1,1) node[anchor=south east] {$\gamma(t_1)$}
			-- (2,1) node[anchor=south west] {$\gamma(t_2)$} -- (4,0.5) -- (4,1.5) -- (3,2.5) -- (5,5);
	\end{tikzpicture}
	\caption{Skizze einer möglichen Einteilung der Kurve in stückweise geraden Abschnitte. Die Länge der einzelnen
		Abschnitte approximiert in der Summe die Länge der Kurve $\gamma$.}
	\label{fig:length curve}
\end{figure}

Die Kurve $\gamma: [0,a]\to \bbR^2$ mit $\gamma(0) = (0,0)$ und 
\begin{equation*}
	\gamma(t) = \big(t, t\sin(1/t)\big)
\end{equation*}
ist \emph{nicht} rektifizierbar.

Für ein $n$-Tupel von Regelfunktionen $f=(f_1,\dots,f_n)$, $f_i :[a,b]\to \bbR$ definieren wir
\begin{equation*}
	\int_{a}^{b} f(x) \dd x = \left(\int_{a}^{b}f_1(x)\dd x,\dots, \int_{a}^{b} f_n(x) \dd x \right) \in \bbR^n.
\end{equation*}
Es gilt
\begin{equation*}
	\left|\int_{a}^{b} f(x) \dd x\right| \le \int_{a}^{b} |f(x)| \dd x.
\end{equation*}

\begin{proof}
	Sei $e \in \bbR^n$ mit $|e| = 1$. Dann ist
	\begin{align*}
		\langle e, \int_{a}^{b} f(x) \dd x\rangle &= \sum_{i=1}^{n} e_i \int_{a}^{b} f_i(x) \dd x\\
		&=\int_{a}^{b} \sum_{i=1}^{n} e_i f_i(x) \dd x\\
		&=\int_{a}^{b} \langle e, f(x)\rangle \dd x.
	\end{align*}
	Daraus folgt
	\begin{equation*}
		\left| \langle e,\int_{a}^{b} f(x) \dd x \rangle \right|
			= \left| \int_{a}^{b}\langle e, f(x) \rangle \dd x \right|
			\le \int_{a}^{b} |\langle e, f(x)\rangle| \dd x
			\le \int_{a}^{b} |f(x)| \dd x.
	\end{equation*}
	Mit
	\begin{equation*}
		e \coloneqq \frac{\int_{a}^{b} f(x) \dd x}{\left|\int_{a}^{b} f(x)\dd x\right|}
	\end{equation*}
	folgt die Behauptung.
\end{proof}

\begin{satz}
	\label{10.2.1}
	Sei $\gamma:[a,b] \to \bbR^n$ eine stückweise stetig differenzierbare Kurve. Dann gilt
	\begin{equation*}
		L(\gamma) = \int_{a}^{b} |\dot{\gamma}(t)| \dd t.
	\end{equation*}
\end{satz}

\emph{Stückweise stetig differenzierbar} bedeutet, dass $\gamma$ stetig ist und dass $\gamma$ stetig differenzierbar
auf Teilintervallen $[t_{k-1},t_k]$ einer geeigneten Partition $a = t_0 < \dots < t_n = b$ ist.

\begin{proof}
	Wir nehmen zuerst an, $\gamma$ sei stetig differenzierbar auf $[a,b]$. Aus dem Hauptsatz der Integralrechnung
	wissen wir $\gamma_i(t_k) - \gamma_i(t_{k-1}) = \int_{t_{k-1}}^{t_k} \dot{\gamma}_i(t) \dd t$ für
	$i = 1,\dots, n$. Damit folgt
	\begin{equation*}
		\gamma(t_k) - \gamma(t_{k-1}) = \int_{t_{k-1}}^{t_k} \dot{\gamma}(t) \dd t.
	\end{equation*}
	Also ist
	\begin{equation*}
		\sum_{k=1}^{n} | \gamma(t_k) - \gamma(t_{k-1})|
			= \sum_{k=1}^{n}\left| \int_{t_{k-1}}^{t_k} \dot{\gamma}(t) \dd t\right|
			\le \sum_{k=1}^{n} \int_{t_{k-1}}^{t_k} |\dot{\gamma}(t)| \dd t
			= \int_{t_0}^{t_n} |\dot{\gamma}(t)| \dd t.
	\end{equation*}
	Somit ist $L(\gamma) \le \int_{a}^{b} |\dot{\gamma}(t)| \dd t$.
	
	Für den Beweis von $L(\gamma) \ge \int_{a}^{b} |\dot{\gamma}(t)| \dd t$ genügt es, zu jedem $\varepsilon > 0$ eine
	Partition $t_0 < t_1 < \dots t_m$ zu finden mit
	$\sum|\gamma(t_{i+1}) - \gamma(t_i)| \ge \int_{a}^{b} |\dot{\gamma}(t)|\dd t - \varepsilon$. Da $\dot{\gamma}$ stetig ist, ist
	$\dot{\gamma}$ auf $[a,b]$ gleichmäßig stetig (Theorem~\ref{10.1.4}). Also existiert ein $\delta > 0$, so dass
	\begin{equation*}
		|s-t| < \delta \Rightarrow |\dot{\gamma}(s) - \dot{\gamma}(t)| < \frac{\varepsilon}{2(b-a)}.
	\end{equation*}
	Also wählen wir $t_0 = a < t_1 < \dots < t_n = b$ mit $t_k - t_{k-1} < \delta$ für alle $k$. Dann gilt
	\begin{align*}
		|\gamma(t_k) - \gamma(t_{k-1})| &= \left| \int_{t_{k-1}}^{t_k} \dot{\gamma}(t) \dd t\right| \\
		&= \left| \int_{t_{k-1}}^{t_k} \dot{\gamma}(t_k) \dd t
			+ \int_{t_{k-1}}^{t_k} \dot{\gamma}(t) - \dot{\gamma}(t_k) \dd t \right| \\
		&\ge \left| \int_{t_{k-1}}^{t_k} \dot{\gamma}(t_k) \dd t\right|
			- \int_{t_{k-1}}^{t_k}
				\underset{< \varepsilon/2(b-a)}{\underbrace{|\dot{\gamma}(t) - \dot{\gamma}(t_k)|}}
			\dd t \\
		&\ge \left| \int_{t_{k-1}}^{t_k} \dot{\gamma}(t_k) \dd t\right| - \frac{\varepsilon}{2(b-a)}(t_k - t_{k-1})\\
		&\ge \int_{t_{k-1}}^{t_k} |\dot{\gamma}(t)| \dd t - \frac{\varepsilon}{b-a}(t_k - t_{k-1}).
	\end{align*}
	Daraus folgt
	\begin{equation*}
		\sum_{k=1}^{n} |\gamma(t_k) - \gamma(t_{k-1})| \ge \int_{a}^{b} |\dot{\gamma}(t)| \dd t - \varepsilon.
	\end{equation*}
	
	Ist $\gamma$ stückweise stetig differenzierbar, dann existiert $a = t_0 < \dots < t_n = b$, so dass $\gamma$ auf
	$[t_{k-1},t_k]$ stetig differenzierbar ist und somit
	\begin{equation*}
		L(\gamma) = \sum_{k=1}^{n} L(\gamma|_{[t_{k-1},t_k]})
			= \sum_{k=1}^{n} \int_{t_{k-1}}^{t_k}|\dot{\gamma}(t)|\dd t = \int_{a}^{b} |\dot{\gamma}(t)| \dd t.
			\qedhere
	\end{equation*}
\end{proof}

\subsubsection{Parametertransformation}
Sei $\gamma: I\to \bbR^n$ eine parametrisierte $C^k$- Kurve, $k \in \bbN_0$. Eine
Parametertransformation $\sigma$ von $\gamma$ ist eine bijektive Abbildung $\sigma: J \to I$ auf einem Intervall $J$,
so dass $\sigma$, $\sigma^{-1}$ von der Klasse $C^k$ sind. Die Umparametrisierung
\begin{equation*}
	\tilde{\gamma} = \gamma \circ\sigma: J\to \bbR^n
\end{equation*}
von $\gamma$ ist wieder eine parametrisierte $C^k$-Kurve. Sie hat die gleiche Spur und die gleiche Länge wie $\gamma$.
$\sigma$ heißt orientierungstreu wenn $\sigma$ streng monoton wachsend ist.

Eine (orientierte) \emph{$C^k$-Kurve} in $\bbR^n$ ist eine Äquivalenzklasse von parametrisierten $C^k$-Kurven
wobei $\gamma_2 \sim \gamma_1$, wenn $\gamma_2 = \gamma_1 \circ \sigma$ mit einer (orientierungstreuen)
Parametertransformation $\sigma$ gilt.

Kurven können mit Worten und Bildern beschrieben werden: z.B..\ der "`ein Mal im Gegenuhrzeigersinn durchlaufene
Einheitskreis um den Ursprung"'. Sie wird repräsentiert durch die parametrisierte Kurve
\begin{align*}
	\gamma:[0,2\pi] &\to \bbR^2\\
	\gamma(t) = (\cos t, \sin t).
\end{align*}

\begin{lemma}
	\label{10.2.2}
	Sei $\sigma: J \to I$ eine bijektive Abbildung zwischen Intervallen $I,J \subset \bbR$ und $k\in \bbN$. Dann sind
	äquivalent
	\begin{enumerate}
		\item $\sigma$ und $\sigma^{-1}$ sind von der Klasse $C^k$.
		
		\item $\sigma \in C^k(J)$ und $\sigma'(t) \ne 0$ für alle $t$.
	\end{enumerate}
\end{lemma}

\begin{proof}
	Dass \textit{1.}  die Aussage \textit{2.} impliziert, folgt aus
	\begin{equation*}
		1 = \frac{\dd}{\dd t} \sigma^{-1}\big(\sigma(t)\big) = (\sigma^{-1})'(\sigma(t))\sigma'(t).		
	\end{equation*}
	 Also $\sigma'(t) \ne 0$.
	 
	 Aus $\sigma \in C^k$ und $\sigma'(t) \ne 0$ für die $t$ folgt, dass auch $\sigma^{-1} \in C^k$ nach
	 Satz~5.6.4 aus Analysis~I. Damit folgt aus Aussage~\textit{2.} die Aussage~\textit{1.}
\end{proof}

Sei $\gamma$ eine \emph{reguläre} $C^k$-Kurve ($k\ge 1$), $t_0 \in I$ und
\begin{equation*}
	\sigma(t) = \int_{t_0}^{t} |\dot{\gamma}(\tau)| \dd \tau.
\end{equation*}
Dann ist $\sigma \in C^k(I)$ und $\dot{\sigma}(t) = |\dot{\gamma}(t)| > 0$. Also ist $\sigma^{-1}$ eine
orientierungstreue Parametertransformation von $\gamma$. Die Umparametrisierung
\begin{equation*}
	c = \gamma\circ \sigma^{-1}: \sigma(i) \to \bbR^n
\end{equation*}
hat die Geschwindigkeit
\begin{align*}
	c'(s) &= \frac{\dd}{\dd s} (\gamma\circ \sigma^{-1})(s)\\
	&= \dot{\gamma}(\sigma^{-1}(s))\frac{\dd}{\dd s} \sigma^{-1}(s)\\
	&= \dot{\gamma}(\sigma^{-1}(s))\frac{1}{\dot{\sigma}(\sigma^{-1}(s))} \\
	&= \frac{\dot{\gamma}(t)}{|\dot{\gamma}(t)|},
\end{align*}
wobei $t = \sigma^{-1}(s)$. Der neue Parameter $s$ heißt \emph{Bogenlänge}, denn $s = \sigma(t)$, die Länge der
Kurve $\gamma$. Nach Umparametrisierung auf Bogenlänge wird die Kurve mit konstanter Geschwindigkeit durchlaufen.

\subsubsection{Krümmung ebener Kurven}
Sei $c: I \to \bbR^2$ eine durch Bogenlänge parametrisierte $C^2$-Kurve (d.h. $|c'(t)| \equiv 1$). Die Vektoren
\begin{equation*}
	T(s) \coloneqq c'(s) \qquad N(s) \coloneqq J T(s)
\end{equation*}
mit
\begin{equation*}
	J =
	\begin{pmatrix}
		0 & -1 \\ 1 & 0
	\end{pmatrix}
\end{equation*}
nennt man \emph{begleitendes Zweibein} von $c$. Aus $|T| = |c'| = 1$ folgt
\begin{equation*}
	0 = \frac{\dd}{\dd s}\langle T,T\rangle = 2 \langle T', T\rangle,
\end{equation*}
d.h.\ $T'$ und $T$ sind orthogonal. Also existiert $\chi(s)\in \bbR$ mit
\begin{equation*}
	T'(s) = \chi(s) N(s),
\end{equation*}
da auch $N(s)$ orthogonal auf $T(s)$ steht. $\chi(s) = \langle T'(s),N(s)\rangle$ heißt Krümmung der Kurve $c$
an der Stelle $s$.

Beispiel: Sei $\gamma(t) = (r\cos t, r\sin t)$, $t\in [0,2\pi]$. Es ist $\Spur(\gamma) =$ Kreis mit Radius $r$.
Es gilt $|\dot{\gamma}(t)| = r$ und $s = \int_{0}^{t} |\dot{\gamma}(\tau)|\dd \tau = tr$.
Also $t = s/r$ und die Parametrisierung des Kreises durch die Bogenlänge ist gegeben durch
\begin{equation*}
	c(s) = \left(r\cos \frac{s}{r}, r \sin\frac{s}{r}\right)
\end{equation*}
und
\begin{equation*}
	c''(s) = \frac{1}{r} \left(-\cos\frac{s}{r}, - \sin \frac{s}{r}\right).
\end{equation*}
Also is $\chi(s) = |c''(s)| = 1/r$ und $\chi(s) = 1/r$. Für eine reguläre $C^2$-Kurve $\gamma:I\to \bbR^2$ gilt
\begin{equation*}
	\chi(t) = \frac{\det(\dot{\gamma}(t),\ddot{\gamma}(t))}{|\dot{\gamma}(t)|^3}.
\end{equation*}


























