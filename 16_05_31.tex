% !TEX root = Ana2.tex

\section{Folgen und Reihen in $\bbR^d$}
Die Begriffe konvergent, divergent, beschränkt, Cauchy-Folge, Häufungspunkt und $\varepsilon$-Umgebung
lassen sich von $\bbR^2 = \bbC$ unverändert auf $\bbR^d$ übertragen:
Eine Folge $(x)_n$ in $\bbR^n$ heißt
\begin{enumerate}
	\item \emph{konvergent} mit Limes $x\in\bbR^d$, wenn $\lim_{n\to \infty} |x_n - x| = 0$,
	
	\item \emph{beschränkt}, wenn ein $R\in \bbR$ existiert mit $|x_n| \le R$,
	
	\item \emph{Cauchy-Folge}, wenn zu jedem $\varepsilon > 0$ ein $N\in\bbN$ existiert mit $m,n \ge N$, so dass
		$|x_n - x_m| < \varepsilon$.
\end{enumerate}
Die Folge $(x_n)$ hat den Häufungspunkt $x \in \bbR^d$, wenn jede $\varepsilon$-Umgebung
\begin{equation*}
	B_\varepsilon(x) \coloneqq \{y\in\bbR^d\mid |x-y| < \varepsilon \}
\end{equation*}
unendliche viele Glieder der Folge enthält. Das ist genau dann der Fall, wenn $x$ Grenzwert einer Teilfolge von
$(x_n)$ ist. Der Beweis ist analog zu dem aus der Analysis 1.

\begin{satz}
	\label{9.2.1}
	Wenn $\lim_{n\to \infty} x_n = x$, $\lim_{n\to \infty} y_n = y$ in $\bbR^d$ und
	$\lim_{n\to\infty}\lambda_n = \lambda$ in $\bbR$, dann gilt
	\begin{enumerate}
		\item $\lim_{b\to\infty}(x_n + y_n) = x+y$, $\lim_{b\to\infty}(\lambda_n y_n) = \lambda y$,
		
		\item $\lim_{n\to\infty} \langle x_n,y_n\rangle = \langle x,y \rangle$,
		
		\item $\lim_{n\to \infty} |x_n| = |x|$.
	\end{enumerate}
\end{satz}

\begin{proof}
	\begin{enumerate}
		\item[\textit{3.}] $|\, |x_n| - |x|\, |  \le |x_n - x| \to 0$ für $n\to\infty$.
		
		\item[\textit{2.}] Wir berechnen
			\begin{align*}
				\left| \langle x,y\rangle - \langle x_n,y_n\rangle \right|
					&= \left| \langle x,y - y_n\rangle - \langle x_n - x,y_n\rangle \right| \\
					&\le |x||y-y_n| + |x-x_n| |y| \to 0
			\end{align*}
		
		\item[\textit{1.}] Es ist
			\begin{align*}
				\left| (x+y) - (x_n + y_n)\right| &= \left|(x-x_n) + (y- y_n)\right| \\
				&\le |x - x_n| + |y-y_n|
				&\to 0\quad (n\to\infty). & \qedhere
			\end{align*}
	\end{enumerate}
\end{proof}

\begin{theorem}
	\label{9.2.2}
	Jede beschränkte Folge in $\bbR^d$ hat eine konvergente Teilfolge.
\end{theorem}

\begin{proof}
	Beweis mit Induktion in $d$. Für $d = 1$ ist die Aussage aus der Analysis~1 bekannt. Sei $d \ge 2$ und die
	Aussage sei richtig in $\bbR^{d-1}$. Sei $(x_n)$ eine beschränkte Folge in $\bbR^d$. Wir schreiben
	\begin{equation*}
		x_n = (x'_n, y_n) \in \bbR^{d-1}\times \bbR.
	\end{equation*}
	Wegen $|x_n'| \le |x_n|$ ist $(x_n)'$ eine beschränkte Folge in $\bbR^{d-1}$ und hat also nach
	Induktionsannahme eine konvergente Teilfolge $(x_{k_{n}}')_{k\in\bbN}$, $x_{k_{n}}' \to x' \in \bbR^{d-1}$.
	Wegen $|y_{k_{n}}| \le |x_{k_n}|$ ist $y_{k_{n}}$ eine beschränkte Folge in $\bbR$. Nach Bolzano-Weierstraß in
	$\bbR$ hat $(y_{k_{n}})$ eine konvergente Teilfolge $(y_{m_{k}}) \to y \in \bbR$. Als Teilfolge von
	$(x_{k_{n}}')$ gilt auch $(x_{m_{k}}') \to x'$. Es folgt
	\begin{equation*}
		\left| (x',y) - (x_{m_{k}}',y_{m_k})\right|^2 = |x' - x_{m_{k}}'|^2 + |y-y_{m_{k}}|^2 \to 0.
		\qedhere
	\end{equation*}
\end{proof}


\begin{theorem}
	\label{9.2.3}
	Eine Folge in $\bbR^d$ ist genau dann konvergent, wenn sie eine Cauchy-Folge ist.
\end{theorem}

\begin{proof}
	Das jede konvergente Folge eine Cauchy-Folge ist, folgt leicht aus der Dreiecksungleichung.
	Sei jetzt $(x_k)$ eine Cauchy-Folge in $\bbR^d$. Wegen $|\,|x_n| - |x_m|\,| \le |x_m-x_n|$ ist dann
	$(|x_n|)$ eine Cauchy-Folge in $\bbR$ und somit beschränkt. D.h. die Folge $(x_k)$ ist beschränkt in
	$\bbR^d$. Nach Theorem~\ref{9.2.2} existiert eine konvergente Teilfolge $x_{n_{k}} \to x$. Da $(x_n)$
	eine Cauchy-Folge ist gibt es zu $\varepsilon > 0$ ein $N\in\bbN$ mit
	\begin{equation*}
		n,m\ge N,\Rightarrow|x_n-x_m| \le \frac{\varepsilon}{2}.
	\end{equation*}
	
	Also gilt
	\begin{equation*}
		n \ge N \Rightarrow |x-x_n| = \lim_{k\to\infty} |x_{n_{k}} - x_{n}| \le \frac{\varepsilon}{2}
			< \varepsilon.
	\end{equation*}
	Das beweist, dass $x_n \to x$.
\end{proof}

\begin{satz}
	\label{9.2.4}
	Eine Folge $(x_n) \in \bbR^d$ mit $x_n = (x_{n,1},\dots,x_{n,d})$ konvergiert genau
	gegen $a=(a_1,\dots,a_d) \in \bbR^d$, wenn $\lim_{n\to\infty} x_{n,k} = a_k$ für
	$k=1\dots d$.
\end{satz}

\begin{proof}
	Der Satz folgt aus
	\begin{equation*}
		|x_{n,k} - a_k| \le |x_k-a|  = \left(\sum_{k=1}^{d}  (x_{n,k} - a_k)^2 \right)^{1/2}. \qedhere
	\end{equation*}
\end{proof}

Eine Reihe $\sum a_k$ mit Gliedern $a_k \in \bbR^d$ heißt \emph{absolut konvergent}, wenn
$\sum_{k\ge0}|a_k|<\infty$.
\begin{satz}
	\label{9.2.5}
	Jede absolut konvergente Reihe in $\bbR^d$ ist konvergent.
\end{satz}

\section{Topologie des euklidischen Raumes $\bbR^d$}
Eine Menge $D\subset \bbR$ heißt \emph{offen}, wenn zu jedem $x\in D$ ein $\varepsilon>0$ existiert mit
$B_\varepsilon(x) \subset D$. $D\subset \bbR^d$ heißt \emph{abgeschlossen}, wenn $\bbR^d\setminus D$ offen ist.
$D$ heißt \emph{Umgebung} von $x$, wenn ein $\varepsilon>0$ existiert mit $B_\varepsilon(x) \subset D$. Eine
Menge $D\subset \bbR^d$ ist genau dann offen, wenn sie Umgebung jedes Punktes $x\in D$ ist.

\begin{satz}
	\label{9.3.1}
	\begin{enumerate}
		\item $\emptyset$, $\bbR^d$ sind offen.
		
		\item Sind $V_1,\dots,V_n \subset \bbR^d$ offen, dann ist auch $\bigcap_{k=1}^{n}V_k$ offen.
		
		\item Ist $(V_i)_{i\in I}$ eine beliebige Familie offener Teilmengen von $\bbR^d$, dann ist
			$\bigcup_{i\in I} V_i$ offen.
	\end{enumerate}
\end{satz}

\noindent
Bemerkung: Die Intervalle $(-1/k, 1/k)$, $k\in \bbR$, sind offen, aber
\begin{equation}
	\bigcap_{k\ge 1} (-1/k,1/k) = \{0\}
\end{equation}
ist \emph{nicht} offen. Der Satz gilt nur für endliche Schnitte.

Erinnerung: $\bigcup_{i\in I} V_i \coloneqq \{x\in\bbR^d\mid\exists i \in I: x\in V_i\}$.

\begin{cor}
	\label{9.3.2}
	\begin{enumerate}
		\item $\emptyset$, $\bbR^d$ sind abgeschlossen.
		
		\item Sind $A_1,\dots, A_n\subset\bbR^d$ abgeschlossen, dann ist auch $\bigcup_{k=1}^{n} A_k$
			abgeschlossen.
		
		\item Ist $(A_i)_{i\in I}$ eine beliebige Familie abgeschlossener Teilmengen von $\bbR^d$, dann ist
			$\bigcap_{i\in I} A_i$ abgeschlossen.
	\end{enumerate}
\end{cor}

\begin{proof}
	\begin{enumerate}
		\item[\textit{2.}] Z.z.: $\bigcup A_k$ ist abgeschlossen. Es ist
			\begin{equation*}
				\bbR^d\setminus \bigcup_{k=1}^{n}A_k = \bigcap_{k=1}^n \left(\bbR^d\setminus A_k \right),
			\end{equation*}
			was offen ist nach Satz~\ref{9.3.1}.
		
		\item[\textit{3.}] $\bbR^d\setminus\bigcap_{i\in I}A_i = \bigcup \bbR^d\setminus A_i$, was offen ist.
			Also ist $\bigcap A_i$ abgeschlossen. \qedhere
	\end{enumerate}
\end{proof}

\begin{satz}
	\label{9.3.3}
	Eine Teilmenge $A\subset \bbR^d$ ist genau dann abgeschlossen, wenn für jede konvergente Folge $(x_k)$ in
	$\bbR^d$ gilt:
	\begin{equation*}
		x_k \in A \text{ für alle $k$} \quad\Rightarrow\quad \lim_{k\to\infty} x_k \in A.
	\end{equation*}
\end{satz}

\begin{proof}
	Sei $A$ abgeschlossen und $(x_n)$ eine Folge in $A$ mit $x_n \to x$. Wir machen die Widerspruchsannahme
	$x \not\in A$. Dann ist $x\in \bbR^d \setminus A$, was offen ist. Also existiert ein $\varepsilon > 0$ mit
	$B_\varepsilon(x) \subset \bbR^d\setminus A$. Da $x_n \to x$ existiert $N = N_\varepsilon \in \bbN$ mit
	$x_n \in B_\varepsilon(x)$ für $n > N$. D.h.\ $x_n \in \bbR^d\setminus A$ für $n \ge N$ ist im Widerspruch
	zur Annahme $x_n \in A$ für alle $n$. Also ist $x\in A$.
	
	Sei nun $x_n \in A \Rightarrow \lim x_n \in A$. Wir zeigen, dass $A$ abgeschlossen ist. Wieder machen wir
	eine Widerspruchsannahme: $A$ sei nicht abgeschlossen. Also ist $\bbR^d\setminus A$ \emph{nicht} offen. Also
	existiert $x\in \bbR^d\setminus A$ mit $B_\varepsilon(x) \cap A \ne \emptyset$ für alle $\varepsilon > 0$.
	D.h.\ $B_{1/n}(x) \cap A \ne \emptyset$ für alle $n \in \bbN$. Wähle, zu jedem $n\in \bbN$,
	$x_n \in B_{1/n}(x)\cap A$. Dann gilt $|a-x_n| <\frac{1}{n} \to 0$ und $x_n \in A$. Also, nach Annahme,
	$x = \lim x_n \in A$, im Widerspruch zu $x \in \bbR^d \setminus A$. Somit war die Annahme falsch und $A$ ist
	abgeschlossen.
\end{proof}

Sei $D\subset \bbR^d$. Dass \emph{Innere} $\inner{D}$ von $D$ und der \emph{Abschluss} $\overline{D}$ von $D$
werden durch
\begin{equation*}
	\inner{D} \coloneqq \bigcup_{V\subset D, \text{$V$ offen}} V,\qquad
	\overline{D} \coloneqq \bigcap_{A\supset D, \text{$A$ abg.}} A
\end{equation*}
definiert. Nach Satz~\ref{9.3.1} und Korollar~\ref{9.3.2} ist $\inner{D}$ offen und $\overline{D}$ abgeschlossen.
Also ist $\inner{D}$ die größte offene Teilmenge von $D$ und $\overline{D}$ ist die kleinste abgeschlossene
Teilmenge, welche $D$ enthält.

\begin{satz}
	\label{9.3.4}
	Für jede Teilmenge $D \subset \bbR^d$ gilt
	\begin{enumerate}
		\item $D$ ist offen genau dann, wenn $D = \inner{D}$,
		
		\item $D$ ist genau dann abgeschlossen, wenn $D = \overline{D}$,
		
		\item $\overline{D^c} = (\inner{D})^c$ und $\overline{D}^c = (D^c)^\circ$.
	\end{enumerate}
\end{satz}






