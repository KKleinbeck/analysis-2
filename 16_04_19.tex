% !TEX root = Ana2.tex

Bemerkung: Ist $f$ reellwertig, so folgt aus dem Mittelwertsatz der Integralrechnung, Satz~\ref{6.3.4}, dass
\begin{align*}
	\frac{1}{(n-1)!} \int_{a}^{x} (x-t)^{n-1} f^{(n)}(t)\dd t
		&= f^{(n)}(t_x) \frac{1}{(n-1)!} \int_{a}^{x}(x-t)^{n-1} \dd t\\
		&= f^{(n)}(t_x) \frac{1}{n!} (x-a)^{n},\qquad t_x\in(a,x).
\end{align*}
Das integrale Restglied ist folglich für reellwertige Funktionen gleich dem Lagrange-Restglied.

\subsection{Integration durch Substitution}

\begin{theorem}
	\label{6.5.3}
	Sei $F:I\to \bbC$ stetig und $\varphi:[a,b]\to I$ stetig differenzierbar. Dann gilt
	\begin{equation*}
		\int f\left(\varphi(x) \right)\varphi'(x) \dd x = \int f(u) \dd u \Big|_{u=\varphi(x)}
	\end{equation*}
	und
	\begin{equation*}
		\int_{a}^{b} f\left(\varphi(x) \right)\varphi'(x) \dd x = \int_{\varphi(a)}^{\varphi(b)} f(u) \dd u.
	\end{equation*}
\end{theorem}

Das wird Integration per Substitution genannt

\begin{proof}
	Ist $F$ Stammfunktion von $f$, dann gilt
	\begin{align*}
		f(\varphi(x)) \varphi'(x) = F'(\varphi(x))\varphi'(x) = \frac{\dd}{\dd x} F(\varphi(x))
	\end{align*}
	nach der Kettenregel. Also folgt
	\begin{align*}
		\int f(\varphi(x)) \varphi'(x) \dd x &= \int \frac{\dd}{\dd x} F(\varphi(x))\dd x\\
		&= F(\varphi(x)) + c\\
		&= F(u) + c \Big|_{u=\varphi(x)}\\
		&= \int f(u) \dd u \Big|_{u=\varphi(x)}.
	\end{align*}
	Analog bestimmen wir
	\begin{align*}
		\int_{a}^{b} f(\varphi(x)) \varphi'(x) \dd x &= \int_{a}^{b} \frac{\dd}{\dd x} F(\varphi(x))\dd x\\
		&= F(\varphi(b)) + F(\varphi(a))\\
		&= F(x)\Big|_{\varphi(a)}^{\varphi(b)}\\
		&= \int_{\varphi(a)}^{\varphi(b)} f(u) \dd u. \qedhere
	\end{align*}
\end{proof}

Dieses Theorem ist extrem bedeutsam in der Integralrechnung und sollte auswendig gekonnt werden. Eine einfache
Anleitung lautet wie folgt:
\begin{enumerate}
	\item Zunächst substituiere $u=\varphi(x)$ und $\dd u = \varphi'(x) \dd x$.
	\item Integriere unbestimmt $f$ nach $u$.
	\item Drücke zum Schluss $u$ wieder durch $x$ aus.
\end{enumerate}

Betrachten wir dazu ein Beispiel. Als erstes haben wir
\begin{equation*}
	\int \frac{1}{x(\log x)^2} \dd x
\end{equation*}
Wir identifizieren $\varphi = \log x$ und damit $f(\varphi(x)) = \frac{1}{\varphi(x)^2}$. Damit substituieren
wir entsprechend und berechnen
\begin{align*}
	\int \frac{1}{x(\log x)^2} \dd x &= \int \frac{1}{u^2}\dd u\Big|_{u=\varphi(x)}\\
	&= -\frac{1}{u} + c \Big|_{u=\varphi(x)}\\
	&= -\frac{1}{\log x} + c.
\end{align*}
Durch Ableiten dieses Ausdruckes können wir unser Ergebnis zusätzlich verifizieren, wenn wir möchten:
\begin{align*}
	\frac{\dd}{\dd x} \left(-\frac{1}{\log x}\right) &= \frac{1}{(\log x)^2} \frac{\dd}{\dd x} \log x \\
	&= \frac{1}{x(\log x)^2}.
\end{align*}

Für bestimmte Integrale ist die einfache Anleitung ähnlich:
\begin{enumerate}
	\item Substituiere formal $u=\varphi(x)$ und $\dd u = \varphi'(x) \dd x$ und ersetze die
		Integrationsgrenzen durch $\varphi(a)$ und $\varphi(b)$.
	\item Integriere bestimmt $f$ nach $u$.
\end{enumerate}

Ein Beispiel dafür ist
\begin{equation*}
	\int_{0}^{1} \sqrt{1 - u^2} \dd u.
\end{equation*}
Wir wenden die Substitution rückwärts an und substituieren $u = \sin x$ und $\dd u = \cos x \dd x$. Damit
ergibt sich
\begin{align*}
	\int_{0}^{1} \sqrt{1 - u^2} \dd u &= \int_{0}^{\pi/2}\cos\vartheta \sqrt{1 - \sin^2\vartheta}\dd\vartheta\\
	&= \int_{0}^{\pi/2} \cos^2\vartheta \dd\vartheta\\
	&= \frac{1}{2}\int_{0}^{\pi/2} 1\dd x = \frac{\pi}{4}.
\end{align*}
Im letzten Schritt haben wir unsere Rekursionsformel aus dem Abschnitt zur partiellen Integration verwendet.

\section{Integration rationaler Funktionen}
\subsection{Partialbruchzerlegung}
Beispielsweise wollen wir die einfache rationale Funktion
\begin{equation*}
	f(x) = \frac{1}{x^2-1}
\end{equation*}
integrieren. Dazu zerlegen wir den Bruch in einfacherer Brüche, indem wir ihn zuerst in Linearfaktoren zerlegen
\begin{equation*}
	\frac{1}{x^2 - 1} = \frac{1}{(x+1) (x-1)} \overset{!}{=} \frac{A}{x-1} + \frac{B}{x+1}.
\end{equation*}
Die Lösung des Ansatzes, also die Werte für $A$ und $B$, finden wir, indem wir mit $x^2-1$ durchmultiplizieren.
Wir erhalten das lineare Gleichungssystem
\begin{equation*}
	A(x+1) + B(x-1) = 1
\end{equation*}
in den einzelnen Potenzen von $x$, also
\begin{align*}
	A + B &= 0\\
	A - B &= 1.
\end{align*}
Die Lösung lautet $A=\frac{1}{2}$ und $B=-\frac{1}{2}$. Damit gilt
\begin{equation*}
	f(x) = \frac{1}{x^2 - 1} = \frac{1}{2}\frac{1}{x-1} - \frac{1}{2}\frac{1}{x+1}.
\end{equation*}
Das ist eine Summe einfacher Funktionen, deren Stammfunktionen wir bereits kennen. Also bekommen wir
\begin{align*}
	\int \frac{1}{x^2 - 1} \dd x &= \int \frac{1}{2}\frac{1}{x-1} - \frac{1}{2}\frac{1}{x+1} \dd x\\
	&= \frac{1}{2}\big(\log|x-1| - \log|x+1| \big).
\end{align*}

\begin{satz}
	\label{6.6.1}
	Seien $p$, $q$ zwei Polynome, wobei $q \ne 0$. Dann gibt es eine eindeutig bestimmte Darstellung
	\begin{equation*}
		p = bq + r \text{ bzw. } \frac{p}{q} = b + \frac{r}{q}
	\end{equation*}
	mit Polynomen $b$ und $r$, wobei $\operatorname{Grad}(r) < \operatorname{Grad}(q)$.
\end{satz}

Dieser Satz wurde in der Linearen Algebra bewiesen. Beispiel:
\begin{equation*}
	\frac{2x^4 + 1}{x^2 + 1} = 2x^2 - 2 + \frac{3}{x^2 + 1}.
\end{equation*}
Wir nenne ein Polynom $p(x) = a_nx^n + \dots + a_1 x + a_0$ normiert, wenn $a_n = 1$.

\begin{satz}
	\label{6.6.2}
	Sei $p$ ein normiertes Polynom vom Grad $n\ge 1$. Dann gibt es $n$ bis auf Nummerierung eindeutig
	bestimmte komplexe Zahlen $\xi_1,\dots,\xi_n$, so dass
	\begin{equation*}
		p(x) = \prod_{k=1}^{n} (x-\xi_k).
	\end{equation*}
\end{satz}

\begin{proof}
	Induktion in $n$. Für $n = 1$ ist $p(x) = x + a_0$. Also mit $\xi_1 = -a_0$ gilt $p(x) = x - \xi_1$.
	Induktionsschritt: Die Behauptung sei wahr für ein gegebenes $n\ge 1$ und $\operatorname{Grad}(p) = n+1$.
	Nach dem Fundamentalsatz der Algebra existiert ein $\xi_{n+1}\in \bbC$ mit $p(\xi_{n+1}) = 0$. Dividiere
	$p$ mit $x-\xi_{n+1}$: Nach Satz~\ref{6.6.1} gilt $p(x) = p^*(x)(x-\xi_{n+1}) + r$ mit $r\in \bbC$. Aus
	$p(\xi_{n+1}) = 0$ folgt $r= 0$. Aus $p(x) = p^*(x) (x-\xi_{n+1})$ folgt, dass
	$\operatorname{Grad}(p^*) = n$. Damit kann $p^*$ nach Induktionsvoraussetzung in der obigen Form
	zerlegt werden.
	
	Es bleibt noch die Eindeutigkeit zu zeigen: Ist $p(x) =\prod_{k = 1}^{n+1} (x-\eta_k)$ eine zweite
	Zerlegung. Mit Induktion in $n$ zeigt man $\{\xi_1,\dots,\xi_n\} = \{\eta_1,\dots,\eta_n\}$.
\end{proof}

Tritt der Linearfaktor $x-\xi$ in der Zerlegung $p=\prod (x-\xi_k)$ genau $m$ Mal auf, dann heißt $\xi$ eine
\emph{$m$-fache Nullstelle} von $p$.

\begin{satz}
	\label{6.6.3}
	Ist $q$ ein reellesPolynom und $\xi \in \bbC$ eine $m$-fache Nullstelle von $q$, dann ist auch
	$\overline{\xi}$ eine $m$-fache Nullstelle von $q$.
\end{satz}

\begin{satz}
	\label{6.6.4}
	Jedes normierte reelle Polynom $q$ besitzt eine, bis auf die Reihenfolge der Faktoren eindeutige
	Darstellung
	\begin{equation*}
		q(x) = \prod_{k=1}^{k_0}(x-\alpha_k)^{m_k}\prod_{j=1}^{j_0}(x^2 - 2\beta_j x +\gamma_j)^{n_j}~,
	\end{equation*}
	wobei
	\begin{enumerate}
		\item $\alpha_k \in \bbR$, $k=1,\dots,k_0$ paarweise verschieden sind,
		\item paarweise verschiedene Faktoren $(x^2 - 2\beta_jx + \gamma_j)$ keine gemeinsamen Nullstellen
			haben,
		\item die Diskriminanten $\beta^2_j - \gamma_j$ negativ sind.
	\end{enumerate}
\end{satz}

Die vorhergehenden Sätze werden aus Zeitgründen nicht bewiesen.

\begin{theorem}
	\label{6.6.5}
	Sei $p/q$ eine reelle rationale Funktion mit normiertem Nennerpolynom $q$ und sei
	$\operatorname{Grad}(p) < \operatorname{Grad}(q)$. Dann gibt es eine eindeutige Darstellung von $p/q$ als
	Summe von Partialbrüchen bestehend aus folgenden Summanden
	Für jeden Faktor $(x - \alpha)^m$ der Zerlegung von $q$ nach Satz~\ref{6.6.4} gibt es $m$ Summanden
	\begin{equation*}
		\frac{A}{x - \alpha} + \frac{A_2}{(x - \alpha)^2} + \dots + \frac{A_m}{(x - \alpha)^m}
	\end{equation*}
	und für jeden Faktor $(x^2 -2\beta x +\gamma )^n$ gibt es $n$ Summanden
	\begin{equation*}
		\frac{B_1 x + C_1}{(x^2 - 2\beta x +\gamma)} + \frac{B_2 x + C_2}{(x^2 - 2\beta x +\gamma)^2}
		+ \dots + \frac{B_n x + C_n}{(x^2 - 2\beta x +\gamma)^n}.
	\end{equation*}
\end{theorem}

Bemerkungen: Ist $\operatorname{Grad}(p) \ge \operatorname{Grad}(q)$ dann muss zuerst eine Division mit Rest
durchgeführt werden (siehe Satz~\ref{6.6.1}). Der Beweis des Theorems~\ref{6.6.5} erfolgt in der Analysis 3.
Weiter, ist $\alpha$ eine einfache Nullstelle des Nennerpolynoms $q$, das heißt
\begin{equation*}
	\frac{p}{q} = \frac{p}{(x-\alpha)q^*},\qquad q^*(\alpha) \ne 0,
\end{equation*}
dann ist der Koeffizient $A$ des Partialbruch gegeben durch $A= \frac{p(\alpha)}{q^*(\alpha)}$. Das folgt
direkt aus der obigen Identität.
\begin{equation*}
	\frac{p}{q} = \frac{p}{(x-\alpha)q^*} = \frac{A}{x-\alpha} + r.
\end{equation*}
Damit formen wir um
\begin{equation*}
	\frac{p}{q^*} = A + r(x-\alpha).
\end{equation*}
Einsetzen von $x = \alpha$ liefert das Ergebnis.

Beispiel
\begin{equation*}
	\frac{x^2 - 2}{(x+1)x(x-1)} = \frac{A}{x+1} + \frac{B}{x} + \frac{C}{x-1}.
\end{equation*}
Die Koeffizienten $A$, $B$ und $C$ berechnen wir direkt mit Hilfe der Bemerkung zu
\begin{align*}
	 A &= \frac{-1}{2},\\
	 B &= \frac{-2}{-1} = 2,\\
	 C &= \frac{-1}{2}.
\end{align*}











