% !TEX root = Ana2.tex
Eine $2\pi$-periodische Funktion $f:\bbR\to\bbC$ heißt \emph{stückweise glatt}, wenn eine Zerlegung
$0 = x_0 \le \dots \le x_n =2\pi$ und stetig differenzierbare Funktionen $f_k:[x_k,x_{k+1}] \to \bbC$ existieren
mit $f(x) = f_k(x)$ für $x\in (x_k,x_{k+1})$.

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,
					width=0.5\textwidth,
					xmin=0,
					xmax=12.6,
					xtick={0,6.28,12.56},
					xticklabels={0,$2\pi$,$4\pi$},]
			\addplot[blue,domain=0:6.28] {1 - x/3.14};
			\addplot[blue,domain=6.28:12.56] {1 - (x-6.28)/3.14};
		\end{axis}
	\end{tikzpicture} %
	\begin{tikzpicture}
		\begin{axis}[axis lines=middle,
					width=0.5\textwidth,
					xmin=0,
					xmax=12.6,
					xtick={0,6.28,12.56},
					xticklabels={0,$2\pi$,$4\pi$},]
			\addplot[orange,domain=0:6.28,samples=200] {sqrt(x/3.14)} node[anchor=north west]{$\sqrt{x}$};
			\addplot[orange,domain=6.28:12.56,samples=200] {sqrt((x-6.28)/3.14)};
		\end{axis}
	\end{tikzpicture}
	\caption{Beispiel einer stückweisen glatten und einer nicht glatten Funktion}
\end{figure}

\begin{cor}
	\label{8.4.3}
	Sei $f: \bbR\to \bbC$ $2\pi$-periodisch und stückweise glatt mit Fourierkoeffizienten $c_n$. Dann gilt für
	alle $x\in\bbR$,
	\begin{equation*}
		\sum_{n=-\infty}^{\infty} c_n e^{inx} = \frac{1}{2}\left(f(x+) + f(x-)\right),
	\end{equation*}
	wobei $\frac{1}{2}\left(f(x+) + f(x-)\right) = f(x)$, wenn $f$ stetig in $x$ ist.
\end{cor}

Das Korollar folgt aus Theorem~\ref{8.4.2} und da die Voraussetzungen aus Theorem~\ref{8.4.2} für jedes
$x\in\bbR$ erfüllt sind. 

\begin{lemma}
	\label{8.4.4}
	Auf jedem Intervall $[\delta,2\pi-\delta]$ mit $\delta>0$ ist die Reihe $\sum_{k=1}^{\infty} e^{ikx}/k$
	gleichmäßig konvergent.
\end{lemma}

\begin{proof}
	Wir definieren zunächst
	\begin{align*}
		S_n(x) = \sum_{k=0}^{n} e^{ikx} = \frac{1 - e^{i(k+1)x}}{1 - e^{ix}}.
	\end{align*}
	Dann ist
	\begin{align*}
		|S_n(x)| &= \left| \frac{e^{-ix(k+1)/2}}{e^{-ix/2}}\frac{1 - e^{i(k+1)x}}{1 - e^{ix}}.\right| \\
		&= \left| \frac{\sin\left(\frac{x(k+1)}{2}\right)}{\sin \frac{x}{2}}\right|\\
		&\le \left|\frac{1}{\sin \frac{x}{2}}\right|\\
		&\le \frac{1}{\sin \frac{x}{2}} \eqqcolon M,\qquad x \in [\delta,2\pi-\delta].
	\end{align*}

	Beweis der gleichmäßigen Konvergenz mit dem Cauchy-Kriterium: Sei $n>m$, $n,m\in\bbN$. Dann ist
	\begin{align*}
		\sum_{k=1}^{n}\frac{e^{ikx}}{k} - \sum_{k=1}^{m} \frac{e^{ikx}}{k}
			&= \sum_{k=m+1}^{n} \frac{e^{ikx}}{k}\\
		&= \sum_{k=m+1}^{n} \left( S_k(x) - S_{k-1}(x)\right)/k\\
		&= \sum_{k=m+1}^{n}\frac{S_k(x)}{k} - \sum_{k=m}^{n-1}S_k(x) \frac{1}{k+1}\\
		&= \sum_{k=m}^{n-1} S_k(x) \left(\frac{1}{k} - \frac{1}{k+1}\right)
			+ \frac{S_n(k)}{n} - \frac{S_m(k)}{m}.
	\end{align*}
	Der Betrag dieser Summen ist somit
	\begin{align*}
		\left| \sum_{k=1}^{n}\frac{e^{ikx}}{k} - \sum_{k=1}^{m} \frac{e^{ikx}}{k} \right|
			&\le \sum_{k=m}^{n-1} |S_k(x)| \left(\frac{1}{k} - \frac{1}{k+1}\right)
				+ \frac{|S_n(k)|}{n} + \frac{|S_m(k)|}{m}\\
			&\le M \sum_{k=m}^{n-1} \left(\frac{1}{k} - \frac{1}{k+1}\right) + \frac{M}{k} + \frac{M}{k+1}\\
			&= M\left(\frac{1}{m} - \frac{1}{n}\right) + \frac{M}{n} + \frac{M}{m} = \frac{2M}{m}.
	\end{align*}
	Sei $\varepsilon >0$ und $n>m$ so groß, dass $\frac{2M}{m} <\varepsilon$, dann folgt, dass
	\begin{equation*}
		\left| \sum_{k=1}^{n}\frac{e^{ikx}}{k} - \sum_{k=1}^{m} \frac{e^{ikx}}{k} \right| < \varepsilon
	\end{equation*}
	für alle $x\in [\delta,2\pi-\delta]$.
\end{proof}

\begin{satz}
	\label{8.4.5}
	Für alle $x\in(0,2\pi)$ gilt
	\begin{equation*}
		\sum_{k=1}^{\infty} \frac{\sin kx}{k} = \frac{1}{2} (\pi - x)
	\end{equation*}
	und auf jedem Intervall $[\delta,2\pi-\delta]$ mit $\delta > 0$ ist die Konvergenz gleichmäßig.
\end{satz}

\begin{proof}
	Nach Lemma~\ref{8.4.4} ist Konvergenz gleichmäßig, da $\sin kx = \imPart e^{ikx}$. Es bleibt die Summe zu
	berechnen.
	Sei
	\begin{equation*}
		f(t) = \sum_{k=1}^{\infty} \frac{e^{ikx}}{k}t^k
	\end{equation*}
	für $t\in[0,1]$ und $x\in(0,2\pi)$ fest. Nach dem Satz von Abel ist $f$ stetig und die Reihe $\sum e^{ikx}/k$
	ist konvergent.Insbesondere ist $f(1) = \lim\limits_{t\to 1} f(t)$. Für $|t| < 1$ ist $f$ differenzierbar
	(Theorem \ref{8.2.3}) und
	\begin{align*}
		f'(t) &= \sum_{k=1}^{\infty} e^{ikx} t^{k-1} \\
		&= \frac{1}{t} \sum_{k=1}^{\infty} (e^{ix}t)^{k}\\
		&= \frac{1}{e^{-ix} - t} \\
		&= \frac{1}{(\cos x - t) - i\sin x}.
	\end{align*}
	Also
	\begin{equation*}
		\imPart f'(t) = \frac{\sin x}{(t-\cos x)^2 + (\sin x)^2}.
	\end{equation*}
	Da $f(0) = 0$ folgt
	\begin{align*}
		\imPart f(1) &= \int_{0}^{1} \imPart f'(t) \dd t \\
		&= \int_{0}^{1}\frac{\sin x}{(t-\cos x)^2 + (\sin x)^2} \dd t\\
		&= \arctan\left(\frac{t-\cos x}{\sin x}\right)\Big|_{0}^{1}\\
		&= \arctan\left(\frac{1 - \cos x}{\sin x}\right) + \arctan\left(\frac{\cos x}{\sin x}\right).
	\end{align*}
	Wir verwenden jetzt, dass
	\begin{align*}
		\sin(x) &= 2\sin(x/2)\cos(x/2)\\
		1-\cos x &= 2 \sin^2(x/2)\\
		\frac{\cos x}{\sin x} &= \tan \left( \frac{\pi}{2} - x \right).
	\end{align*}
	Damit ist
	\begin{equation*}
		\im f(1) = \arctan(\tan x/2) + \arctan\left(\tan \left(\frac{\pi}{2} - x\right)\right)
			= \frac{1}{2}(\pi - x).
	\end{equation*}
	$\frac{\pi}{2} - x = \arctan \left( \tan \left(\frac{\pi}{2} - x\right)\right)$ gilt im Intervall $x \in (0,\pi)$. Für $x \in (\pi,2\pi)$
	können wir den Beweis durch die Substitution $x = 2\pi - u$ analog durchführen.
\end{proof}

Folgerung: $\sum e^{ikx}/k$ ist die Fourierreihe von $\frac{1}{2}(\pi - x)$.

\begin{lemma}
	\label{8.4.6}
	Sei $f:[0,2\pi]\to \bbC$ eine Regelfunktion mit Fourierkoeffizienten $(c_n)$. Dann gilt
	\begin{equation*}
		\sum_{n=-\infty}^{\infty} |c_n|^2 \le \frac{1}{2\pi} \int_{0}^{2\pi} |f(x)|^{2}\dd x.
	\end{equation*}
\end{lemma}

\begin{proof}
	Sei $S_N(x) = \sum_{n=-N}^{N}c_n e^{inx}$. Dann gilt
	\begin{align*}
		0 &\le \int_{0}^{2\pi} | f(x) - S_N|^2 \dd x\\
		&= \int_{0}^{2\pi} |f(x)|^2 + \int_{0}^{2\pi} |S_N(x)|^2 \dd x
			- 2\Re \int_{0}^{2\pi} f(x)\overline{S_N(x)} \dd x\\
		&=  \int_{0}^{2\pi} |f(x)|^2 - 2\pi \sum_{n=-N}^{N} |c_n|^2.
	\end{align*}
	Denn
	\begin{equation*}
		\Re \int_{0}^{2\pi} f(x) \overline{S_N(x)}\dd x = \int_{0}^{2\pi} |S_N(x)|^2\dd x. \qedhere	
	\end{equation*}
\end{proof}

\begin{theorem}
	\label{8.4.7}
	Sei $f:\bbR \to \bbC$ $2\pi$-periodisch und stückweise glatt. Dann gilt
	\begin{itemize}
		\item Auf jedem kompakten Intervall $[a,b]$ weg von den Unstetigkeitsstellen konvergiert die Fourierreihe
			\emph{gleichmäßig} gegen $f$.
		\item Ist $f$ überall stetig, dann konvergiert die Fourierreihe von $f$ absolut und gleichmäßig auf ganz
			$\bbR$.
	\end{itemize}
\end{theorem}

\begin{proof}
	Wir beweisen zuerst den zweiten Teil. Sei $f:\bbR\to\bbC$ stetig. Wir wissen aus Korollar~\ref{8.4.3}, dass
	\begin{equation*}
		\sum_{n=-\infty}^{\infty} c_n e^{inx} =f(x)
	\end{equation*}
	für alle $x\in\bbR$. Zum Beweis der gleichmäßigen Konvergenz verwenden wir den Weierstraßschen $M$-Test.
	\begin{align*}
		c_n(f') &= \frac{1}{2\pi}\int_{0}^{2\pi} e^{-inx} f'(x)\dd x\\
		&= e^{-inx}f(x)\Big|_{0}^{2\pi} + \frac{in}{2\pi}\int_{0}^{2\pi}e^{-inx} f(x) \dd x\\
		&= in c_n(f).
	\end{align*}
	Damit ist $|c_n(f)| = \frac{1}{n} |c_n(f')|$ für alle $n\ne 0$. Also
	\begin{align*}
		\sum_{n=-\infty}^{\infty} |c_n| &= |c_0| + \sum_{n\ne 0}|c_n| \\
		&= |c_0| + \sum_{n\ne 0}\frac{1}{n} |c_n(f')|\\
		&\le |c_0| + \frac{1}{2}\sum_{n\ne 0} \left( \frac{1}{n^2} + |c_n(f')|^2 \right) \\
		&\le |c_0| + \frac{1}{2} \sum_{n \ne 0} \frac{1}{n^2} + \frac{1}{2}\frac{1}{2\pi} \int_{0}^{2\pi} |f'(x)|^2 \dd x\\
		&< M.
	\end{align*}
	Da $|e^{inx}c_n| \le |c_n|$ für alle $x\in\bbR$ folgt die Behauptung aus dem $M$-Test.
	
	Zum Teil 1. Seien $x_1< \dots < x_m$ die Sprungstellen von $f$ in $[0, 2\pi]$. Dann ist $f$ in $(x_k,x_{k+1})$
	stetig. Sei
	\begin{equation*}
		\delta_k = f(x_k+) - f(x_k-)
	\end{equation*}
	die Sprunghöhe und
	\begin{equation*}
		h(x) = \sum_{k=1}^{m} \frac{\delta_k}{\pi} \sigma(x-x_k),
	\end{equation*}
	wobei $\sigma(x) = (\pi - x)/2$ in $[0,2\pi)$ und $2\pi$ periodisch ist. Die Funktion $h$ ist $2\pi$
	periodisch, stückweise glatt und sie hat dieselben Sprungstellen und Sprunghöhen wie $f$. Also ist
	$g\coloneqq f - h$ stetig und stückweise glatt (genauer ist $g(x_k) = (f-h)(x_k+) = (f-h)(x_k-)$. Aus der
	bereits bewiesenen zweiten Aussage folgt, dass $S_N g \to g$ gleichmäßig für $N\to \infty$, wobei
	\begin{equation*}
		S_N g(x) = \frac{1}{2\pi}\int_{0}^{2\pi} D_N(x-t) g(t) \dd t.
	\end{equation*}
	Das entspricht der $N$-ten Partialsumme der Fourierreihe von $g$. Nach Satz~\ref{8.4.5} gilt für
	$\delta > 0$, dass $S_N\sigma \to \sigma$ gleichmäßig auf $[\delta,2\pi - \delta]$.
	
	Also gilt für jedes kompakte Intervall $[a,b]\subset \bbR$, das keine Sprungstellen von $h$ enthält
	$S_Nh\to h$ auf $[a,b]$ für $N\to\infty$.  Daraus folgt, dass
	\begin{equation*}
		S_N f = S_N (g+h) = S_N g + S_N h \to g+h = f
	\end{equation*}
	gleichmäßig auf $[a,b]$.
\end{proof}






























